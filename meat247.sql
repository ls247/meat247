-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 18, 2020 at 10:34 AM
-- Server version: 10.1.47-MariaDB-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `meat247`
--

-- --------------------------------------------------------

--
-- Table structure for table `biggie`
--

CREATE TABLE `biggie` (
  `id` int(11) NOT NULL,
  `fname` varchar(600) NOT NULL,
  `email` varchar(600) NOT NULL,
  `phone_number` varchar(600) NOT NULL,
  `delivery_address` varchar(600) NOT NULL,
  `plan` varchar(600) NOT NULL,
  `referral` varchar(100) NOT NULL,
  `agent_commission` varchar(100) NOT NULL,
  `customer_discount` varchar(100) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `reference` varchar(600) NOT NULL,
  `paystack_status` varchar(600) NOT NULL,
  `paystack_reference_lsxpress` varchar(600) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `biggie`
--

INSERT INTO `biggie` (`id`, `fname`, `email`, `phone_number`, `delivery_address`, `plan`, `referral`, `agent_commission`, `customer_discount`, `amount`, `reference`, `paystack_status`, `paystack_reference_lsxpress`, `date`) VALUES
(219, 'esther farm', 'esther.akowe@livestock247.com', '07033141516', '99 opebi', 'Biggie', '07033141516', '952.38', '476.19', '47619', '377679171', 'Success', 'T703190275605469', '2020-11-18 08:15:15'),
(220, 'Esther Eze', 'estherakowe@yahoo.com', '07033141516', '99 opebi', 'Biggie', '07033141516', '952.38', '476.19', '47619', '1582954863', 'Pending', '', '2020-11-18 08:24:59'),
(221, 'Esther Eze', 'estherakowe@yahoo.com', '07033141516', '99 opebi', 'Biggie', '07033141516', '962', '481', '47619', '1453261358', 'Pending', '', '2020-11-18 08:31:15'),
(222, 'Esther Eze', 'estherakowe@yahoo.com', '07033141516', '99 opebi', 'Biggie', '07033141516', '962', '481', '47619', '500526751', 'Pending', '', '2020-11-18 08:32:09'),
(223, 'Esther Eze', 'estherakowe@yahoo.com', '07033141516', '99 opebi', 'Biggie', '07033141516', '962', '481', '47619', '1962686767', 'Pending', '', '2020-11-18 08:33:54'),
(224, 'Esther Eze', 'estherakowe@yahoo.com', '07033141516', '99 opebi', 'Biggie', '07033141516', '962', '481', '47619', '496422518', 'Pending', '', '2020-11-18 08:36:22'),
(225, 'Esther Eze', 'estherakowe@yahoo.com', '07033141516', '99 opebi', 'Biggie', '07033141516', '962', '481', '47619', '496422518', 'Pending', '', '2020-11-18 08:36:29'),
(226, 'Esther Eze', 'estherakowe@yahoo.com', '07033141516', '99 opebi', 'Biggie', '07033141516', '962', '481', '47619', '1701950046', 'Pending', '', '2020-11-18 08:42:26');

-- --------------------------------------------------------

--
-- Table structure for table `midi`
--

CREATE TABLE `midi` (
  `id` int(11) NOT NULL,
  `fname` varchar(600) NOT NULL,
  `email` varchar(600) NOT NULL,
  `phone_number` varchar(600) NOT NULL,
  `delivery_address` varchar(600) NOT NULL,
  `plan` varchar(600) NOT NULL,
  `referral` varchar(100) NOT NULL,
  `agent_commission` varchar(100) NOT NULL,
  `customer_discount` varchar(100) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `reference` varchar(600) NOT NULL,
  `paystack_status` varchar(600) NOT NULL,
  `paystack_reference_lsxpress` varchar(600) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `midi`
--

INSERT INTO `midi` (`id`, `fname`, `email`, `phone_number`, `delivery_address`, `plan`, `referral`, `agent_commission`, `customer_discount`, `amount`, `reference`, `paystack_status`, `paystack_reference_lsxpress`, `date`) VALUES
(11, 'Esther Eze', 'estherakowe@yahoo.com', '07033141516', '99 opebi', 'Midi', '07033141516', '491.04', '245.52', '24552', '611582929', 'Success', 'T380739197670397', '2020-11-18 08:15:58'),
(12, 'Esther Eze', 'estherakowe@yahoo.com', '07033141516', '99 opebi', 'Midi', '07033141516', '491.04', '245.52', '24552', '2017484333', 'Pending', '', '2020-11-18 08:32:36'),
(13, 'Esther Eze', 'estherakowe@yahoo.com', '07033141516', '99 opebi', 'Midi', '07033141516', '491.04', '245.52', '24552', '1853485376', 'Pending', '', '2020-11-18 08:45:23'),
(14, 'Esther Eze', 'estherakowe@yahoo.com', '07033141516', '99 opebi', 'Midi', '07033141516', '496', '248', '24552', '1362742608', 'Pending', '', '2020-11-18 09:10:34');

-- --------------------------------------------------------

--
-- Table structure for table `mini`
--

CREATE TABLE `mini` (
  `id` int(11) NOT NULL,
  `fname` varchar(600) NOT NULL,
  `email` varchar(600) NOT NULL,
  `phone_number` varchar(600) NOT NULL,
  `delivery_address` varchar(600) NOT NULL,
  `plan` varchar(600) NOT NULL,
  `referral` varchar(100) NOT NULL,
  `agent_commission` varchar(100) NOT NULL,
  `customer_discount` varchar(100) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `reference` varchar(600) NOT NULL,
  `paystack_status` varchar(600) NOT NULL,
  `paystack_reference_lsxpress` varchar(600) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mini`
--

INSERT INTO `mini` (`id`, `fname`, `email`, `phone_number`, `delivery_address`, `plan`, `referral`, `agent_commission`, `customer_discount`, `amount`, `reference`, `paystack_status`, `paystack_reference_lsxpress`, `date`) VALUES
(6, 'esther farm', 'estherakowe@yahoo.com', '07033141516', '99 opebi', 'Mini', '07033141516', '255.42', '127.71', '12771', '1155016323', 'Success', 'T159725946361675', '2020-11-18 08:12:47'),
(7, 'Esther Eze', 'estherakowe@yahoo.com', '07033141516', '99 opebi', 'Mini', '07033141516', '258', '129', '12771', '161910910', 'Pending', '', '2020-11-18 09:11:28'),
(8, 'Esther Eze', 'estherakowe@yahoo.com', '07033141516', '99 opebi', 'Mini', '07033141516', '258', '129', '12771', '161910910', 'Pending', '', '2020-11-18 09:14:31');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `referral` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `user_type` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `code` varchar(100) NOT NULL,
  `active` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `bank` varchar(100) NOT NULL,
  `account_name` varchar(100) NOT NULL,
  `account_number` varchar(100) NOT NULL,
  `percentage` float NOT NULL,
  `user_image` longtext NOT NULL,
  `user_card` longtext NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `email`, `referral`, `address`, `user_type`, `password`, `code`, `active`, `status`, `bank`, `account_name`, `account_number`, `percentage`, `user_image`, `user_card`, `date`) VALUES
(2, 'omonego', 'Eze', 'esther.akowe@livestock247.com', '081878768768768', '44 Igodo Road, Magboro off Lagos-Ibadan expressway', 'user', '25d55ad283aa400af464c76d713c07ad', 'kD3lAHoz75fy', 'verified', 'Approved', 'Access', 'Bethel David', '0121190898', 2, 'b8237faecfe9ed32785d2b3893b127de.jpg', 'cbe363449f432417b94f194ca0127131.jpg', '2020-10-22 20:36:17'),
(3, 'Livestock247', 'Admin', 'admin@livestock247.com', '09062903550', '99 opebi road ikeja lagos', 'admin', 'fcea920f7412b5da7be0cf42b8c93759', 'bagLYr6jWEvH', 'verified', 'Approved', 'Zenith', 'Esther Akowe Omonego Eze', '0121190898', 0, '', '', '2020-10-23 04:36:14'),
(11, 'Omonego', 'Esther', 'estherakowe@yahoo.com', '07033141516', '99 Opebi Road Ikeja Lagos', 'user', '25d55ad283aa400af464c76d713c07ad', 'kBeOrdLPwuXg', 'verified', 'Approved', 'Zenith', 'Faith', '544654654', 2, '60673887a32eea6d7043860c174b4485.jpg', 'a7cd5934a747094a13b949db8a9671a8.jpg', '2020-11-06 17:22:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `biggie`
--
ALTER TABLE `biggie`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `midi`
--
ALTER TABLE `midi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mini`
--
ALTER TABLE `mini`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`referral`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `biggie`
--
ALTER TABLE `biggie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=227;
--
-- AUTO_INCREMENT for table `midi`
--
ALTER TABLE `midi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `mini`
--
ALTER TABLE `mini`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
