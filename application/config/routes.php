<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'pagescontroller';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['lagos'] = 'pagescontroller/lagos';
$route['abuja'] = 'AbjController/abuja';

//abuja meat247 packages
$route['abuja_biggie'] = 'AbjController/abuja_biggie';
$route['abuja_biggie_pay'] = 'AbjController/abuja_biggie_pay';
$route['abuja_biggie_success'] = 'AbjController/abuja_biggiesuccess';
$route['abuja_midi'] = 'AbjController/abuja_midi';
$route['abuja_midi_pay'] = 'AbjController/abuja_midi_pay';
$route['abuja_midi_success'] = 'AbjController/abuja_midisuccess';
$route['abuja_mini'] = 'AbjController/abuja_mini';
$route['abuja_mini_pay'] = 'AbjController/abuja_mini_pay';
$route['abuja_mini_success'] = 'AbjController/abuja_minisuccess';

//lagos meat247 packages
$route['biggie'] = 'pagescontroller/biggie';
$route['biggie_pay'] = 'pagescontroller/biggie_pay';
$route['biggie_success'] = 'pagescontroller/biggiesuccess';
$route['midi'] = 'pagescontroller/midi';
$route['midi_pay'] = 'pagescontroller/midi_pay';
$route['midi_success'] = 'pagescontroller/midisuccess';
$route['mini'] = 'pagescontroller/mini';
$route['mini_pay'] = 'pagescontroller/mini_pay';
$route['mini_success'] = 'pagescontroller/minisuccess';

$route['goat_large'] = 'pagescontroller/goat_large';
$route['goat_large_pay'] = 'pagescontroller/goat_large_pay';
$route['goat_large_success'] = 'pagescontroller/goat_large_success';
$route['goat_medium'] = 'pagescontroller/goat_medium';
$route['goat_medium_pay'] = 'pagescontroller/goat_medium_pay';
$route['goat_medium_success'] = 'pagescontroller/goat_medium_success';
$route['goat_small'] = 'pagescontroller/goat_small';
$route['goat_small_pay'] = 'pagescontroller/goat_small_pay';
$route['goat_small_success'] = 'pagescontroller/goat_small_success';

$route['ram_maxi'] = 'pagescontroller/ram_maxi';
$route['ram_maxi_pay'] = 'pagescontroller/ram_maxi_pay';
$route['ram_maxi_success'] = 'pagescontroller/ram_maxi_success';

$route['ram_standard'] = 'pagescontroller/ram_standard';
$route['ram_standard_pay'] = 'pagescontroller/ram_standard_pay';
$route['ram_standard_success'] = 'pagescontroller/ram_standard_success';

$route['ram_compact'] = 'pagescontroller/ram_compact';
$route['ram_compact_pay'] = 'pagescontroller/ram_compact_pay';
$route['ram_compact_success'] = 'pagescontroller/ram_compact_success';

$route['flash_sales'] = 'pagescontroller/flash_sales';
$route['flash_sales_pay'] = 'pagescontroller/flash_sales_pay';
$route['flash_sales_success'] = 'pagescontroller/flash_sales_success';

$route['verify/(:any)'] = 'pagescontroller/verify/$1';
$route['webhook'] = 'pagescontroller/webhook';

//Special bundles url
$route['easter'] = 'pagescontroller/easter';
$route['sallah'] = 'pagescontroller/sallah';
$route['goatysales'] = 'pagescontroller/goatysales';

// Christmas Sales url to be open during Christmas
//$route['christmasMedium'] = 'pagescontroller/christmasMedium';
//$route['christmasMedium_pay'] = 'pagescontroller/christmasMedium_pay';
//$route['christmasMedium_success'] = 'pagescontroller/christmasMedium_success';
//$route['christmasLarge'] = 'pagescontroller/christmasLarge';
//$route['christmasLarge_pay'] = 'pagescontroller/christmasLarge_pay';
//$route['christmasLarge_success'] = 'pagescontroller/christmasLarge_success';


//Specta Payment
$route['biggie_verify'] = 'specta/biggie_verify';
$route['midi_verify'] = 'specta/midi_verify';
$route['large_verify'] = 'specta/large_verify';

// Specta Christmas sales url

//$route['christmas'] = 'specta/christmas';
//$route['christmas'] = 'specta/christmasLarge';
