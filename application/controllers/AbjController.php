<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AbjController extends CI_Controller
{

	public function abuja()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		$this->load->view('pages/abuja');
		$this->load->view('includes/footer');
	}
	//Abuja package functions start here


	public function abuja_biggie()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load registration view form

		$this->load->helper('form');
		$this->load->helper('url');
		//load registration view form
		//load Model
		$this->load->database();
		$this->load->library('session');

		// validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('abuja/abuja_biggie');
			$this->load->view('includes/footer');
		} else {
			//load Model
			$this->load->model('Referral_model');
			$fname = $this->input->post('name');
			$phone_number = $this->input->post('phone_number');
			$delivery_address = $this->input->post('delivery_address');
			$location = $this->input->post('location');
			$email = $this->input->post('email');
			$plan = "Biggie";
			$bamount = $this->input->post('bamount');
			$amount = '57440';
			$reference = $this->input->post('reference');
			$paystack_status="Pending";
			$referral_code = $this->input->post('referral');
			$code = $this->Referral_model->referral($referral_code);

			if($referral_code== $code) {
				//get percentage for a particular user base on referral code entered
				$query = $this->db->get_where('users',array('referral' => $referral_code));
				$row = $query->row(0);

				$percentage = isset($row->percentage) ? $row->percentage : 0; // access attributes
				//agent commission
				$commission = $amount * $percentage /100;
				$agent_commission = $commission;
				// customer's discount
				$c_per =1;
				$c_discount = $amount * $c_per / 100;
				$discount_amount= $bamount - $c_discount;
				$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,location,phone_number,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$location', '$phone_number','$plan', '$referral_code','$agent_commission', '$c_discount','$bamount','$reference','$paystack_status', 'Not Paid')");
				$this->load->view('abuja/abuja_biggie_pay');
			}

			elseif (empty($referral_code))
			{
				$agent_commission = 0;
				$c_discount = 0;
				$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,location,phone_number,plan, referral, agent_commission, customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$location', '$phone_number','$plan', '$referral_code','$agent_commission','$c_discount','$bamount', '$reference','$paystack_status', 'Not Paid')");
				//redirect('biggie_pay');
				$this->load->view('abuja/abuja_biggie_pay');
			}
			else {
				$this->session->set_flashdata('code', 'Invalid Referral Code');
				$this->load->view('abuja/abuja_biggie');
			}

		}


	}
	public function biggie_pay()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load registration view form
		$this->load->view('abuja/abuja_biggie_pay');
//		$this->load->helper('form');
//		$this->load->helper('url');
		$this->load->database();
		$this->load->view('includes/footer');
	}

	public function abuja_biggiesuccess($biggiesuccess = '')
	{
		$this->load->database();
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load database
		$data['biggiesuccess'] = $biggiesuccess;
		$successfullypaid = $this->input->get('successfullypaid');
		$pref = $this->input->get('pref');
		$fname = $this->input->get('name');
		$phone_number = $this->input->get('phone_number');
		$delivery_address = $this->input->get('delivery_address');
		$location = $this->input->get('location');
		$email = $this->input->get('email');
		$bamount = $this->input->get('bamount');
		$reference = $this->input->get('reference');
		$referral = $this->input->get('referral');
		$status="Success";
		//  $verify = $this->input->get('verify');
		// $this->db->query("INSERT INTO biggie(paystack_reference_meat247) VALUES ('$successfullypaid')");

		$sql="UPDATE meat_sharing SET paystack_reference = '$successfullypaid', paystack_status = '$status'  WHERE  reference='$pref' ";
		$this->db->query($sql);

//agent email

		//send email
		//set up email
		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'admin@livestock247.com', // change it to yours
			'smtp_pass' => 'b861N8cPchr3', // change it to yours
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);

		$to = $email;
		$groupmail = 'esther.akowe@livestock247.com';
		//$groupmail = 'esther.akowe@livestock247.com';

		$message = "
						<html>
						<head>
							<title>New Abuja Meat247 Sales</title>
						</head>
						<body>
							
							<p>Hi $fname, Your payment of NGN $bamount for a Biggie Meat Sharing Package was successful
    						</p>
    						<p>
    						 <strong>Transaction Details:</strong> <br>
							Payment ID: 		$pref<br>
							Name:  				$fname<br>
							Email: 				$email<br>
							Phone: 				$phone_number<br>
							Delivery Address:	$delivery_address<br>
							Location:			$location
						</p>
							
					
					
						</body>
						</html>
						";

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from($config['smtp_user']);
		$this->email->to($to);
		$this->email->cc($groupmail);

		$this->email->subject('Abuja Meat247 Biggie Request');
		$this->email->message($message);



		if ($this->email->send()) {


		}
		//load view
		$this->load->view('abuja/abuja_biggie_success', $data);
		$this->load->view('includes/footer');
	}


	public function abuja_midi()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		$this->load->helper('form');
		$this->load->helper('url');
		//load registration view form
		$this->load->database();
		//load Model
		$this->load->model('Referral_model');
		$this->load->library('session');

		// validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('abuja/abuja_midi');
			$this->load->view('includes/footer');
		} else {
			$fname = $this->input->post('name');
			$phone_number = $this->input->post('phone_number');
			$delivery_address = $this->input->post('delivery_address');
			$location = $this->input->post('location');
			$email = $this->input->post('email');
			$plan = "Midi";
			$midi_amount = $this->input->post('midi_amount');
			$amount = '29730';
			$reference = $this->input->post('reference');
			$paystack_status="Pending";
			$referral_code = $this->input->post('referral');
			$code = $this->Referral_model->referral($referral_code);

			if($referral_code== $code) {
				$query = $this->db->get_where('users',array('referral' => $referral_code));
				$row = $query->row(0);

				$percentage = isset($row->percentage) ? $row->percentage : 0; // access attributes
				//agent commission
				$commission = $amount * $percentage /100;
				$agent_commission = $commission;
				// customer's discount
				$c_per =1;
				$c_discount = $amount *  $c_per / 100;
				$discount_amount= $midi_amount - $c_discount;
				$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,location,phone_number,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status,agent_payment_status) VALUES ('$fname','$email','$delivery_address','$location', '$phone_number','$plan', '$referral_code','$agent_commission', '$c_discount','$midi_amount','$reference','$paystack_status', 'Not Paid')");
				$this->load->view('abuja/abuja_midi_pay');
			}

			elseif (empty($referral_code))
			{
				$agent_commission = 0;
				$c_discount = 0;
				$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,location,phone_number,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address','$location', '$phone_number','$plan', '$referral_code','$agent_commission', '$c_discount','$midi_amount','$reference','$paystack_status', 'Not Paid')");
				//redirect('biggie_pay');
				$this->load->view('abuja/abuja_midi_pay');
			}
			else {
				$this->session->set_flashdata('code', 'Invalid Referral Code');
				$this->load->view('abuja/abuja_midi');
			}

		}

	}

	public function midi_pay()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load registration view form
		$this->load->view('abuja/abuja_midi_pay');
//		$this->load->helper('form');
//		$this->load->helper('url');
		$this->load->database();
		$this->load->view('includes/footer');
	}


	public function abuja_midisuccess($midisuccess = '')
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load database
		$data['midisuccess'] = $midisuccess;
		$successfullypaid = $this->input->get('successfullypaid');
		$pref = $this->input->get('pref');
		$fname = $this->input->get('name');
		$phone_number = $this->input->get('phone_number');
		$delivery_address = $this->input->get('delivery_address');
		$location = $this->input->get('location');
		$email = $this->input->get('email');
		$midi_amount = $this->input->get('midi_amount');
		$reference = $this->input->get('reference');
		$status="Success";
		//  $verify = $this->input->get('verify');
		// $this->db->query("INSERT INTO biggie(paystack_reference_meat247) VALUES ('$successfullypaid')");

		$sql="UPDATE meat_sharing SET paystack_reference = '$successfullypaid', paystack_status = '$status'  WHERE  reference='$pref' ";
		$this->db->query($sql);

//agent email


		//send email
		//set up email
		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'admin@livestock247.com', // change it to yours
			'smtp_pass' => 'b861N8cPchr3', // change it to yours
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);

		$to = $email;
		$groupmail = 'esther.akowe@livestock247.com';
		//$groupmail = 'esther.akowe@livestock247.com';

		$message = "
						<html>
						<head>
							<title>New Abuja Meat247 Sales</title>
						</head>
						<body>
							
							<p>Hi $fname, Your payment of NGN $midi_amount for a Midi Meat Sharing Package was successful
    						</p>
    						<p>
    						 <strong>Transaction Details:</strong> <br>
							Payment ID: 		$pref<br>
							Name:  				$fname<br>
							Email: 				$email<br>
							Phone: 				$phone_number<br>
							Delivery Address:	$delivery_address<br>
							Location:			$location
						</p>
							
					
					
						</body>
						</html>
						";

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from($config['smtp_user']);
		$this->email->to($to);

		$this->email->subject('Abuja Meat247 Midi Request');
		$this->email->message($message);



		if ($this->email->send()) {

		}
		//load view
		$this->load->view('abuja/abuja_midi_success', $data);
		$this->load->view('includes/footer');
	}

	public function abuja_mini()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		$this->load->helper('form');
		$this->load->helper('url');
		//load registration view form
		$this->load->database();
		//load Model
		$this->load->model('Referral_model');
		$this->load->library('session');

		// validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('abuja/abuja_mini');
			$this->load->view('includes/footer');
		} else {
			$fname = $this->input->post('name');
			$phone_number = $this->input->post('phone_number');
			$delivery_address = $this->input->post('delivery_address');
			$location = $this->input->post('location');
			$email = $this->input->post('email');
			$plan = "Mini";
			$mini_amount = $this->input->post('mini_amount');
			$amount = '15900';
			$reference = $this->input->post('reference');
			$paystack_status="Pending";
			$referral_code = $this->input->post('referral');
			$code = $this->Referral_model->referral($referral_code);

			if($referral_code== $code) {
				$query = $this->db->get_where('users',array('referral' => $referral_code));
				$row = $query->row(0);

				$percentage = isset($row->percentage) ? $row->percentage : 0; // access attributes
				//agent commission
				$commission = $amount * $percentage /100;
				$agent_commission = $commission;
				// customer's discount
				$c_per =1;
				$c_discount = $amount * $c_per   / 100;
				$discount_amount= $mini_amount - $c_discount;
				$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,location,phone_number,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status,agent_payment_status) VALUES ('$fname','$email','$delivery_address','$location', '$phone_number','$plan', '$referral_code','$agent_commission', '$c_discount','$mini_amount','$reference','$paystack_status','Not Paid')");
				$this->load->view('abuja/abuja_mini_pay');
			}

			elseif (empty($referral_code))
			{
				$agent_commission = 0;
				$c_discount = 0;
				$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,location,phone_number,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status,agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$location', '$phone_number','$plan', '$referral_code','$agent_commission', '$c_discount','$mini_amount','$reference','$paystack_status', 'Not Paid')");
				//redirect('biggie_pay');
				$this->load->view('abuja/abuja_mini_pay');
			}
			else {
				$this->session->set_flashdata('code', 'Invalid Referral Code');
				$this->load->view('abuja/abuja_mini');
			}

		}
	}

	public function mini_pay()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load registration view form
		$this->load->view('abuja/mini_pay');
//		$this->load->helper('form');
//		$this->load->helper('url');
		$this->load->database();
		$this->load->view('includes/footer');
	}

	public function abuja_minisuccess($minisuccess = '')
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load database
		$data['midisuccess'] = $minisuccess;
		$successfullypaid = $this->input->get('successfullypaid');
		$pref = $this->input->get('pref');
		$fname = $this->input->get('name');
		$phone_number = $this->input->get('phone_number');
		$delivery_address = $this->input->get('delivery_address');
		$location= $this->input->get('location');
		$email = $this->input->get('email');
		$mini_amount = $this->input->get('mini_amount');
		$reference = $this->input->get('reference');
		$referral = $this->input->get('referral');
		$status="Success";
		//  $verify = $this->input->get('verify');
		// $this->db->query("INSERT INTO biggie(paystack_reference_meat247) VALUES ('$successfullypaid')");

		$sql="UPDATE meat_sharing SET paystack_reference = '$successfullypaid', paystack_status = '$status'  WHERE  reference='$pref' ";
		$this->db->query($sql);

		//agent email
		//send email
		//set up email
		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'admin@livestock247.com', // change it to yours
			'smtp_pass' => 'b861N8cPchr3', // change it to yours
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);

		$to = $email;
		$groupmail = 'esther.akowe@livestock247.com';
		//$groupmail = 'esther.akowe@livestock247.com';

		$message = "
						<html>
						<head>
							<title>New Abuja Meat247 Sales</title>
						</head>
						<body>
							
							<p>Hi $fname, Your payment of NGN $mini_amount for a Mini Meat Sharing Package was successful
    						</p>
    						<p>
    						 <strong>Transaction Details:</strong> <br>
							Payment ID: 		$pref<br>
							Name:  				$fname<br>
							Email: 				$email<br>
							Phone: 				$phone_number<br>
							Delivery Address:	$delivery_address<br>
							Location:			$location
						</p>
							
					
					
						</body>
						</html>
						";

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from($config['smtp_user']);
		$this->email->to($to);
		$this->email->cc($groupmail);

		$this->email->subject('Abuja Meat247 Mini Request');
		$this->email->message($message);



		if ($this->email->send()) {

		}
		//load view
		$this->load->view('abuja/abuja_mini_success', $data);
		$this->load->view('includes/footer');
	}


	public function specta(){
		$this->load->database();
		//load Model
		$this->load->model('Plan_Model');
		$this->load->library('session');
		//$this->Plan_Model->specta();
//		$curl = curl_init();
//
//		curl_setopt_array($curl, array(
//			CURLOPT_URL => 'https://paywithspectaapi.sterling.ng/api/Purchase/CreatePaymentUrl',
//			CURLOPT_RETURNTRANSFER => true,
//			CURLOPT_ENCODING => '',
//			CURLOPT_MAXREDIRS => 10,
//			CURLOPT_TIMEOUT => 0,
//			CURLOPT_FOLLOWLOCATION => true,
//			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//			CURLOPT_CUSTOMREQUEST => 'POST',
//			CURLOPT_POSTFIELDS =>'{
//    "callBackUrl": "http",
//    "reference": "test",
//    "merchantId": "174646",
//    "description": "test",
//    "amount": "233333.00"
//}\'',
//			CURLOPT_HTTPHEADER => array(
//				'x-ApiKey: TEST_API_KEY'),
//		));
//
//		$response = curl_exec($curl);
//
//		curl_close($curl);
//		echo $response;

	}

	public function biggiesuccess($biggiesuccess = '')
	{
		$this->load->database();
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load database
		$data['biggiesuccess'] = $biggiesuccess;
		$successfullypaid = $this->input->get('successfullypaid');
		$ref = $this->input->get('reference');
		$fname = $this->input->get('name');
		$phone_number = $this->input->get('phone_number');
		$delivery_address = $this->input->get('delivery_address');
		$location = $this->input->get('location');
		$email = $this->input->get('email');
		$bamount = $this->input->get('bamount');
		//$reference = $this->input->get('reference');
		//$referral = $this->input->get('referral');
		$status="Success";
		//  $verify = $this->input->get('verify');
		// $this->db->query("INSERT INTO biggie(paystack_reference_meat247) VALUES ('$successfullypaid')");

		$sql="UPDATE meat_sharing SET paystack_reference = '$successfullypaid', paystack_status = '$status'  WHERE  reference='$ref' ";
		$this->db->query($sql);

//agent email

		//send email
		//set up email
		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'admin@livestock247.com', // change it to yours
			'smtp_pass' => 'b861N8cPchr3', // change it to yours
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);

		$to = $email;
		$groupmail = 'esther.akowe@livestock247.com';
		//$groupmail = 'esther.akowe@livestock247.com';

		$message = "
						<html>
						<head>
							<title>New Abuja Meat247 Sales</title>
						</head>
						<body>
							
							<p>Hi $fname, Your payment of NGN $bamount for a Biggie Meat Sharing Package was successful
    						</p>
    						<p>
    						 <strong>Transaction Details:</strong> <br>
							Payment ID: 		$ref<br>
							Name:  				$fname<br>
							Email: 				$email<br>
							Phone: 				$phone_number<br>
							Delivery Address:	$delivery_address<br>
							Location:			$location
						</p>
							
					
					
						</body>
						</html>
						";

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from($config['smtp_user']);
		$this->email->to($to);
		$this->email->cc($groupmail);

		$this->email->subject('Abuja Meat247 Biggie Request');
		$this->email->message($message);



		if ($this->email->send()) {


		}
		//load view
		$this->load->view('abuja/abuja_biggie_success', $data);
		$this->load->view('includes/footer');
	}



}
