<?php

class Specta extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Africa/Lagos");
		$this->load->model('meat_model');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->database();

	}
	public function biggie_verify(){
		//load database
		$this->load->database();
		//load Model
		$this->load->model('meat_model');
		//load helper
		$this->load->helper('url');
		$this->load->helper('date');
		//load library
		$this->load->library('pagination');
		$this->load->library('table');
		$this->load->library('calendar');
		$this->load->library('encrypt');
		$fname = $this->input->get('name');
		$phone_number = $this->input->get('phone_number');
		$delivery_address = $this->input->get('delivery_address');
		$delivery_date = $this->input->get('delivery_date');
		$location = $this->input->get('location');
		$email = $this->input->get('email');
		$bamount = $this->input->get('bamount');

		// get specta reference
		$reference = $this->input->get('ref', TRUE);
		//echo $reference;

		$data = array(
			"verificationToken" => $reference
		);
		$post_data = json_encode($data);
		// get payment verification
		$crl = curl_init('https://paywithspectaapi.sterling.ng/api/Purchase/VerifyPurchase');
		curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($crl, CURLINFO_HEADER_OUT, true);
		curl_setopt($crl, CURLOPT_POST, true);
		curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);

		// Set HTTP Header for POST request
		// TEST_API_KEY
		// 969688b48dad4b8981be20fc035da527
		curl_setopt($crl, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'x-ApiKey: 969688b48dad4b8981be20fc035da527',
				'Content-Length: ' . strlen($post_data))
		);

		$response = curl_exec($crl);
		curl_close($crl);

		$better_response = json_decode($response);
		//print_r($better_response);
		//echo $response;
		$ref = $better_response->result->data->reference;
		$query = $this->db->query("SELECT cname, amount, email, phone_number, plan, delivery_address, process, delivery_date FROM meat_sharing WHERE reference = '$ref'");
		$results = $query->result();

		foreach ($results as $row)
		{
//			echo $row->cname;
//			echo $row->email;
//			echo $row->phone_number;
//			echo $row->plan;
//			echo $row->amount;
//			echo $row->process;
//			echo $row->delivery_address;
//			echo $row->delivery_date;
		}

		if (isset($better_response->result->data->isSuccessful) && $better_response->result->data->isSuccessful == true) {
			//echo $better_response->result->data->reference;
			//update specta reference into db
			$meat['specta_reference'] = $better_response->result->data->paymentReference;
			$meat['specta_status']= 'Success';
			$meat['specta_payment'] = $better_response->result->data->amountSettled;
			$this->db->where('reference', $better_response->result->data->reference);
			$this->db->update('meat_sharing', $meat);
			$ref = $better_response->result->data->reference;
			$aa = $better_response->result->data->amount;
			//get data from table and send email

			//set up email
			$config = array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.gmail.com',
				'smtp_port' => 465,
				'smtp_user' => 'admin@livestock247.com', // change it to yours
				'smtp_pass' => 'b861N8cPchr3', // change it to yours
				'mailtype' => 'html',
				'charset' => 'iso-8859-1',
				'wordwrap' => TRUE
			);

			//$to = $email;
			$groupmail = 'meat247@livestock247.com';
			//$groupmail = 'esther.akowe@livestock247.com';

			$message = " 
						<html>
						<head>
							<title>Meat247 Sales</title>
						</head>
						<body>
							<h3>Meat247- Biggie Payment Via Specta</h3>
							<p>
							$row->cname has paid N$row->amount Via Specta<br><br>
    						Email: 				$row->email <br>
    						Phone:				$row->phone_number <br>
    						Payment reference: 	$ref<br>
    						Package: 			$row->plan <br>
    						Process:			$row->process <br>
    						Delivery:			$row->delivery_address <br>
    						Delivery Date:		$row->delivery_date <br>
    					
							<a href='" . site_url() . "admin' class='btn btn-sm'></a>
							<a href='https://livestock247.com/sales_agent/login' class='text-primary'> Click here to see payment details</a>
    						</p>
    					
						</body>
						</html>
						";

			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from($config['smtp_user']);
			$this->email->to($groupmail);
			//$this->email->cc($groupmail);
			$this->email->subject('Meat247 - Biggie Payment Via Specta');
			$this->email->message($message);


			if ($this->email->send()) {

			}
			//$this->load->view('admin/payment_verify');
			echo '<script>alert("Your Payment Was Successful. A member of our team will contact you.")</script>';
			$this->output->set_header('refresh:2;'.site_url());

		}
		if (isset($better_response->result->data->isSuccessful) && $better_response->result->data->isSuccessful == false) {
			//echo  $better_response->result->data->amountSettled;
			$meat['specta_reference'] = $better_response->result->data->paymentReference;
			$meat['specta_status']= 'Failed';
			$this->db->where('reference', $better_response->result->data->reference);
			$this->db->update('meat_sharing', $meat);
			// Display the alert box
			echo '<script>alert("Payment Failed.")</script>';
			$this->output->set_header('refresh:2;'.site_url());
		}

	}//biggie


	public function midi_verify(){
		//load database
		$this->load->database();
		//load Model
		$this->load->model('meat_model');
		//load helper
		$this->load->helper('url');
		$this->load->helper('date');
		//load library
		$this->load->library('pagination');
		$this->load->library('table');
		$this->load->library('calendar');
		$this->load->library('encrypt');
		// get specta reference
		$reference = $this->input->get('ref', TRUE);
		//echo $reference;

		$data = array(
			"verificationToken" => $reference
		);
		$post_data = json_encode($data);
		// get payment verification
		$crl = curl_init('https://paywithspectaapi.sterling.ng/api/Purchase/VerifyPurchase');
		curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($crl, CURLINFO_HEADER_OUT, true);
		curl_setopt($crl, CURLOPT_POST, true);
		curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);

		// Set HTTP Header for POST request
		// TEST_API_KEY
		// 969688b48dad4b8981be20fc035da527
		curl_setopt($crl, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'x-ApiKey: 969688b48dad4b8981be20fc035da527',
				'Content-Length: ' . strlen($post_data))
		);

		$response = curl_exec($crl);
		curl_close($crl);

		$better_response = json_decode($response);
		//print_r($better_response);
		//echo $response;
		$ref = $better_response->result->data->reference;
		$query = $this->db->query("SELECT cname, amount, email, phone_number, plan, delivery_address, process, delivery_date FROM meat_sharing WHERE reference = '$ref'");
		$results = $query->result();

		foreach ($results as $row)
		{
//			echo $row->cname;
//			echo $row->email;
//			echo $row->phone_number;
//			echo $row->plan;
//			echo $row->amount;
//			echo $row->process;
//			echo $row->delivery_address;
//			echo $row->delivery_date;
		}

		if (isset($better_response->result->data->isSuccessful) && $better_response->result->data->isSuccessful == true) {
			//echo $better_response->result->data->reference;
			//echo $response;
			//update specta reference into db
			$meat['specta_reference'] = $better_response->result->data->paymentReference;
			$meat['specta_status']= 'Success';
			$meat['specta_payment'] = $better_response->result->data->amountSettled;
			$this->db->where('reference', $better_response->result->data->reference);
			$this->db->update('meat_sharing', $meat);
			$ref = $better_response->result->data->reference;
			$aa = $better_response->result->data->amount;
			//get data from table and send email

			//set up email
			$config = array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.gmail.com',
				'smtp_port' => 465,
				'smtp_user' => 'admin@livestock247.com', // change it to yours
				'smtp_pass' => 'b861N8cPchr3', // change it to yours
				'mailtype' => 'html',
				'charset' => 'iso-8859-1',
				'wordwrap' => TRUE
			);

			//$to = $email;
			$groupmail = 'meat247@livestock247.com';
			//$groupmail = 'esther.akowe@livestock247.com';

			$message = " 
						<html>
						<head>
							<title>Meat247 Sales</title>
						</head>
						<body>
							<h3>Meat247- Midi Payment Via Specta</h3>
							<p>
							$row->cname has paid N$row->amount Via Specta<br><br>
    						Email: 				$row->email <br>
    						Phone:				$row->phone_number <br>
    						Payment reference: 	$ref<br>
    						Package: 			$row->plan <br>
    						Process:			$row->process <br>
    						Delivery:			$row->delivery_address <br>
    						Delivery Date:		$row->delivery_date <br>
    						
    						
							<a href='" . site_url() . "admin' class='btn btn-sm'></a>
							<a href='https://livestock247.com/sales_agent/specta' class='text-primary'> Click here to see payment details</a>
    						</p>
    					
						</body>
						</html>
						";

			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from($config['smtp_user']);
			$this->email->to($groupmail);
			//$this->email->cc($groupmail);
			$this->email->subject('Meat247 - Midi Payment Via Specta');
			$this->email->message($message);

			if ($this->email->send()) {

			}
			//$this->load->view('admin/payment_verify');
			echo '<script>alert("Your Payment Was Successful. A member of our team will contact you.")</script>';
			$this->output->set_header('refresh:2;'.site_url());

		}
		if (isset($better_response->result->data->isSuccessful) && $better_response->result->data->isSuccessful == false) {
			//echo  $better_response->result->data->amountSettled;
			$meat['specta_reference'] = $better_response->result->data->paymentReference;
			$meat['specta_status']= 'Failed';
			$this->db->where('reference', $better_response->result->data->reference);
			$this->db->update('meat_sharing', $meat);
			// Display the alert box
			echo '<script>alert("Payment Failed.")</script>';
			$this->output->set_header('refresh:2;'.site_url());
		}

	}//Midi


	public function large_verify(){
		//load database
		$this->load->database();
		//load Model
		$this->load->model('meat_model');
		//load helper
		$this->load->helper('url');
		$this->load->helper('date');
		//load library
		$this->load->library('pagination');
		$this->load->library('table');
		$this->load->library('calendar');
		$this->load->library('encrypt');
		$fname = $this->input->get('name');
		$phone_number = $this->input->get('phone_number');
		$delivery_address = $this->input->get('delivery_address');
		$delivery_date = $this->input->get('delivery_date');
		$location = $this->input->get('location');
		$email = $this->input->get('email');
		$bamount = $this->input->get('goat_amount');

		// get specta reference
		$reference = $this->input->get('ref', TRUE);
		//echo $reference;

		$data = array(
			"verificationToken" => $reference
		);
		$post_data = json_encode($data);
		// get payment verification
		$crl = curl_init('https://paywithspectaapi.sterling.ng/api/Purchase/VerifyPurchase');
		curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($crl, CURLINFO_HEADER_OUT, true);
		curl_setopt($crl, CURLOPT_POST, true);
		curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);

		// Set HTTP Header for POST request
		// TEST_API_KEY
		// 969688b48dad4b8981be20fc035da527
		curl_setopt($crl, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'x-ApiKey: 969688b48dad4b8981be20fc035da527',
				'Content-Length: ' . strlen($post_data))
		);

		$response = curl_exec($crl);
		curl_close($crl);

		$better_response = json_decode($response);
		//print_r($better_response);
		//echo $response;

		$ref = $better_response->result->data->reference;
		$query = $this->db->query("SELECT cname, amount, email, phone_number, plan, delivery_address, process, delivery_date FROM meat_sharing WHERE reference = '$ref'");
		$results = $query->result();

		foreach ($results as $row)
		{
//			echo $row->cname;
//			echo $row->email;
//			echo $row->phone_number;
//			echo $row->plan;
//			echo $row->amount;
//			echo $row->process;
//			echo $row->delivery_address;
//			echo $row->delivery_date;
		}
		if (isset($better_response->result->data->isSuccessful) && $better_response->result->data->isSuccessful == true) {
			//echo $better_response->result->data->reference;
			//echo $response;

			//update specta reference into db
			$meat['specta_reference'] = $better_response->result->data->paymentReference;
			$meat['specta_status']= 'Success';
			$meat['specta_payment'] = $better_response->result->data->amountSettled;
			$this->db->where('reference', $better_response->result->data->reference);
			$this->db->update('meat_sharing', $meat);
			$ref = $better_response->result->data->reference;
			$aa = $better_response->result->data->amount;
			//get data from table and send email

			//set up email
			$config = array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.gmail.com',
				'smtp_port' => 465,
				'smtp_user' => 'admin@livestock247.com', // change it to yours
				'smtp_pass' => 'b861N8cPchr3', // change it to yours
				'mailtype' => 'html',
				'charset' => 'iso-8859-1',
				'wordwrap' => TRUE
			);

			//$to = $email;
			$groupmail = 'meat247@livestock247.com';
			//$groupmail = 'esther.akowe@livestock247.com';

			$message = " 
						<html>
						<head>
							<title>Meat247 Sales</title>
						</head>
						<body>
						<h3>Meat247- Large Goat Payment Via Specta</h3>
							<p>
							$row->cname has paid N$row->amount Via Specta<br><br>
    						Email: 				$row->email <br>
    						Phone:				$row->phone_number <br>
    						Payment reference: 	$ref<br>
    						Package: 			$row->plan <br>
    						Process:			$row->process <br>
    						Delivery:			$row->delivery_address <br>
    						Delivery Date:		$row->delivery_date <br>
    						
							<a href='" . site_url() . "admin' class='btn btn-sm'></a>
							<a href='https://livestock247.com/sales_agent/specta' class='text-primary'> Click here to see payment details</a>
    						</p>
    					
						</body>
						</html>
						";

			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from($config['smtp_user']);
			$this->email->to($groupmail);
			//$this->email->cc($groupmail);
			$this->email->subject('Meat247 - Large Goat Payment Via Specta');
			$this->email->message($message);


			if ($this->email->send()) {

			}
			//$this->load->view('admin/payment_verify');
			echo '<script>alert("Your Payment Was Successful. A member of our team will contact you.")</script>';
			$this->output->set_header('refresh:2;'.site_url());

		}
		if (isset($better_response->result->data->isSuccessful) && $better_response->result->data->isSuccessful == false) {
			//echo  $better_response->result->data->amountSettled;
			$meat['specta_reference'] = $better_response->result->data->paymentReference;
			$meat['specta_status']= 'Failed';
			$this->db->where('reference', $better_response->result->data->reference);
			$this->db->update('meat_sharing', $meat);

			// Display the alert box
			echo '<script>alert("Payment Failed.")</script>';
			$this->output->set_header('refresh:2;'.site_url());
		}
	}

	public function medium_verify(){
		//load database
		$this->load->database();
		//load Model
		$this->load->model('meat_model');
		//load helper
		$this->load->helper('url');
		$this->load->helper('date');
		//load library
		$this->load->library('pagination');
		$this->load->library('table');
		$this->load->library('calendar');
		$this->load->library('encrypt');
		$fname = $this->input->get('name');
		$phone_number = $this->input->get('phone_number');
		$delivery_address = $this->input->get('delivery_address');
		$delivery_date = $this->input->get('delivery_date');
		$location = $this->input->get('location');
		$email = $this->input->get('email');
		$bamount = $this->input->get('goat_amount');

		// get specta reference
		$reference = $this->input->get('ref', TRUE);
		//echo $reference;

		$data = array(
			"verificationToken" => $reference
		);
		$post_data = json_encode($data);
		// get payment verification
		$crl = curl_init('https://paywithspectaapi.sterling.ng/api/Purchase/VerifyPurchase');
		curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($crl, CURLINFO_HEADER_OUT, true);
		curl_setopt($crl, CURLOPT_POST, true);
		curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);

		// Set HTTP Header for POST request
		// TEST_API_KEY
		// 969688b48dad4b8981be20fc035da527
		curl_setopt($crl, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'x-ApiKey: 969688b48dad4b8981be20fc035da527',
				'Content-Length: ' . strlen($post_data))
		);

		$response = curl_exec($crl);
		curl_close($crl);

		$better_response = json_decode($response);
		//print_r($better_response);
		//echo $response;
		$ref = $better_response->result->data->reference;

		$query = $this->db->query("SELECT cname, amount, email, phone_number, plan, delivery_address, process, delivery_date FROM meat_sharing WHERE reference = '$ref'");
		$results = $query->result();

		foreach ($results as $row)
		{
//			echo $row->cname;
//			echo $row->email;
//			echo $row->phone_number;
//			echo $row->plan;
//			echo $row->amount;
//			echo $row->process;
//			echo $row->delivery_address;
//			echo $row->delivery_date;
		}

		if (isset($better_response->result->data->isSuccessful) && $better_response->result->data->isSuccessful == true) {

			//update specta reference into db
			$meat['specta_reference'] = $better_response->result->data->paymentReference;
			$meat['specta_status']= 'Success';
			$meat['specta_payment'] = $better_response->result->data->amountSettled;
			$this->db->where('reference', $better_response->result->data->reference);
			$this->db->update('meat_sharing', $meat);
			$ref = $better_response->result->data->reference;
			$aa = $better_response->result->data->amount;
			//get data from table and send email

			//set up email
			$config = array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.gmail.com',
				'smtp_port' => 465,
				'smtp_user' => 'admin@livestock247.com', // change it to yours
				'smtp_pass' => 'b861N8cPchr3', // change it to yours
				'mailtype' => 'html',
				'charset' => 'iso-8859-1',
				'wordwrap' => TRUE
			);

			//$to = $email;
			$groupmail = 'meat247@livestock247.com';
			//$groupmail = 'esther.akowe@livestock247.com';

			$message = " 
						<html>
						<head>
							<title>Meat247 Sales</title>
						</head>
						<body>
						<h3>Meat247- Medium Goat Payment Via Specta</h3>
							<p>
							$row->cname has paid N$row->amount Via Specta<br><br>
    						Email: 				$row->email <br>
    						Phone:				$row->phone_number <br>
    						Payment reference: 	$ref<br>
    						Package: 			$row->plan <br>
    						Process:			$row->process <br>
    						Delivery:			$row->delivery_address <br>
    						Delivery Date:		$row->delivery_date <br>
    						
							<a href='" . site_url() . "admin' class='btn btn-sm'></a>
							<a href='https://livestock247.com/sales_agent/specta' class='text-primary'> Click here to see payment details</a>
    						</p>
    					
						</body>
						</html>
						";

			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from($config['smtp_user']);
			$this->email->to($groupmail);
			//$this->email->cc($groupmail);
			$this->email->subject('Meat247 - Medium Goat Payment Via Specta');
			$this->email->message($message);


			if ($this->email->send()) {

			}
			//$this->load->view('admin/payment_verify');
			echo '<script>alert("Your Payment Was Successful. A member of our team will contact you.")</script>';
			$this->output->set_header('refresh:2;'.site_url());

		}
		if (isset($better_response->result->data->isSuccessful) && $better_response->result->data->isSuccessful == false) {
			//echo  $better_response->result->data->amountSettled;
			$meat['specta_reference'] = $better_response->result->data->paymentReference;
			$meat['specta_status']= 'Failed';
			$this->db->where('reference', $better_response->result->data->reference);
			$this->db->update('meat_sharing', $meat);

			// Display the alert box
			echo '<script>alert("Payment Failed.")</script>';
			$this->output->set_header('refresh:2;'.site_url());
		}
	}


	public function maxi_verify(){
		//load database
		$this->load->database();
		//load Model
		$this->load->model('meat_model');
		//load helper
		$this->load->helper('url');
		$this->load->helper('date');
		//load library
		$this->load->library('pagination');
		$this->load->library('table');
		$this->load->library('calendar');
		$this->load->library('encrypt');

		// get specta reference
		$reference = $this->input->get('ref', TRUE);
		//echo $reference;

		$data = array(
			"verificationToken" => $reference
		);
		$post_data = json_encode($data);
		// get payment verification
		$crl = curl_init('https://paywithspectaapi.sterling.ng/api/Purchase/VerifyPurchase');
		curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($crl, CURLINFO_HEADER_OUT, true);
		curl_setopt($crl, CURLOPT_POST, true);
		curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);

		// Set HTTP Header for POST request
		// TEST_API_KEY
		// 969688b48dad4b8981be20fc035da527
		curl_setopt($crl, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'x-ApiKey: 969688b48dad4b8981be20fc035da527',
				'Content-Length: ' . strlen($post_data))
		);

		$response = curl_exec($crl);
		curl_close($crl);

		$better_response = json_decode($response);
		//print_r($better_response);
		//echo $response;
		$ref = $better_response->result->data->reference;

		$query = $this->db->query("SELECT cname, amount, email, phone_number, plan, delivery_address, process, delivery_date FROM meat_sharing WHERE reference = '$ref'");
		$results = $query->result();

		foreach ($results as $row)
		{
//			echo $row->cname;
//			echo $row->email;
//			echo $row->phone_number;
//			echo $row->plan;
//			echo $row->amount;
//			echo $row->process;
//			echo $row->delivery_address;
//			echo $row->delivery_date;
		}

		if (isset($better_response->result->data->isSuccessful) && $better_response->result->data->isSuccessful == true) {

			//update specta reference into db
			$meat['specta_reference'] = $better_response->result->data->paymentReference;
			$meat['specta_status']= 'Success';
			$meat['specta_payment'] = $better_response->result->data->amountSettled;
			$this->db->where('reference', $better_response->result->data->reference);
			$this->db->update('meat_sharing', $meat);
			$ref = $better_response->result->data->reference;
			$aa = $better_response->result->data->amount;
			//get data from table and send email

			//set up email
			$config = array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.gmail.com',
				'smtp_port' => 465,
				'smtp_user' => 'admin@livestock247.com', // change it to yours
				'smtp_pass' => 'b861N8cPchr3', // change it to yours
				'mailtype' => 'html',
				'charset' => 'iso-8859-1',
				'wordwrap' => TRUE
			);

			//$to = $email;
			$groupmail = 'meat247@livestock247.com';
			//$groupmail = 'esther.akowe@livestock247.com';

			$message = " 
						<html>
						<head>
							<title>Meat247 Sales</title>
						</head>
						<body>
						<h3>Meat247- Maxi Ram Payment Via Specta</h3>
							<p>
							$row->cname has paid N$row->amount Via Specta<br><br>
    						Email: 				$row->email <br>
    						Phone:				$row->phone_number <br>
    						Payment reference: 	$ref<br>
    						Package: 			$row->plan <br>
    						Process:			$row->process <br>
    						Delivery:			$row->delivery_address <br>
    						Delivery Date:		$row->delivery_date <br>
    						
							<a href='" . site_url() . "admin' class='btn btn-sm'></a>
							<a href='https://livestock247.com/sales_agent/specta' class='text-primary'> Click here to see payment details</a>
    						</p>
    					
						</body>
						</html>
						";

			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from($config['smtp_user']);
			$this->email->to($groupmail);
			//$this->email->cc($groupmail);
			$this->email->subject('Meat247 - Maxi Ram Payment Via Specta');
			$this->email->message($message);


			if ($this->email->send()) {

			}
			//$this->load->view('admin/payment_verify');
			echo '<script>alert("Your Payment Was Successful. A member of our team will contact you.")</script>';
			$this->output->set_header('refresh:2;'.site_url());

		}
		if (isset($better_response->result->data->isSuccessful) && $better_response->result->data->isSuccessful == false) {
			//echo  $better_response->result->data->amountSettled;
			$meat['specta_reference'] = $better_response->result->data->paymentReference;
			$meat['specta_status']= 'Failed';
			$this->db->where('reference', $better_response->result->data->reference);
			$this->db->update('meat_sharing', $meat);

			// Display the alert box
			echo '<script>alert("Payment Failed.")</script>';
			$this->output->set_header('refresh:2;'.site_url());
		}

	}


	public function standard_verify(){
		//load database
		$this->load->database();
		//load Model
		$this->load->model('meat_model');
		//load helper
		$this->load->helper('url');
		$this->load->helper('date');
		//load library
		$this->load->library('pagination');
		$this->load->library('table');
		$this->load->library('calendar');
		$this->load->library('encrypt');

		// get specta reference
		$reference = $this->input->get('ref', TRUE);
		//echo $reference;

		$data = array(
			"verificationToken" => $reference
		);
		$post_data = json_encode($data);
		// get payment verification
		$crl = curl_init('https://paywithspectaapi.sterling.ng/api/Purchase/VerifyPurchase');
		curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($crl, CURLINFO_HEADER_OUT, true);
		curl_setopt($crl, CURLOPT_POST, true);
		curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);

		// Set HTTP Header for POST request
		// TEST_API_KEY
		// 969688b48dad4b8981be20fc035da527
		curl_setopt($crl, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'x-ApiKey: 969688b48dad4b8981be20fc035da527',
				'Content-Length: ' . strlen($post_data))
		);

		$response = curl_exec($crl);
		curl_close($crl);

		$better_response = json_decode($response);
		//print_r($better_response);
		//echo $response;
		$ref = $better_response->result->data->reference;

		$query = $this->db->query("SELECT cname, amount, email, phone_number, plan, delivery_address, process, delivery_date FROM meat_sharing WHERE reference = '$ref'");
		$results = $query->result();

		foreach ($results as $row)
		{
//			echo $row->cname;
//			echo $row->email;
//			echo $row->phone_number;
//			echo $row->plan;
//			echo $row->amount;
//			echo $row->process;
//			echo $row->delivery_address;
//			echo $row->delivery_date;
		}

		if (isset($better_response->result->data->isSuccessful) && $better_response->result->data->isSuccessful == true) {

			//update specta reference into db
			$meat['specta_reference'] = $better_response->result->data->paymentReference;
			$meat['specta_status']= 'Success';
			$meat['specta_payment'] = $better_response->result->data->amountSettled;
			$this->db->where('reference', $better_response->result->data->reference);
			$this->db->update('meat_sharing', $meat);
			$ref = $better_response->result->data->reference;
			$aa = $better_response->result->data->amount;
			//get data from table and send email

			//set up email
			$config = array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.gmail.com',
				'smtp_port' => 465,
				'smtp_user' => 'admin@livestock247.com', // change it to yours
				'smtp_pass' => 'b861N8cPchr3', // change it to yours
				'mailtype' => 'html',
				'charset' => 'iso-8859-1',
				'wordwrap' => TRUE
			);

			//$to = $email;
			$groupmail = 'meat247@livestock247.com';
			//$groupmail = 'esther.akowe@livestock247.com';

			$message = " 
						<html>
						<head>
							<title>Meat247 Sales</title>
						</head>
						<body>
						<h3>Meat247- Standard Ram Payment Via Specta</h3>
							<p>
							$row->cname has paid N$row->amount Via Specta<br><br>
    						Email: 				$row->email <br>
    						Phone:				$row->phone_number <br>
    						Payment reference: 	$ref<br>
    						Package: 			$row->plan <br>
    						Process:			$row->process <br>
    						Delivery:			$row->delivery_address <br>
    						Delivery Date:		$row->delivery_date <br>
    						
							<a href='" . site_url() . "admin' class='btn btn-sm'></a>
							<a href='https://livestock247.com/sales_agent/specta' class='text-primary'> Click here to see payment details</a>
    						</p>
    					
						</body>
						</html>
						";

			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from($config['smtp_user']);
			$this->email->to($groupmail);
			//$this->email->cc($groupmail);
			$this->email->subject('Meat247 - Standard Ram Payment Via Specta');
			$this->email->message($message);


			if ($this->email->send()) {

			}
			//$this->load->view('admin/payment_verify');
			echo '<script>alert("Your Payment Was Successful. A member of our team will contact you.")</script>';
			$this->output->set_header('refresh:2;'.site_url());

		}
		if (isset($better_response->result->data->isSuccessful) && $better_response->result->data->isSuccessful == false) {
			//echo  $better_response->result->data->amountSettled;
			$meat['specta_reference'] = $better_response->result->data->paymentReference;
			$meat['specta_status']= 'Failed';
			$this->db->where('reference', $better_response->result->data->reference);
			$this->db->update('meat_sharing', $meat);

			// Display the alert box
			echo '<script>alert("Payment Failed.")</script>';
			$this->output->set_header('refresh:2;'.site_url());
		}

	}


	public function compact_verify(){
		//load database
		$this->load->database();
		//load Model
		$this->load->model('meat_model');
		//load helper
		$this->load->helper('url');
		$this->load->helper('date');
		//load library
		$this->load->library('pagination');
		$this->load->library('table');
		$this->load->library('calendar');
		$this->load->library('encrypt');

		// get specta reference
		$reference = $this->input->get('ref', TRUE);
		//echo $reference;

		$data = array(
			"verificationToken" => $reference
		);
		$post_data = json_encode($data);
		// get payment verification
		$crl = curl_init('https://paywithspectaapi.sterling.ng/api/Purchase/VerifyPurchase');
		curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($crl, CURLINFO_HEADER_OUT, true);
		curl_setopt($crl, CURLOPT_POST, true);
		curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);

		// Set HTTP Header for POST request
		// TEST_API_KEY
		// 969688b48dad4b8981be20fc035da527
		curl_setopt($crl, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'x-ApiKey: 969688b48dad4b8981be20fc035da527',
				'Content-Length: ' . strlen($post_data))
		);

		$response = curl_exec($crl);
		curl_close($crl);

		$better_response = json_decode($response);
		//print_r($better_response);
		//echo $response;
		$ref = $better_response->result->data->reference;

		$query = $this->db->query("SELECT cname, amount, email, phone_number, plan, delivery_address, process, delivery_date FROM meat_sharing WHERE reference = '$ref'");
		$results = $query->result();

		foreach ($results as $row)
		{
//			echo $row->cname;
//			echo $row->email;
//			echo $row->phone_number;
//			echo $row->plan;
//			echo $row->amount;
//			echo $row->process;
//			echo $row->delivery_address;
//			echo $row->delivery_date;
		}

		if (isset($better_response->result->data->isSuccessful) && $better_response->result->data->isSuccessful == true) {

			//update specta reference into db
			$meat['specta_reference'] = $better_response->result->data->paymentReference;
			$meat['specta_status']= 'Success';
			$meat['specta_payment'] = $better_response->result->data->amountSettled;
			$this->db->where('reference', $better_response->result->data->reference);
			$this->db->update('meat_sharing', $meat);
			$ref = $better_response->result->data->reference;
			$aa = $better_response->result->data->amount;
			//get data from table and send email

			//set up email
			$config = array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.gmail.com',
				'smtp_port' => 465,
				'smtp_user' => 'admin@livestock247.com', // change it to yours
				'smtp_pass' => 'b861N8cPchr3', // change it to yours
				'mailtype' => 'html',
				'charset' => 'iso-8859-1',
				'wordwrap' => TRUE
			);

			//$to = $email;
			$groupmail = 'meat247@livestock247.com';
			//$groupmail = 'esther.akowe@livestock247.com';

			$message = " 
						<html>
						<head>
							<title>Meat247 Sales</title>
						</head>
						<body>
						<h3>Meat247- Compact Ram Payment Via Specta</h3>
							<p>
							$row->cname has paid N$row->amount Via Specta<br><br>
    						Email: 				$row->email <br>
    						Phone:				$row->phone_number <br>
    						Payment reference: 	$ref<br>
    						Package: 			$row->plan <br>
    						Process:			$row->process <br>
    						Delivery:			$row->delivery_address <br>
    						Delivery Date:		$row->delivery_date <br>
    						
							<a href='" . site_url() . "admin' class='btn btn-sm'></a>
							<a href='https://livestock247.com/sales_agent/specta' class='text-primary'> Click here to see payment details</a>
    						</p>
    					
						</body>
						</html>
						";

			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from($config['smtp_user']);
			$this->email->to($groupmail);
			//$this->email->cc($groupmail);
			$this->email->subject('Meat247 - Compact Ram Payment Via Specta');
			$this->email->message($message);


			if ($this->email->send()) {

			}
			//$this->load->view('admin/payment_verify');
			echo '<script>alert("Your Payment Was Successful. A member of our team will contact you.")</script>';
			$this->output->set_header('refresh:2;'.site_url());

		}
		if (isset($better_response->result->data->isSuccessful) && $better_response->result->data->isSuccessful == false) {
			//echo  $better_response->result->data->amountSettled;
			$meat['specta_reference'] = $better_response->result->data->paymentReference;
			$meat['specta_status']= 'Failed';
			$this->db->where('reference', $better_response->result->data->reference);
			$this->db->update('meat_sharing', $meat);

			// Display the alert box
			echo '<script>alert("Payment Failed.")</script>';
			$this->output->set_header('refresh:2;'.site_url());
		}

	}

	public function christmas(){
		//load database
		$this->load->database();
		//load Model
		$this->load->model('meat_model');
		//load helper
		$this->load->helper('url');
		$this->load->helper('date');
		//load library
		$this->load->library('pagination');
		$this->load->library('table');
		$this->load->library('calendar');
		$this->load->library('encrypt');
		$fname = $this->input->get('name');
		$phone_number = $this->input->get('phone_number');
		$delivery_address = $this->input->get('delivery_address');
		$delivery_date = $this->input->get('delivery_date');
		$location = $this->input->get('location');
		$email = $this->input->get('email');
		$bamount = $this->input->get('bamount');

		// get specta reference
		$reference = $this->input->get('ref', TRUE);
		//echo $reference;

		$data = array(
			"verificationToken" => $reference
		);
		$post_data = json_encode($data);
		// get payment verification
		$crl = curl_init('https://paywithspectaapi.sterling.ng/api/Purchase/VerifyPurchase');
		curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($crl, CURLINFO_HEADER_OUT, true);
		curl_setopt($crl, CURLOPT_POST, true);
		curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);

		// Set HTTP Header for POST request
		// TEST_API_KEY
		// 969688b48dad4b8981be20fc035da527
		curl_setopt($crl, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'x-ApiKey: 969688b48dad4b8981be20fc035da527',
				'Content-Length: ' . strlen($post_data))
		);

		$response = curl_exec($crl);
		curl_close($crl);

		$better_response = json_decode($response);
		//print_r($better_response);
		//echo $response;
		$ref = $better_response->result->data->reference;
		$query = $this->db->query("SELECT cname, amount, email, phone_number, plan, delivery_address, process, delivery_date FROM meat_sharing WHERE reference = '$ref'");
		$results = $query->result();

		foreach ($results as $row)
		{
//			echo $row->cname;
//			echo $row->email;
//			echo $row->phone_number;
//			echo $row->plan;
//			echo $row->amount;
//			echo $row->process;
//			echo $row->delivery_address;
//			echo $row->delivery_date;
		}

		if (isset($better_response->result->data->isSuccessful) && $better_response->result->data->isSuccessful == true) {
			//echo $better_response->result->data->reference;
			//update specta reference into db
			$meat['specta_reference'] = $better_response->result->data->paymentReference;
			$meat['specta_status']= 'Success';
			$meat['specta_payment'] = $better_response->result->data->amountSettled;
			$this->db->where('reference', $better_response->result->data->reference);
			$this->db->update('meat_sharing', $meat);
			$ref = $better_response->result->data->reference;
			$aa = $better_response->result->data->amount;
			//get data from table and send email

			//set up email
			$config = array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.gmail.com',
				'smtp_port' => 465,
				'smtp_user' => 'admin@livestock247.com', // change it to yours
				'smtp_pass' => 'b861N8cPchr3', // change it to yours
				'mailtype' => 'html',
				'charset' => 'iso-8859-1',
				'wordwrap' => TRUE
			);

			//$to = $email;
			$groupmail = 'meat247@livestock247.com';
			//$groupmail = 'esther.akowe@livestock247.com';

			$message = " 
						<html>
						<head>
							<title>Meat247 Sales</title>
						</head>
						<body>
							<h3>Meat247- Christmas Promo Payment Via Specta</h3>
							<p>
							$row->cname has paid N$row->amount Via Specta<br><br>
    						Email: 				$row->email <br>
    						Phone:				$row->phone_number <br>
    						Payment reference: 	$ref<br>
    						Package: 			$row->plan <br>
    						Process:			$row->process <br>
    						Delivery:			$row->delivery_address <br>
    						Delivery Date:		$row->delivery_date <br>
    					
							<a href='" . site_url() . "admin' class='btn btn-sm'></a>
							<a href='https://livestock247.com/sales_agent/login' class='text-primary'> Click here to see payment details</a>
    						</p>
    					
						</body>
						</html>
						";

			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from($config['smtp_user']);
			$this->email->to($groupmail);
			//$this->email->cc($groupmail);
			$this->email->subject('Meat247 - Christmas Promo Payment Via Specta');
			$this->email->message($message);


			if ($this->email->send()) {

			}
			//$this->load->view('admin/payment_verify');
			echo '<script>alert("Your Payment Was Successful. A member of our team will contact you.")</script>';
			$this->output->set_header('refresh:2;'.site_url());

		}
		if (isset($better_response->result->data->isSuccessful) && $better_response->result->data->isSuccessful == false) {
			//echo  $better_response->result->data->amountSettled;
			$meat['specta_reference'] = $better_response->result->data->paymentReference;
			$meat['specta_status']= 'Failed';
			$this->db->where('reference', $better_response->result->data->reference);
			$this->db->update('meat_sharing', $meat);
			// Display the alert box
			echo '<script>alert("Payment Failed.")</script>';
			$this->output->set_header('refresh:2;'.site_url());
		}

	}//biggie


	public function christmasLarge(){
		//load database
		$this->load->database();
		//load Model
		$this->load->model('meat_model');
		//load helper
		$this->load->helper('url');
		$this->load->helper('date');
		//load library
		$this->load->library('pagination');
		$this->load->library('table');
		$this->load->library('calendar');
		$this->load->library('encrypt');
		$fname = $this->input->get('name');
		$phone_number = $this->input->get('phone_number');
		$delivery_address = $this->input->get('delivery_address');
		$delivery_date = $this->input->get('delivery_date');
		$location = $this->input->get('location');
		$email = $this->input->get('email');
		$bamount = $this->input->get('bamount');

		// get specta reference
		$reference = $this->input->get('ref', TRUE);
		//echo $reference;

		$data = array(
			"verificationToken" => $reference
		);
		$post_data = json_encode($data);
		// get payment verification
		$crl = curl_init('https://paywithspectaapi.sterling.ng/api/Purchase/VerifyPurchase');
		curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($crl, CURLINFO_HEADER_OUT, true);
		curl_setopt($crl, CURLOPT_POST, true);
		curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);

		// Set HTTP Header for POST request
		// TEST_API_KEY
		// 969688b48dad4b8981be20fc035da527
		curl_setopt($crl, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'x-ApiKey: 969688b48dad4b8981be20fc035da527',
				'Content-Length: ' . strlen($post_data))
		);

		$response = curl_exec($crl);
		curl_close($crl);

		$better_response = json_decode($response);
		//print_r($better_response);
		//echo $response;
		$ref = $better_response->result->data->reference;
		$query = $this->db->query("SELECT cname, amount, email, phone_number, plan, delivery_address, process, delivery_date FROM meat_sharing WHERE reference = '$ref'");
		$results = $query->result();

		foreach ($results as $row)
		{
//			echo $row->cname;
//			echo $row->email;
//			echo $row->phone_number;
//			echo $row->plan;
//			echo $row->amount;
//			echo $row->process;
//			echo $row->delivery_address;
//			echo $row->delivery_date;
		}

		if (isset($better_response->result->data->isSuccessful) && $better_response->result->data->isSuccessful == true) {
			//echo $better_response->result->data->reference;
			//update specta reference into db
			$meat['specta_reference'] = $better_response->result->data->paymentReference;
			$meat['specta_status']= 'Success';
			$meat['specta_payment'] = $better_response->result->data->amountSettled;
			$this->db->where('reference', $better_response->result->data->reference);
			$this->db->update('meat_sharing', $meat);
			$ref = $better_response->result->data->reference;
			$aa = $better_response->result->data->amount;
			//get data from table and send email

			//set up email
			$config = array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.gmail.com',
				'smtp_port' => 465,
				'smtp_user' => 'admin@livestock247.com', // change it to yours
				'smtp_pass' => 'b861N8cPchr3', // change it to yours
				'mailtype' => 'html',
				'charset' => 'iso-8859-1',
				'wordwrap' => TRUE
			);

			//$to = $email;
			$groupmail = 'meat247@livestock247.com';
			//$groupmail = 'esther.akowe@livestock247.com';

			$message = " 
						<html>
						<head>
							<title>Meat247 Sales</title>
						</head>
						<body>
							<h3>Meat247- Christmas Promo Payment Via Specta</h3>
							<p>
							$row->cname has paid N$row->amount Via Specta<br><br>
    						Email: 				$row->email <br>
    						Phone:				$row->phone_number <br>
    						Payment reference: 	$ref<br>
    						Package: 			$row->plan <br>
    						Process:			$row->process <br>
    						Delivery:			$row->delivery_address <br>
    						Delivery Date:		$row->delivery_date <br>
    					
							<a href='" . site_url() . "admin' class='btn btn-sm'></a>
							<a href='https://livestock247.com/sales_agent/login' class='text-primary'> Click here to see payment details</a>
    						</p>
    					
						</body>
						</html>
						";

			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from($config['smtp_user']);
			$this->email->to($groupmail);
			//$this->email->cc($groupmail);
			$this->email->subject('Meat247 - Christmas Promo Payment Via Specta');
			$this->email->message($message);


			if ($this->email->send()) {

			}
			//$this->load->view('admin/payment_verify');
			echo '<script>alert("Your Payment Was Successful. A member of our team will contact you.")</script>';
			$this->output->set_header('refresh:2;'.site_url());

		}
		if (isset($better_response->result->data->isSuccessful) && $better_response->result->data->isSuccessful == false) {
			//echo  $better_response->result->data->amountSettled;
			$meat['specta_reference'] = $better_response->result->data->paymentReference;
			$meat['specta_status']= 'Failed';
			$this->db->where('reference', $better_response->result->data->reference);
			$this->db->update('meat_sharing', $meat);
			// Display the alert box
			echo '<script>alert("Payment Failed.")</script>';
			$this->output->set_header('refresh:2;'.site_url());
		}

	}//

}//class
