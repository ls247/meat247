<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class meat_model extends CI_Model
{
//validate referral code
	public function validate($referral){
		$this->db->where('referral',$referral);
		$result = $this->db->get('users',1);
		return $result;
	}

	public function insert($biggie){
		$this->db->insert('biggie', $biggie);
		return $this->db->insert_id();
	}

	public function update($meat, $ref){
		$this->db->where('reference', $ref);
		$this->db->update('meat_sharing', $meat);
	}

	public function select($ref){
		//$result['cname']=array();
		$this->db->select('cname');
		$this->db->from('meat_sharing');
		$this->db->where('reference', $ref);
		$query=$this->db->get();

		return $query;



	}

}
