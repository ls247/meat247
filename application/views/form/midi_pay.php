
<div class="space"></div>

<h3 class="text-primary text-center">
	<?php
	echo "Hi" ."&nbsp" ."&nbsp" ."&nbsp";
	?></h3>
<p class="text-primary text-center" style="text-align:center">Choose Your Preferred Payment Method</p>
<p class="text-primary text-center" style="text-align:center"><i class="fa fa-arrow-down"></i></p>
<div class="container">
	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-4">
			<p class="text-center font-weight-bold">For Instant Payment</p>
<form method="GET" id="paymentForm" >
	<script src="https://js.paystack.co/v1/inline.js"></script>

	<div class="form-group">
		<input class="lsxpressinput" type="hidden" id="referral"  name="referral"  placeholder="Enter Referral Code"/>
	</div>

	<div class="form-group">
		<input class="lsxpressinput" type="hidden" id="amount"  name="plan" value="" readonly />

		<input class="lsxpressinput" type="hidden" id="midi_amount" name="midi_amount" value="<?php echo $_POST['midi_amount'] ;?>">

	</div>

	<div class="form-group">
		<input class="lsxpressinput" type="hidden" id="name" name="name" placeholder="Your Name.." value="<?php echo $_POST['name'] ;?>" required>
	</div>


	<div class="form-group">
		<input class="lsxpressinput" type="hidden" id="email" name="email" placeholder="Enter A Valid Email Address" value="<?php echo $_POST['email'] ;?>">
		<input class="lsxpressinput" type="hidden" id="reference" name="reference" value="<?php echo rand() ;?>">
	</div>
	<div class="form-group">
		<input class="lsxpressinput" type="hidden" id="phone_number" name="phone_number" placeholder="Phone Number.." value="<?php echo $_POST['phone_number'] ;?>" required >
	</div>

	<div class="form-group">
		<!-- <input class="lsxpressinput" type="tel" id="amount" name="plan" value="5000" readonly> -->
		<input class="lsxpressinput" type="hidden" id="delivery_address"  name="delivery_address"  placeholder="Delivery Address.." value="<?php echo $_POST['delivery_address'] ;?>"required />
	</div>

	<div class="form-group">
		<!-- <input class="lsxpressinput" type="tel" id="amount" name="plan" value="5000" readonly> -->
		<input class="form-control" type="hidden" id="delivery_date"  name="delivery_date"   value="<?php echo $_POST['delivery_date'] ;?>" required />
	</div>
	<div class="form-group">
		<input class="lsxpressinput" type="hidden" id="location"  name="location"  value="<?php echo $_POST['location'] ;?>"required />
	</div>

	<div class="form-group">
		<div class="col text-center">
			<button  class="btn btn-success"  type="button" onclick=" payWithPaystack(); " name="save" value="Pay" id="saver">Pay With Paystack</button>
		</div>
	</div>
</form>

<script>
	var paymentForm = document.getElementById('paymentForm');
	paymentForm.addEventListener('submit', payWithPaystack, false);
	function payWithPaystack(){
		var handler = PaystackPop.setup({
			// key: 'pk_test_bc9a0d7fb2c4c13c4fc1d9f65d44da3cc55d7f45',
			 key: 'pk_live_b17badea73dd7d69efa07dbb9c8edf1d36256821',
			email:  document.getElementById("email").value,
			amount: document.getElementById("midi_amount").value * 100, // the amount value is multiplied by 100 to convert to the lowest currency unit
			currency: 'NGN', // Use GHS for Ghana Cedis or USD for US Dollars


			reference: '<?php echo  $_POST['reference'] ; ?>', // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
			metadata: {
				custom_fields: [
					{
						display_name: "Mobile Number",
						variable_name: "mobile_number",
						value: "+2349025670847"
					}
				]

			},


			callback: function(response){
				//this happens after the payment is completed successfully
				var reference = <?php echo  $_POST['reference'] ; ?>;
				var name = document.getElementById("name").value;
				var email = document.getElementById("email").value;
				var midi_amount = document.getElementById("midi_amount").value;
				var phone_number = document.getElementById("phone_number").value;
				var delivery_address = document.getElementById("delivery_address").value;
				var delivery_date = document.getElementById("delivery_date").value;
				var location = document.getElementById("location").value;


				// alert('Payment complete! Reference : ' + reference);

				var paystack_reference=response.reference;
				var reference = reference;

				// var verify = "<?php echo site_url();?>verify/paystack_reference";
				window.location = "<?php echo site_url();?>midi_success?successfullypaid="+paystack_reference + '&pref='+reference + '&name='+name + '&email='+email + '&midi_amount='+midi_amount + '&phone_number='+phone_number + '&delivery_address='+delivery_address + '&location='+location +'&delivery_date='+delivery_date;
				// document.getElementById("paystack_reference_lsxpress").value = "paystack_reference_lsxpress";

			},
			onClose: function(){
				alert('Transaction was not completed, window closed.');
			}
		});
		handler.openIframe();

	}
</script>
		</div>

<!--Specta-->
<div class="col-lg-4">
	<p class="text-center font-weight-bold">Buy Now Pay Later</p>
	<!-- Specta Payment -->
	<form method="post" action="<?php echo site_url('midi_pay') ; ?>">
		<div class="form-group">
			<input class="lsxpressinput" type="hidden" id="amount"  name="plan" value="" readonly />
			<input class="lsxpressinput" type="hidden" id="midi_amount" name="midi_amount" value="<?php echo $_POST['midi_amount'] ;?>">
		</div>

		<div class="form-group">
			<input class="form-control"  type="hidden" id="name" name="name" placeholder="Your Name.."  value="<?php echo $_POST['name'] ;?>">
		</div>

		<div class="form-group">
			<input class="form-control"  type="hidden" id="email" name="email" placeholder="Enter A Valid Email Address" value="<?php echo $_POST['email'] ;?>">
			<input class="form-control"  type="hidden" id="reference" name="reference" value="<?php echo $_POST['reference']  ;?>">
		</div>
		<div class="form-group">
			<input class="form-control"  type="hidden" id="phone_number" name="phone_number" placeholder="Phone Number.."  value="<?php echo $_POST['phone_number'] ;?>" >
		</div>

		<div class="form-group">
			<input class="form-control" type="hidden" id="delivery_address"  name="delivery_address"  placeholder="Delivery Address.." value="<?php echo $_POST['delivery_address'] ;?>">
		</div>
		<div class="form-group">
			<input class="form-control" type="hidden" id="delivery_date"  name="delivery_date"   value="<?php echo $_POST['delivery_date'] ;?>">
		</div>

		<div class="form-group">
			<input class="form-control" type="hidden" id="location"  name="location"  placeholder="" value="<?php echo $_POST['location'] ;?>">
		</div>

		<div class="form-group">
			<input class="form-control" type="hidden" id="referral"  name="referral"  value="<?php echo $_POST['referral'] ?>">
		</div>

		<div class="form-group">
			<input class="form-control"  type="hidden" id="merchant" name="merchantid" value="174646">
			<input class="form-control"  type="hidden" id="plan" name="plan" value="<?php echo $_POST['plan'] ;?> " >
		</div>
		<div class="form-group">
			<div class="col text-center">
				<input class="btn" name="specta_btn" type="submit" value="Pay With Specta" style="background-color: #a00065; color:#fff;">
			</div>
		</div>
	</form>	<!-- Pay with Specta-->
	<p class="text-center font-weight-bold">Don't Have Specta Account?  <a href="https://paywithspecta.com/account/register" target="_blank">SignUp Here</a></p>
</div>
<div class="col-lg-2"></div>

</div><!-- payment row-->
</div><!-- container-->
