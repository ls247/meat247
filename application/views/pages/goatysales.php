<!-- Page Content -->
<div class="container">
	<div class="row">
		<div class="col-lg-3" style="background:rgba(32,120,191, 0.1);">
			<h3 class="my-4"><br>Special Bundles</h3>
			<div class="list-group" >
				<a href="<?php echo site_url();?>" class="list-group-item">Meat247</a>
				<a href="https://livestock247.com/easter-meat" class="list-group-item">Easter Meat Sales</a>
<!--				<a href="https://livestock247.com/sallah" class="list-group-item">Special Ram Sales</a>-->
				<a href="https://livestock247.com/meat247/lagos" class="list-group-item">Lagos Meat Sharing</a>
				<a href="https://livestock247.com/meat247/abuja" class="list-group-item">Abuja Meat Sharing</a>

			</div>

			<!-- testimonials -->
			<p >
				<br><br>
			<h3 class="my-4">What Our Customers Say</h3>
			</p>
			<div id="carouselExampleIndicators1" class="carousel slide " data-ride="carousel">
				<ol class="carousel-indicators" >
					<li style="width: 10px; height: 10px; background-color: #003966;" data-target="#carouselExampleIndicators1" data-slide-to="0" class="active"></li>
					<li style="width: 10px; height: 10px; background-color: #003966;" data-target="#carouselExampleIndicators1" data-slide-to="1"></li>
					<li style="width: 10px; height: 10px; background-color: #003966;" data-target="#carouselExampleIndicators1" data-slide-to="2"></li>
					<li style="width: 10px; height: 10px; background-color: #003966;" data-target="#carouselExampleIndicators1" data-slide-to="3"></li>
					<li style="width: 10px; height: 10px; background-color: #003966;" data-target="#carouselExampleIndicators1" data-slide-to="4"></li>
					<li style="width: 10px; height: 10px; background-color: #003966;" data-target="#carouselExampleIndicators1" data-slide-to="5"></li>
					<li style="width: 10px; height: 10px; background-color: #003966;" data-target="#carouselExampleIndicators1" data-slide-to="6"></li>
					<li style="width: 10px; height: 10px; background-color: #003966;" data-target="#carouselExampleIndicators1" data-slide-to="7"></li>

				</ol>

				<div class="carousel-inner" role="listbox">
					<div class="carousel-item active">
						<p class="text-black-50">
							<i class="fa fa-quote-left" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br>
							Livestock247 really surprised me. I've heard bad stories about some meat selling platforms so my expectations were kinda low.
							The Ram meat  delivered to me was very reasonable with the price and it was not just bones.
							It was given a decent wash. Also, delivery was as at when delivered. I'll definitely recommend them.
							<br>
							<strong>Oluwafemi</strong>
							<i class="fa fa-quote-right" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br><br>
						</p>
					</div>

					<div class="carousel-item ">
						<p class="text-black-50">
							<i class="fa fa-quote-left" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br>
							Your services has been satisfactory.
							The beef is good and goat meat is fine. Just keep it on, please. I will keep on advertising you.<br>
							<strong>Akin Walter</strong>
							<i class="fa fa-quote-right" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br><br>
						</p>
					</div>

					<div class="carousel-item ">
						<p class="text-black-50">
							<i class="fa fa-quote-left" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br>
							Wallahi it was perfect, delivered fresh and clean
							<br>
							<strong>Mimi.</strong>
							<i class="fa fa-quote-right" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br><br>
						</p>
					</div>


					<div class="carousel-item">
						<p class="text-black-50">
							<i class="fa fa-quote-left" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br>
							It was my first time of patronising and i got value for my money. I will surely patronise again

							<br>
							<strong>Khrmi Roy.</strong>
							<i class="fa fa-quote-right" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br><br>
						</p>
					</div>

					<div class="carousel-item">
						<p class="text-black-50">
							<i class="fa fa-quote-left" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br>
							I'm a satisfied customer, i look forward to future engagements, thanks to the team.
							<br>
							<strong>MR. Chukwuemeka</strong>
							<i class="fa fa-quote-right" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br><br>
						</p>
					</div>

					<div class="carousel-item">
						<p class="text-black-50">
							<i class="fa fa-quote-left" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br>
							Professionalism, Quality, Responsiveness.
							Thank you guys.
							<br>
							<strong>Peter Dayamu Agga.</strong>
							<i class="fa fa-quote-right" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br><br>
						</p>
					</div>

					<div class="carousel-item">
						<p class="text-black-50">
							<i class="fa fa-quote-left" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br>
							I'm most grateful, my order came so quickly,
							thank you so much.
							<br>
							<strong>Fatima.</strong>
							<i class="fa fa-quote-right" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br><br>
						</p>
					</div>


					<div class="carousel-item">
						<p class="text-black-50">
							<i class="fa fa-quote-left" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br>
							You guys did good, i like the meats i got.
							<br>
							<strong>Balogun Ifedayo.</strong>
							<i class="fa fa-quote-right" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br><br>
						</p>
					</div>


				</div>

				<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>


		</div><!-- end side bar col-3-->
		<!-- /.col-lg-3 -->
		<div class="col-lg-9">

			<div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
				</ol>
				<div class="carousel-inner" role="listbox">
					<div class="carousel-item active">
						<img class="d-block img-fluid" src="<?php echo base_url().'assets/images/goatybanner.png' ?>" style="height: 350px; width: 900px;" alt="Christmas Sales">
<!--						<div class="carousel-caption">-->
<!--							<a class="btn btn-primary" href="#proceed"><i class="fa fa-arrow-down"></i> Proceed Below</a>-->
<!---->
<!--						</div>-->
					</div>
				</div>
				<!--next and previous button here-->
			</div>

			<div class="row">
				<div class="col-lg-4" style="text-align: center;">
					<h5 style="">Call us on </h5>
					<p>
						<i class="fa fa-phone" style="color:#25D366; font-size:26px; font-weight: 400;"  > </i>
						<span style="font-size: 20px;"> 0906-290-3550 </span>
					</p>
				</div>

				<div class="col-lg-4" style="text-align: center;">
					<h5>Order now via WhatsApp,</h5>
					<span style="font-size: 20px;">send <strong style=" color:#2078BF">Meat247</strong> to <strong style=" color:#2078BF">0810 760 0076</strong></span>
				</div>

				<div class="col-lg-4" style="text-align: center;">
					<h5 style="">Chat with us on</h5>
					<p>
						<i  style="color:#25D366; font-size:26px; font-weight: 300;" class="fa fa-whatsapp" ></i>
						<span style="font-size: 20px;"><a href ="https://api.whatsapp.com/send?phone=+2349062903550&text=i%20want%20to%20buy%20meat" style="color:#000;"> WhatsApp</a></span>
					</p>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12">
					<br>
					<h5 class="text-center">Or Buy Online </h5>

					<h5 class="text-center"><i class="fa fa-arrow-down"></i> </h5>
				</div>
			</div>



<!--		<div class="row">-->
<!--				<div class="col-lg-6">-->
<!--					<div style="border: 1px solid #ccc; border-radius: 5px; padding: 12px">-->
<!--						<a href="--><?php //echo site_url(); ?><!--christmasMedium" >-->
<!--							<img class="d-block w-100" src="--><?php //echo base_url().'assets/images/goatymedium.png' ?><!--"  alt=" ">-->
<!--						</a>-->
<!--					</div>-->
<!--					<br>-->
<!--					<p class="text-center">Medium Goat</p>-->
<!--					<button class="btn btn-primary  btn-block"><a href="--><?php //echo site_url(); ?><!--christmasMedium" class="text-white">Buy Now (₦23,000)</a></button>-->
<!--				</div>-->
<!---->
<!--				<div class="col-lg-6">-->
<!--					<div style="border: 1px solid #ccc; border-radius: 5px; padding: 12px">-->
<!--						<a href="" >-->
<!--							<img class="d-block w-100" src="--><?php //echo base_url().'assets/images/goatylarge.png' ?><!--"  alt=" ">-->
<!--						</a>-->
<!--					</div>-->
<!--					<br>-->
<!--					<p class="text-center">Large Goat</p>-->
<!--					<button class="btn btn-primary btn-block"><a href="--><?php //echo site_url(); ?><!--christmasLarge" class="text-white">Buy Now (₦27,800)</a></button>-->
<!--				</div>-->
<!---->
<!--			</div>-->
			<!-- /row -->
<p><br><br></p>
		</div>	<!-- /.col-lg-9 -->
	</div>	<!-- /.row -->
</div>	<!-- /container -->
