<!-- Page Content -->
<div class="container">
	<div class="row">
		<div class="col-lg-3" style="background:rgba(32,120,191, 0.1);">
			<h3 class="my-4"><br>Special Bundles</h3>
			<div class="list-group" >
				<a href="<?php echo site_url();?>goatysales" class="list-group-item">Goaty Festive Sales</a>
				<a href="https://livestock247.com/easter-meat" class="list-group-item">Easter Meat Sales</a>
<!--				<a href="https://livestock247.com/sallah" class="list-group-item">Special Ram Sales</a>-->
				<a href="<?php echo site_url();?>lagos" class="list-group-item">Lagos Meat Sharing</a>
				<a href="<?php echo site_url();?>abuja" class="list-group-item">Abuja Meat Sharing</a>
			</div>

			<!-- testimonials -->
			<p >
			<br><br>
			<h3 class="my-4">What Our Customers Say</h3>
			</p>
			<div id="carouselExampleIndicators1" class="carousel slide " data-ride="carousel">
				<ol class="carousel-indicators" >
					<li style="width: 10px; height: 10px; background-color: #003966;" data-target="#carouselExampleIndicators1" data-slide-to="0" class="active"></li>
					<li style="width: 10px; height: 10px; background-color: #003966;" data-target="#carouselExampleIndicators1" data-slide-to="1"></li>
					<li style="width: 10px; height: 10px; background-color: #003966;" data-target="#carouselExampleIndicators1" data-slide-to="2"></li>
					<li style="width: 10px; height: 10px; background-color: #003966;" data-target="#carouselExampleIndicators1" data-slide-to="3"></li>
					<li style="width: 10px; height: 10px; background-color: #003966;" data-target="#carouselExampleIndicators1" data-slide-to="4"></li>
					<li style="width: 10px; height: 10px; background-color: #003966;" data-target="#carouselExampleIndicators1" data-slide-to="5"></li>
					<li style="width: 10px; height: 10px; background-color: #003966;" data-target="#carouselExampleIndicators1" data-slide-to="6"></li>
					<li style="width: 10px; height: 10px; background-color: #003966;" data-target="#carouselExampleIndicators1" data-slide-to="7"></li>

				</ol>

				<div class="carousel-inner" role="listbox">
					<div class="carousel-item active">
						<p class="text-black-50">
							<i class="fa fa-quote-left" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br>
							Livestock247 really surprised me. I've heard bad stories about some meat selling platforms so my expectations were kinda low.
							The Ram meat  delivered to me was very reasonable with the price and it was not just bones.
							It was given a decent wash. Also, delivery was as at when delivered. I'll definitely recommend them.
							<br>
							<strong>Oluwafemi</strong>
							<i class="fa fa-quote-right" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br><br>
						</p>
					</div>

					<div class="carousel-item ">
						<p class="text-black-50">
							<i class="fa fa-quote-left" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br>
							Your services has been satisfactory.
							The beef is good and goat meat is fine. Just keep it on, please. I will keep on advertising you.<br>
							<strong>Akin Walter</strong>
							<i class="fa fa-quote-right" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br><br>
						</p>
					</div>

					<div class="carousel-item ">
						<p class="text-black-50">
							<i class="fa fa-quote-left" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br>
							Wallahi it was perfect, delivered fresh and clean
							<br>
							<strong>Mimi.</strong>
							<i class="fa fa-quote-right" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br><br>
						</p>
					</div>


					<div class="carousel-item">
						<p class="text-black-50">
							<i class="fa fa-quote-left" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br>
							It was my first time of patronising and i got value for my money. I will surely patronise again

							<br>
							<strong>Khrmi Roy.</strong>
							<i class="fa fa-quote-right" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br><br>
						</p>
					</div>

					<div class="carousel-item">
						<p class="text-black-50">
							<i class="fa fa-quote-left" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br>
							I'm a satisfied customer, i look forward to future engagements, thanks to the team.
							<br>
							<strong>MR. Chukwuemeka</strong>
						<i class="fa fa-quote-right" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
						<br><br>
						</p>
					</div>

					<div class="carousel-item">
						<p class="text-black-50">
							<i class="fa fa-quote-left" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br>
							Professionalism, Quality, Responsiveness.
							Thank you guys.
							<br>
							<strong>Peter Dayamu Agga.</strong>
							<i class="fa fa-quote-right" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br><br>
						</p>
					</div>

					<div class="carousel-item">
						<p class="text-black-50">
							<i class="fa fa-quote-left" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br>
							I'm most grateful, my order came so quickly,
							thank you so much.
							<br>
							<strong>Fatima.</strong>
							<i class="fa fa-quote-right" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br><br>
						</p>
					</div>


					<div class="carousel-item">
						<p class="text-black-50">
							<i class="fa fa-quote-left" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br>
							You guys did good, i like the meats i got.
							<br>
							<strong>Balogun Ifedayo.</strong>
							<i class="fa fa-quote-right" aria-hidden="true" style="color: #003966; font-size: 30px;"></i>
							<br><br>
						</p>
					</div>


				</div>

				<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>


		</div><!-- end side bar col-3-->
		<!-- /.col-lg-3 -->
		<div class="col-lg-9">
			<div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
<!--					<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>-->
<!--					<li data-target="#carouselExampleIndicators" data-slide-to="3"></li>-->
				</ol>
				<div class="carousel-inner" role="listbox">
					<div class="carousel-item active">
						<img class="d-block img-fluid" src="<?php echo base_url().'assets/images/goatybanner.png' ?>" style="height: 350px; width: 900px;" alt="Christmas Sales">
						<div class="carousel-caption">
							<!--							<h3>Buy Livestock Now</h3>-->
							<!--							<p class="text-white">-->
							<!--							<h4>Pay With Specta</h4>-->
							<!--								<br>&nbsp; </p>-->
							<a class="btn btn-primary" href="<?php echo site_url();?>goatysales">Click Here</a>
						</div>
					</div>
					<div class="carousel-item">
						<img class="d-block img-fluid" src="<?php echo base_url().'assets/images/buy-meat.png' ?>" style="height: 350px; width: 900px;" alt="First slide">
						<div class="carousel-caption">
<!--							<h3>Buy Meat Now</h3>-->
<!--							<p class="text-white">-->
<!--								<h4>Pay With Specta</h4>-->
<!--								<br> &nbsp;</p>-->
							<a class="btn btn-primary" href="#proceed"><i class="fa fa-arrow-down"></i> Proceed Below</a>

						</div>
					</div>

					<div class="carousel-item">
						<img class="d-block img-fluid" src="<?php echo base_url().'assets/images/buy-livestock.png' ?>" style="height: 350px; width: 900px;" alt="First slide">
						<div class="carousel-caption">
<!--							<h3>Buy Livestock Now</h3>-->
<!--							<p class="text-white">-->
<!--							<h4>Pay With Specta</h4>-->
<!--								<br>&nbsp; </p>-->
							<a class="btn btn-primary" href="https://livestock247.com/buy">Click Here</a>
						</div>
					</div>

<!--					</div>-->
<!--					<div class="carousel-item">-->
<!--						<img class="d-block img-fluid" src="--><?php //echo base_url().'assets/images/banner3.png' ?><!--" style="height: 350px; width: 900px;" alt="First slide">-->
<!--						<div class="carousel-caption">-->
<!--							<h3 class="">No Yamá Yamá</h3>-->
<!--							<p class="text-white">Certified veterinary check on all Livestock-->
<!--								<br>Convenience, quality meat.-->
<!--							</p>-->
<!--							<a class="btn btn-primary" href="lagos">Buy Now</a>-->
<!--						</div>-->
<!--					</div>-->
				</div>
				<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>

			<div class="row" id="proceed">
				<div class="col-lg-4 mb-4">
					<div class="card h-100">
						<a href="lagos"><img class="card-img-top" src="<?php echo base_url().'assets/images/Lagos.png' ?>" width="700" height="150"></a>
						<div class="card-body">
							<h4 class="card-title">
								<a href="lagos">Lagos</a>
							</h4>
							<table class="table-striped small">
								<tr>
									<td>Certified Veterinary Check</td>
									<td class="text-success">✔︎</td>
								</tr>
								<tr>
									<td>Hygienic Processing</td>
									<td class="text-success">✔︎</td>
								</tr>
								<tr>
									<td>Convenience</td>
									<td class="text-success">✔︎</td>
								</tr>
								<tr>
									<td>Wholesome</td>
									<td class="text-success">✔︎</td>
								</tr>
								<tr>
									<td>Consistency</td>
									<td class="text-success">✔︎</td>
								</tr>
								<tr>
									<td>Free Delivery</td>
									<td class="text-success">✔︎</td>
								</tr>
								<tr>
									<td>Same Day Delivery (<a class="" style="cursor: pointer;" data-toggle="tooltip" data-placement="bottom" title="Any order after 12:noon, delivery is next day">
											T & C apply
										</a>)
									</td>
									<td class="text-danger">✘</td>
								</tr>

							</table>

							<p class="card-text">
								</p>
						</div>
						<div class="card-footer btn btn-primary">
							<a href="lagos" class="text-white">Proceed</a>
						</div>
					</div>
				</div>

				<div class="col-lg-4 mb-4">
					<div class="card h-100">
						<a href="abuja"><img class="card-img-top" src="<?php echo base_url().'assets/images/Abuja.png' ?>" width="700" height="150"></a>
						<div class="card-body">
							<h4 class="card-title">
								<a href="abuja">Abuja</a>
							</h4>
							<table class="table-striped small">
								<tr>
									<td>Certified Veterinary Check</td>
									<td class="text-success">✔︎</td>
								</tr>
								<tr>
									<td>Hygienic Processing</td>
									<td class="text-success">✔︎</td>
								</tr>
								<tr>
									<td>Convenience</td>
									<td class="text-success">✔︎</td>
								</tr>
								<tr>
									<td>Wholesome</td>
									<td class="text-success">✔︎</td>
								</tr>
								<tr>
									<td>Consistency</td>
									<td class="text-success">✔︎</td>
								</tr>
								<tr>
									<td>Free Delivery</td>
									<td class="text-success">✔︎</td>
								</tr>
								<tr>
									<td>Same Day Delivery (<a class="" style="cursor: pointer;" data-toggle="tooltip" data-placement="bottom" title="Any order after 12:noon, delivery is next day">
											T&C apply
										</a>)</td>
									<td class="text-danger">✘</td>
								</tr>

							</table>
							<p class="card-text">
							</p>
						</div>
						<div class="card-footer btn btn-primary">
							<a href="abuja" class="text-white">Proceed</a>
						</div>
					</div>
				</div>

				<div class="col-lg-4 mb-4">
					<div class="card h-100">
						<a href="#"><img class="card-img-top" src="<?php echo base_url().'assets/images/Port-Harcourt.png' ?>" width="700" height="150"></a>
						<div class="card-body">
							<h4 class="card-title">
								<a href="#">Port-Harcourt</a>
							</h4>
							<p class="card-text">COMING SOON</p>
						</div>
						<div class="card-footer btn btn-primary">
							<small class="">Coming Soon</small>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.col-lg-9 -->
	</div>
	<!-- /.row -->
</div>
<!-- /.container -->
