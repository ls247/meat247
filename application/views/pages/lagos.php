<div class="space"></div>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h5 style=" color:#2078BF" class="text-center"> To Buy Meat (Lagos) </h5>
		</div>
	</div>
</div>

<div class="container">
	<br>
	<div class="row">
		<div class="col-lg-4" style="text-align: center;">
			<h5 style="">Call us on </h5>
			<p class="text-center">
				<i class="fa fa-phone" style="color:#25D366; font-size:26px; font-weight: 400;"  > </i>
				<span style="font-size: 20px;"> 0906-290-3550 </span>
			</p>
		</div>

		<div class="col-lg-4" style="text-align: center;">
			<h5>Order now via WhatsApp,</h5>
			<span style="font-size: 20px;">send <strong style=" color:#2078BF">Meat247</strong> to <strong style=" color:#2078BF">0810 760 0076</strong></span>
		</div>

		<div class="col-lg-4" style="text-align: center;">
			<h5 style="">Chat with us on</h5>
			<p class="text-center">
				<i  style="color:#25D366; font-size:26px; font-weight: 300;" class="fa fa-whatsapp" ></i>
				<span style="font-size: 20px;"><a href ="https://api.whatsapp.com/send?phone=+2349062903550&text=i%20want%20to%20buy%20meat" style="color:#000;"> WhatsApp</a></span>
			</p>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<br>
			<h5 class="text-center">Or Buy Online </h5>

			<h5 class="text-center"><i class="fa fa-arrow-down"></i> </h5>
		</div>
	</div>
</div>


<div class="container">
	<div class="row">
		<div class="col-lg-12 text-center">

			<a class="btn btn-outline-primary" onclick="cowFunction()">Cow Meat</a>
			&nbsp;
			<a class="btn btn-primary" onclick="goatFunction()">Goat Meat</a>
			&nbsp;
			<a class="btn btn-outline-primary" onclick="ramFunction()">Ram Meat</a>
			&nbsp;
<!--		<a class="btn btn-primary" onclick="salesFunction()">Flash Sales!</a>-->
			<br>
		</div>
	</div>
</div>

<div class="container">
	<br>
	<div class="row" id="cowMeat">
		<div class="col-lg-12">
<table  class="table table-striped" >
	<thead>
	<tr class="table-primary">
<th>Packages</th>
<th>Biggie</th>
<th>Midi</th>
<th> Mini</th>
</tr>
	</thead>
	<tbody>
<tr>
<th>Amount</th>
<td>&#8358;54,500</td>
<td>&#8358;28,600</td>
<td>&#8358;15,700</td>
</tr>
<tr>
<th>Quantity(kg)</th>
<td>Not less than 20kg</td>
<td>Not less than 10kg</td>
<td>Not less than 5kg</td>
</tr>
<tr>
<th></th>
<td>Per person</td>
<td>Per person</td>
<td>Per person</td>
</tr>
<tr>
<th>A Big Cow Shared Among</th>
<td>5 person(s)</td>
<td>10 person(s)</td>
<td>20 person(s)</td>
</tr>

<tr>
	<th>All Parts Inclusive</th>
	<td>Yes</td>
	<td>Yes</td>
	<td>Yes</td>

</tr>

<tr>
<th></th>
<td><a href="<?php echo site_url(); ?>biggie" class="btn btn-primary ">Buy (₦54,500)</a></td>
<td><a href="<?php echo site_url(); ?>midi" class="btn btn-primary ">Buy (₦28,600)</a></td>
<td><a href="<?php echo site_url(); ?>mini" class="btn btn-primary ">Buy (₦15,700)</a></td>
</tr>
	</tbody>
</table>
		</div>
	</div><!-- end row-->


	<div class="row" id="goatMeat">
		<div class="col-lg-12">
			<!-- goat meat--->
			<table class="table table-striped" >
				<thead>
				<tr class="table-primary">
					<th>Packages</th>
					<th>Large</th>
					<th>Medium</th>
					<th>Small</th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<th>Amount</th>
					<td>&#8358;39,800</td>
					<td>&#8358;29,800</td>
					<td>&#8358;23,000</td>
				</tr>
				<tr>
					<th>Inclusive</th>
					<td>Delivery</td>
					<td>Delivery</td>
					<td>Delivery</td>
				</tr>
				<tr>
					<th></th>
					<td>Processing</td>
					<td>Processing</td>
					<td>Processing</td>
				</tr>
				<tr>
					<th>A Whole Goat</th>
					<td>No Sharing</td>
					<td>No Sharing</td>
					<td>No Sharing</td>
				</tr>
				<tr>
					<th>All Parts Inclusive</th>
					<td>Yes</td>
					<td>Yes</td>
					<td>Yes</td>

				</tr>
				<tr>
					<th></th>
					<td><a href="<?php echo site_url(); ?>goat_large" class="btn btn-primary">Buy (₦39,800)</a></td>
					<td><a href="<?php echo site_url(); ?>goat_medium" class="btn btn-primary">Buy (₦29,800)</a></td>
					<td><a href="<?php echo site_url(); ?>goat_small" class="btn btn-primary">Buy (₦23,000)</a></td>
				</tr>
				</tbody>
			</table>
		</div>
	</div><!-- end row-->


	<div class="row" id="ramMeat">
		<div class="col-lg-12">
			<!-- Ram meat--->
			<table  class="table table-striped" >
				<thead>
				<tr class="table-primary">
					<th>Packages</th>
					<th>Maxi</th>
					<th>Standard</th>
					<th>Compact</th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<th>Amount</th>
					<td>&#8358;150,000</td>
					<td>&#8358;85,000</td>
					<td>&#8358;55,000</td>
				</tr>
				<tr>
					<th>Inclusive</th>
					<td>Delivery</td>
					<td>Delivery</td>
					<td>Delivery</td>
				</tr>
				<tr>
					<th></th>
					<td>Processing</td>
					<td>Processing</td>
					<td>Processing</td>
				</tr>
				<tr>
					<th>A Whole Ram</th>
					<td>No Sharing</td>
					<td>No Sharing</td>
					<td>No Sharing</td>
				</tr>
				<tr>
					<th>All Parts Inclusive</th>
					<td>Yes</td>
					<td>Yes</td>
					<td>Yes</td>
				</tr>
				<tr>
					<th></th>
					<td><a href="<?php echo site_url(); ?>ram_maxi" class="btn btn-primary">Buy (₦150,000)</a></td>
					<td><a href="<?php echo site_url(); ?>ram_standard" class="btn btn-primary">Buy (₦85,000)</a></td>
					<td><a href="<?php echo site_url(); ?>ram_compact" class="btn btn-primary">Buy (₦55,000)</a></td>
				</tr>
				</tbody>
			</table>
		</div>
	</div> <!-- end row -->
</div><!-- end container-->
<p><br></p>
