<div class="space"></div>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h5 style=" color:#2078BF" class="text-center"> To Buy Meat (Abuja) </h5>
			<!--			<br>-->
			<!--			<h5 class="text-center">-->
			<!--				All payments after 5:00pm on-->
			<!--				--><?php
			//				$monday = strtotime('wednesday this week');;
			//				//$monday = date('W', $monday)==date('W') ? $monday-7*86400 : $monday;
			//				$sunday = strtotime(date("Y-m-d",$monday)." +9 days");
			//				$this_week_sd = date("D d M, Y",$monday);
			//				$this_week_ed = date("D d M, Y",$sunday);
			//				echo "$this_week_sd ";
			//				echo "  <br> Collection will be next slaughter day ";
			//				echo "$this_week_ed";
			//				?>
			<!--			</h5>-->

		</div>
	</div>
</div>


<div class="container">
	<br>
	<div class="row">
		<div class="col-lg-4" style="text-align: center;">
			<h5 style="">Call us on </h5>
			<p>
				<i class="fa fa-phone" style="color:#25D366; font-size:26px; font-weight: 400;"  > </i>
				<span style="font-size: 20px;"> 0906-290-3550 </span>
			</p>
		</div>
		<div class="col-lg-4" style="text-align: center;">
			<h5>Order now via WhatsApp,</h5>
			<span style="font-size: 20px;">send <strong style=" color:#2078BF">Meat247</strong> to <strong style=" color:#2078BF">0810 760 0076</strong></span>
		</div>

		<div class="col-lg-4" style="text-align: center;">
			<h5 style="">Chat with us on</h5>
			<p>
				<i  style="color:#25D366; font-size:26px; font-weight: 300;" class="fa fa-whatsapp" ></i>
				<span style="font-size: 20px;"><a href ="https://api.whatsapp.com/send?phone=+2349079961680&text=i%20want%20to%20buy%20meat" style="color:#000;"> WhatsApp</a></span>
			</p>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<br>
			<h5 class="text-center">Or Buy Online </h5>

			<h5 class="text-center"><i class="fa fa-arrow-down"></i> </h5>
		</div>
	</div>
</div>

<!---->
<!--<div class="container">-->
<!--	<div class="row">-->
<!--		<div class="col-lg-12 text-center">-->
<!---->
<!--			<a class="btn btn-outline-primary" onclick="cowFunction()">Cow Meat</a>-->
<!--			&nbsp;-->
<!--			<a class="btn btn-primary" onclick="goatFunction()">Goat Meat</a>-->
<!--			&nbsp;-->
<!--			<a class="btn btn-outline-primary" onclick="ramFunction()">Ram Meat</a>-->
<!--			&nbsp;-->
<!--			<a class="btn btn-primary" onclick="salesFunction()">Flash Sales!</a>-->
<!--			<br>-->
<!--		</div>-->
<!--	</div>-->
<!--</div>-->

<div class="container">
	<br>
	<div class="row">
		<div class="col-lg-12">
			<table  class="table table-striped" >
				<thead>
				<tr class="table-primary">
					<th>Packages</th>
					<th>Biggie</th>
					<th>Midi</th>
					<th> Mini</th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<th>Amount</th>
					<td>&#8358;57,440</td>
					<td>&#8358;29,730</td>
					<td>&#8358;15,900</td>
				</tr>
				<tr>
					<th>Quantity(kg)</th>
					<td>Not less than 25kg</td>
					<td>Not less than 15kg</td>
					<td>Not less than 6kg</td>
				</tr>
				<tr>
					<th></th>
					<td>Per person</td>
					<td>Per person</td>
					<td>Per person</td>
				</tr>
				<tr>
					<th>A Big Cow Shared Among</th>
					<td>5 person(s)</td>
					<td>10 person(s)</td>
					<td>20 person(s)</td>
				</tr>
				<tr>
					<th>All Parts Inclusive</th>
					<td>Yes</td>
					<td>Yes</td>
					<td>Yes</td>

				</tr>
				<tr>
					<th></th>
					<td><a href="<?php echo site_url(); ?>abuja_biggie"><button type="button" class="btn btn-primary ">Buy (₦57,440)</button></a></td>
					<td><a href="<?php echo site_url(); ?>abuja_midi"><button type="button" class="btn btn-primary">Buy (₦29,730)</button></a></td>
					<td><a href="<?php echo site_url(); ?>abuja_mini"><button type="button" class="btn btn-primary">Buy (₦15,900)</button></a></td>
				</tr>
				</tbody>
			</table>
		</div>
	</div><!-- end row-->


	<div class="row" id="goatMeat">
		<div class="col-lg-12">
			<!-- goat meat--->
			<table border="0" class="table table-striped" >
				<thead>
				<tr class="table-primary">
					<th>Packages</th>
					<th>Large</th>
					<th>Medium</th>
					<th>Small</th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<th>Amount</th>
					<td>&#8358;29,800</td>
					<td>&#8358;25,000</td>
					<td>&#8358;18,500</td>
				</tr>
				<tr>
					<th>Inclusive</th>
					<td>Delivery</td>
					<td>Delivery</td>
					<td>Delivery</td>
				</tr>
				<tr>
					<th></th>
					<td>Processing</td>
					<td>Processing</td>
					<td>Processing</td>
				</tr>
				<tr>
					<th>A Whole Goat</th>
					<td>No Sharing</td>
					<td>No Sharing</td>
					<td>No Sharing</td>
				</tr>
				<tr>
					<th>All Parts Inclusive</th>
					<td>Yes</td>
					<td>Yes</td>
					<td>Yes</td>

				</tr>
				<tr>
					<th></th>
					<td><a href="<?php echo site_url(); ?>goat_large" class="btn btn-primary">Buy (₦29,800)</a></td>
					<td><a href="<?php echo site_url(); ?>goat_medium" class="btn btn-primary">Buy (₦25,000)</a></td>
					<td><a href="<?php echo site_url(); ?>goat_small" class="btn btn-primary">Buy (₦18,500)</a></td>
				</tr>
				</tbody>
			</table>
		</div>
	</div><!-- end row-->


	<div class="row" id="ramMeat">
		<div class="col-lg-12">
			<!-- Ram meat--->
			<table  class="table table-striped" >
				<thead>
				<tr class="table-primary">
					<th>Packages</th>
					<th>Maxi</th>
					<th>Standard</th>
					<th>Compact</th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<th>Amount</th>
					<td>&#8358;75,000</td>
					<td>&#8358;55,000</td>
					<td>&#8358;45,500</td>
				</tr>
				<tr>
					<th>Inclusive</th>
					<td>Delivery</td>
					<td>Delivery</td>
					<td>Delivery</td>
				</tr>
				<tr>
					<th></th>
					<td>Processing</td>
					<td>Processing</td>
					<td>Processing</td>
				</tr>
				<tr>
					<th>A Whole Ram</th>
					<td>No Sharing</td>
					<td>No Sharing</td>
					<td>No Sharing</td>
				</tr>
				<tr>
					<th>All Parts Inclusive</th>
					<td>Yes</td>
					<td>Yes</td>
					<td>Yes</td>

				</tr>
				<tr>
					<th></th>
					<td><a href="<?php echo site_url(); ?>ram_maxi" class="btn btn-primary">Buy (₦75,000)</a></td>
					<td><a href="<?php echo site_url(); ?>ram_standard" class="btn btn-primary">Buy (₦55,000)</a></td>
					<td><a href="<?php echo site_url(); ?>ram_compact" class="btn btn-primary">Buy (₦45,000)</a></td>
				</tr>
				</tbody>
			</table>
		</div>
	</div> <!-- end row -->

	<div class="row" id="salesMeat">
		<div class="col-lg-12">
			<!-- Flash Sales meat--->
			<table class="table table-striped" >
				<thead>
				<tr class="table-primary">
					<th>Packages</th>
					<th>Medium Goat</th>

				</tr>
				</thead>
				<tbody>
				<tr>
					<th>Amount</th>
					<td>&#8358;22,000</td>

				</tr>
				<tr>
					<th>Inclusive</th>
					<td>Delivery</td>

				</tr>
				<tr>
					<th></th>
					<td>Processing</td>

				</tr>
				<tr>
					<th>A Whole Goat</th>
					<td>No Sharing</td>

				</tr>
				<tr>
					<th></th>
					<td><a href="<?php echo site_url(); ?>flash_sales" class="btn btn-primary">Buy (₦22,000)</a></td>

				</tr>
				</tbody>
			</table>
		</div>
	</div><!-- end row-->

</div><!-- end container-->



<p><br></p>
