
<div class="space"></div>
<h3 class="text-primary text-center">Mini Package (Abuja)</h3>
<div class="container">
<div class="row">
<div class="col-lg-2"></div>
	<div class="col-lg-8">
		<?php
		if(validation_errors()){
			?>
			<div class="alert alert-info text-center">
				<?php echo validation_errors(); ?>
			</div>
			<?php
		}
		if($this->session->flashdata('code')){
			?>
			<div class="alert alert-info text-center">
				<?php echo $this->session->flashdata('code'); ?>

			</div>
			<?php
		}
		?>
		<script>
			function getPrice() {
				var discount = Number(document.getElementById("amount").value) - 159;
				document.getElementById("mini_amount").value = discount;

				//show discounted price;
				var hasValue = document.getElementById('referral').value;
				if (!!hasValue) {
					document.getElementById('customer_discount').style.display = 'inline';
					document.getElementById('amount_1').style.display = 'none';
				} else {
					document.getElementById('customer_discount').style.display = 'none';
				};

			}
		</script>
		<p><br>
			All field mark <i class="text-danger">*</i> are compulsory
			<br> </p>
		<form method="POST" id="paymentForm" action="" >
			<script src="https://js.paystack.co/v1/inline.js"></script>

			<div class="form-group">
				<label  for="amount">Discount Code</label> (optional)
				<input class="form-control" type="text" id="referral"  name="referral" oninput="getPrice()"  placeholder="Enter Referral Code"/>
			</div>

			<div class="form-group">
				<label   for="amount">Amount</label>
				<input class="form-control"  type="text" id="amount_1"  name="plan" value="15900" readonly />
			</div>

			<div class="form-group">
				<input class="form-control"  type="hidden" id="amount"  name="plan" value="15900" readonly />
			</div>

			<div class="form-control" id="customer_discount" style="display:none">
				<del>&#8358;15,900</del>&nbsp; <span class="text-primary">&#8358;15,741</span>
			</div>

			<div class="form-group">
				<input class="form-control" type="hidden" id="mini_amount"  name="mini_amount" oninput="getPrice()"  value="15900" readonly />
			</div>

			<div class="form-group">
				<label  for="fname">Full Name</label><i class="text-danger">*</i>
				<input class="form-control"  type="text" id="name" name="name" placeholder="Your Name.." required>
			</div>


			<div class="form-group">
				<label  for="email">Email</label><i class="text-danger">*</i>
				<input class="form-control"  type="email" id="email" name="email" placeholder="Enter A Valid Email Address" required>
				<input class="form-control"  type="hidden" id="reference" name="reference" value="<?php echo rand() ;?>">
			</div>
			<div class="form-group">
				<label  for="Phone Number">Phone Number</label><i class="text-danger">*</i>
				<input class="form-control"  type="number" id="phone_number" name="phone_number" placeholder="Phone Number.." required >
			</div>

			<div class="form-group">
				<label  for="delivery_address">Delivery Address</label><i class="text-danger">*</i>
				<!-- <input class="lsxpressinput" type="tel" id="amount" name="plan" value="5000" readonly> -->
				<input class="form-control"  type="text" id="delivery_address"  name="delivery_address"  placeholder="Delivery Address.." required />
			</div>


			<div class="form-group">
				<label  for="location">State</label><i class="text-danger">*</i>
				<input class="form-control" type="text" id="location"  name="location"  value="Abuja" readonly/>
			</div>
<!--			<span class="font-weight-bold text-primary">-->
<!--	All payments after 5:00pm on-->
<!--			--><?php
//			$monday = strtotime('wednesday this week');;
//			//$monday = date('W', $monday)==date('W') ? $monday-7*86400 : $monday;
//			$sunday = strtotime(date("Y-m-d",$monday)." +9 days");
//			$this_week_sd = date("D d M, Y",$monday);
//			$this_week_ed = date("D d M, Y",$sunday);
//			echo "$this_week_sd ";
//			echo ". Collection will be next slaughter day ";
//			echo "$this_week_ed";
//			?>
<!--	</span>-->
<!---->
<!--			<div class="form-group text-primary">-->
<!--				<input type="checkbox" name="terms" required checked> Clicking proceed means you accept our terms & condition.-->
<!--			</div>-->

			<div class="form-group">
				<button  class="btn btn-primary"  type="submit" onclick=" payWithPaystack(); " name="save" value="Pay" id="saver">Proceed</button>
			</div>

		</form>
	</div>
	<div class="col-lg-2"></div>
</div>

</div>


<script>


	var paymentForm = document.getElementById('paymentForm');
	paymentForm.addEventListener('submit', payWithPaystack, false);
	function payWithPaystack(){
		var handler = PaystackPop.setup({
			 key: 'pk_test_bc9a0d7fb2c4c13c4fc1d9f65d44da3cc55d7f45',
			//key: 'pk_live_b17badea73dd7d69efa07dbb9c8edf1d36256821',
			email:  document.getElementById("email").value,
			amount:  document.getElementById("mini_amount").value * 100, // the amount value is multiplied by 100 to convert to the lowest currency unit
			currency: 'NGN', // Use GHS for Ghana Cedis or USD for US Dollars


        reference: document.getElementById("reference").value, // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
        metadata: {
          custom_fields: [
              {
                  display_name: "Customer Phone",
                  variable_name: "mobile_number",
                  value: document.getElementById("phone_number").value
              }
          ]
          
        },
      
           
        callback: function(response){
          //this happens after the payment is completed successfully
          var reference = document.getElementById("reference").value;
          var name = document.getElementById("name").value;
          var email = document.getElementById("email").value;
          var phone_number = document.getElementById("phone_number").value;
          var delivery_address = document.getElementById("delivery_address").value;
          
          
          
         // alert('Payment complete! Reference : ' + reference);
          
          var paystack_reference = response.reference;
         // var verify = "<?php echo site_url();?>verify/paystack_reference";
         window.location = "<?php echo site_url();?>abuja_mini_success?successfullypaid="+paystack_reference + '&pref='+reference + '&name='+name + '&email='+email + '&phone_number='+phone_number + '&delivery_address='+delivery_address;
         
        },
        onClose: function(){
            alert('Transaction was not completed, window closed.');
        }
      });
      handler.openIframe();
     
    }

</script>
<div class="space"></div>
