<html>
  <head>
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,400i,700,900&display=swap" rel="stylesheet">
  </head>
    <style>
      .bodyfiforsuccess {
        text-align: center;
        
      }
        .hforsuccess {
          color: #88B04B;
          font-family: "Nunito Sans", "Helvetica Neue", sans-serif;
          font-weight: 900;
          font-size: 40px;
          margin-bottom: 10px;
        }
        .pforsuccess {
          color: #404F5E;
          font-family: "Nunito Sans", "Helvetica Neue", sans-serif;
          font-size:20px;
          margin: 0;
        }
      .iforsuccess {
        color: #9ABC66;
        font-size: 100px;
        line-height: 200px;
        margin-left:-15px;
      }
      .cardfiforsuccess {
        background: white;
        padding: 60px;
        border-radius: 4px;
        box-shadow: 0 2px 3px #C8D0D8;
        display: inline-block;
        margin: 0 auto;
      }
    </style>
    <body>
    <div class="container">
      <div class="row">
      <div class=col-lg-2></div>
      <div class=col-lg-8>
     <div  class="bodyfiforsuccess">
      <div class="cardfiforsuccess">
      <div style="border-radius:200px; height:200px; width:200px; background: #F8FAF5; margin:0 auto;">
        <i class="checkmark iforsuccess">✓</i>
      </div>
      <?php
         $successfullypaid = $this->input->get('pref');
         $reference = $this->input->get('reference');
         $name = $this->input->get('name');
         $email = $this->input->get('email');
         $referral=$this->input->get('referral');
	  	 $mamount=$this->input->get('mamount');
         $phone_number = $this->input->get('phone_number');
         $delivery_address = $this->input->get('delivery_address');
         ?>
    
        <h1 class="hforsuccess">Success</h1>
		  <p style="font-size:16px;font-weight:600">Your payment for a Midi meat sharing package was successful</p>
        <?php
         $successfullypaid = $this->input->get('pref');
         $reference = $this->input->get('reference');
         $name = $this->input->get('name');
         $email = $this->input->get('email');
		 $referral=$this->input->get('referral');
		 $mamount=$this->input->get('mamount');
         $phone_number = $this->input->get('phone_number');
         $delivery_address = $this->input->get('delivery_address');
		$location = $this->input->get('location');


		// $reference = $this->input->get('reference');
        //echo "<div style='font-size:16px;font-weight:600;color: #88B04B;'>" . "Your Package will be "."</br>". "DELIVERED TO". "</div>"  ."<div style='font-size:16px;font-weight:600'>" . $delivery_address . "</div>"?>
      </div>
      </div>
      </div>
      <div class=col-lg-2></div>
      </div>
      </div>
    </body>
</html>
