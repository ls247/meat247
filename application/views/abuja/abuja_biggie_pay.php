

<div class="space"></div>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h3 class="text-primary text-center">
				<?php
				echo "Hi" ."&nbsp" ."&nbsp" ."&nbsp";
				?></h3>
			<p class="text-primary" style="text-align:center">Choose your payment method.</p>
			<p class="text-primary" style="text-align:center"><i class="fa fa-arrow-down"></i></p>
			<script>

			</script>

			<form method="GET" id="paymentForm" >
				<script src="https://js.paystack.co/v1/inline.js"></script>


				<div class="form-group">
					<input class="form-control"  type="hidden" id="amount"  name="plan" value="" readonly />

					<input class="form-control"  type="hidden" id="bamount" name="bamount" value="<?php echo $_POST['bamount'] ;?>" readonly>

				</div>

				<div class="form-group">
					<input class="form-control"  type="hidden" id="name" name="name" placeholder="Your Name.."  value="<?php echo $_POST['name'] ;?>" required>
				</div>


				<div class="form-group">
					<input class="form-control"  type="hidden" id="email" name="email" placeholder="Enter A Valid Email Address" value="<?php echo $_POST['email'] ;?>">
					<input class="form-control"  type="hidden" id="reference" name="reference" value="<?php echo rand() ;?>">
				</div>
				<div class="form-group">
					<input class="form-control"  type="hidden" id="phone_number" name="phone_number" placeholder="Phone Number.."  value="<?php echo $_POST['phone_number'] ;?>" required >
				</div>

				<div class="form-group">

					<!-- <input class="lsxpressinput" type="tel" id="amount" name="plan" value="5000" readonly> -->
					<input class="form-control" type="hidden" id="delivery_address"  name="delivery_address"  placeholder="Delivery Address.." value="<?php echo $_POST['delivery_address'] ;?>" required />
				</div>

				<div class="form-group">
					<!-- <input class="lsxpressinput" type="tel" id="amount" name="plan" value="5000" readonly> -->
					<input class="form-control" type="hidden" id="location"  name="location"  placeholder="" value="<?php echo $_POST['location'] ;?>" required />
				</div>

				<div class="form-group">
					<input class="form-control" type="hidden" id="referral"  name="referral"  value="<?php echo $_POST['referral'] ?>" placeholder="Enter Referral Code"/>
				</div>

				<div class="form-group">
					<div class="col text-center">
						<button  class="btn btn-success"  type="button" onclick=" payWithPaystack(); " name="save" value="Pay" id="saver">Pay With Paystack</button>
					</div>
				</div>
			</form>


			<script>
				var paymentForm = document.getElementById('paymentForm');
				paymentForm.addEventListener('submit', payWithPaystack, false);
				function payWithPaystack(){
					var handler = PaystackPop.setup({
						key: 'pk_test_bc9a0d7fb2c4c13c4fc1d9f65d44da3cc55d7f45',
						// key: 'pk_live_b17badea73dd7d69efa07dbb9c8edf1d36256821',
						email:  document.getElementById("email").value,
						amount: document.getElementById("bamount").value * 100, // the amount value is multiplied by 100 to convert to the lowest currency unit
						currency: 'NGN', // Use GHS for Ghana Cedis or USD for US Dollars


						reference: '<?php echo  $_POST['reference'] ; ?>', // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
						metadata: {
							custom_fields: [
								{
									display_name: "Mobile Number",
									variable_name: "mobile_number",
									value: document.getElementById("phone_number").value
								}
							]

						},


						callback: function(response){
							//this happens after the payment is completed successfully
							var reference = <?php echo  $_POST['reference'] ; ?>;
							var name = document.getElementById("name").value;
							var email = document.getElementById("email").value;
							var bamount = document.getElementById("bamount").value;
							var phone_number = document.getElementById("phone_number").value;
							var delivery_address = document.getElementById("delivery_address").value;
							var location = document.getElementById("location").value;

							var referral = document.getElementById("referral").value;


							// alert('Payment complete! Reference : ' + reference);

							var paystack_reference=response.reference;
							var reference = reference;

							// var verify = "<?php echo site_url();?>verify/paystack_reference";
							window.location = "<?php echo site_url();?>abuja_biggie_success?successfullypaid="+paystack_reference + '&pref='+reference + '&name='+name + '&email='+email + '&bamount='+bamount + '&phone_number='+phone_number + '&delivery_address='+delivery_address + '&location='+location +'&referral='+referral;

							// document.getElementById("paystack_reference_lsxpress").value = "paystack_reference_lsxpress";
						},
						onClose: function(){
							alert('Transaction was not completed, window closed.');
						}
					});
					handler.openIframe();

				}


			</script>


			<!-- specta-->
<!--			<form method="GET" action="" id="spectaForm">-->
<!--				-->
				<form method="" action="https://paywithspecta.com/pay?token=g%2fTSyvzSOUTDTXqyzBQmlA%2ftE8TH%2fr9XSi30a%2fhYh22eh4CwjtgZwPv%2f8BQTVJ7mbNnztM6GIsPHFa9Sa0%2bjZjlwak2Dh0iA8NTBip5jPH19Yb%2fD6VDS4fINFjy%2bL%2fGc6nqd67z6DWLJvwoajyBD99YaFralq1qt" id="spectaForm">


				<div class="form-group">
<!--					<input class="form-control"  type="hidden" id="token" name="token" value="g%2fTSyvzSOUTDTXqyzBQmlA">-->
<!--					-->
					<input class="form-control"  type="hidden" id="bamount" name="amount" value="<?php echo $_POST['bamount'] ;?>" readonly>
				</div>

				<div class="form-group">
					<input class="form-control"  type="hidden" id="merchant" name="merchantid" value="174646">
					<input class="form-control"  type="hidden" id="des" name="description" value="payment for biggie meat sharing">
					<input class="form-control"  type="hidden" id="ref" name="ref" value="<?php echo rand() ;?>">
				</div>


				<div class="form-group">
					<div class="col text-center">
<!--						<button class="btn btn-primary" type="button" onclick=" payWithSpecta(); ">Pay With Specta</button>-->
<!--						-->
						<button class="btn btn-primary" type="submit" >Pay With Specta</button>

					</div>
				</div>
			</form>

<script>
	// callBackUrl: function() {
	// 	var ref = document.getElementById("reference").value;
	// 	var amount = document.getElementById("bamount").value;
	// }

	var spectaForm = document.getElementById('spectaForm');
	spectaForm.addEventListener('submit', payWithSpecta, false);
	function payWithSpecta() {

		var myHeaders = new Headers();
		myHeaders.append("x-ApiKey", "TEST_API_KEY");
		myHeaders.append("Content-Type", "application/json");
		//get form data
		var ref = document.getElementById("reference").value;
		var amount = document.getElementById("bamount").value;
		var merchantid = '174646';
		var des = 'payment for biggie meat sharing';

		//window.location = "<?php echo site_url();?>AbjController/biggiesuccess?successfullypaid="+ref +  '&amount='+amount + '&merchantid='+merchantid + '&bamount='+bamount + '&des='+des;

		var share = 'http' + ref + ' reference ' + amount + 'bamount' + merchantid + '174646';

		var raw = "{\n    \"callBackUrl\": \"htt\",\n    \"reference\": \"test\",\n    \"merchantId\": \"174646\",\n    \"description\": \"test\",\n    \"amount\": \"233333.00\"\n}'";
		var requestOptions = {
			method: 'POST',
			headers: myHeaders,
			body: raw,
			redirect: 'follow'
		};

		fetch("https://paywithspectaapi.sterling.ng/api/Purchase/CreatePaymentUrl", requestOptions)
				.then(response => response.json())
				.then(result => console.log(result))
				.catch(error => console.log('error', error));
	}
</script>


		</div>

		<div class="col-lg-6">

		</div>

	</div>
</div>
