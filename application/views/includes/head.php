<!DOCTYPE html>
<html lang="en">
<head>
	<title>Meat247 - Buy and Sell Traceable, Affordable and Fit-for-Slaughter Livestock in Nigeria | Livestock247</title>
	<!--<base href="https://livestock247.com/" />-->
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- Favicons -->
	<link href="<?php echo base_url('assets/images/cow-logo.png')?>" rel="icon">
	<!--  CSS -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://fonts.google.com/?category=Sans+Serif">
	<!-- Bootstrap core CSS -->

		<link rel="stylesheet" href="<?php echo base_url() .'assets/build/css/intlTelInput.css'?>">
		<link rel="stylesheet" href="<?php echo base_url() .'assets/build/css/demo.css'?>">
	<link rel="stylesheet" href="<?php echo base_url() .'assets/old-css/bootstrap/css/bootstrap.css'?>">
<!--	<link rel="stylesheet" href="--><?php //echo base_url() .'assets/old-css/style.css'?><!--">-->
	<link rel="stylesheet" href="<?php echo base_url() .'assets/old-css/shop-homepage.css'?>">
	<link rel="stylesheet" href="<?php echo base_url() .'assets/old-css/custom.css'?>">
	<!-- Template Main CSS File -->
<!--	<link href="--><?php //echo base_url('assets/css/index.css')?><!--" rel="stylesheet">-->
	<link href="<?php echo base_url('assets/css/custom.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css')?>" rel="stylesheet">
	<!-- Custom styles for this template -->

	<!-- Facebook Pixel Code -->
	<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
				n.callMethod.apply(n,arguments):n.queue.push(arguments)};
			if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
			n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t,s)}(window, document,'script',
				'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '760599934877529');
		fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
				   src="https://www.facebook.com/tr?id=760599934877529&ev=PageView&noscript=1"
		/></noscript>
	<!-- End Facebook Pixel Code -->

	<!-- Facebook Pixel Code -->
	<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
				n.callMethod.apply(n,arguments):n.queue.push(arguments)};
			if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
			n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t,s)}(window,document,'script',
				'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '367833173841476');
		fbq('track', 'PageView');

		fbq('track', 'Contact');

	</script>
	<noscript>
		<img height="1" width="1"
			 src="https://www.facebook.com/tr?id=367833173841476&ev=PageView
&noscript=1"/>
	</noscript>
	<!-- End Facebook Pixel Code -->

	<!--track livestock-->
	<script type="text/javascript">
		function track(){
			var userInput = document.getElementById('userInput').value;
			var lnk = document.getElementById('lnk');
			lnk.href = "https://livestock247.com/aims/search_livestock?id=" + userInput;
			window.location = "https://livestock247.com/aims/search_livestock?id=" + userInput;
		}
	</script>
	<!-- End track livestock-->


	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-154570827-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-154570827-1');
		gtag('config', 'AW-619716805');

	</script>

	<!-- Event snippet for Website traffic conversion page --> <script> gtag('event', 'conversion', {'send_to': 'AW-619716805/6J5ZCOiJsdUBEMXBwKcC'}); </script>

	<!--Favicon-->
	<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon.png">
	<!--Zoho form css-->


	<!-- Google recaptcha  -->
	<script src="https://www.google.com/recaptcha/api.js"></script>

	<script src="https://www.google.com/recaptcha/api.js?render=reCAPTCHA_site_key"></script>
	<script>
		grecaptcha.ready(function() {
			grecaptcha.execute('reCAPTCHA_site_key', {action: 'homepage'}).then(function(token) {
			...
			});
		});
	</script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-154570827-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-154570827-1');
		gtag('config', 'AW-619716805');

	</script>

	<!-- Event snippet for Website traffic conversion page -->
	<script> gtag('event', 'conversion', {'send_to': 'AW-619716805/6J5ZCOiJsdUBEMXBwKcC'}); </script>


	<!--  Live Chat  -->
	<!--Start of Tawk.to Script-->
	<script type="text/javascript">
		var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
		(function(){
			var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
			s1.async=true;
			s1.src='https://embed.tawk.to/5cb20a8ed6e05b735b426b41/default';
			s1.charset='UTF-8';
			s1.setAttribute('crossorigin','*');
			s0.parentNode.insertBefore(s1,s0);
		})();
	</script>

	<!-- hide and show meat packages--->

	<style>
		#cowMeat{
			width: 100%;
			display: none;
		}

		#goatMeat{
			width: 100%;
			display: none;
		}

		#ramMeat{
			width: 100%;
			display: none;
		}

		#salesMeat{
			width: 100%;
			display: none;
		}

	</style>
	<script>
		function cowFunction() {
			var x = document.getElementById("cowMeat");
			if (x.style.display === "block") {
				x.style.display = "none";
			} else {
				x.style.display = "block";
			}
		}
	</script>

	<script>
		function goatFunction() {
			var x = document.getElementById("goatMeat");
			if (x.style.display === "block") {
				x.style.display = "none";
			} else {
				x.style.display = "block";
			}
		}
	</script>

	<script>
		function ramFunction() {
			var x = document.getElementById("ramMeat");
			if (x.style.display === "block") {
				x.style.display = "none";
			} else {
				x.style.display = "block";
			}
		}
	</script>

	<script>
		function salesFunction() {
			var x = document.getElementById("salesMeat");
			if (x.style.display === "block") {
				x.style.display = "none";
			} else {
				x.style.display = "block";
			}
		}
	</script>

	<script>
		// $(document).ready(function(){
		// 	$('[data-toggle="popover"]').popover();
		// });

		$('#pop').popover();
	</script>



</head>

<body>
<!-- Twitter universal website tag code -->
<script>
	!function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
	},s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
			a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
	// Insert Twitter Pixel ID and Standard Event data below
	twq('init','o0ibr');
	twq('track','PageView');
</script>
<!-- End Twitter universal website tag code -->
</body>
</html>
