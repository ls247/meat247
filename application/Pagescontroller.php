<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagescontroller extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{

		$this->load->view('includes/head');
		$this->load->view('includes/header');
        $this->load->view('pages/index');
        $this->load->view('includes/footer');
    }
    public function verify($reference = '')
	{

        $data['reference'] = $reference;
       
        $this->load->view('includes/header');
        $this->load->view('pages/verify', $data);
        $this->load->view('includes/footer');
    }
    public function webhook(Request $request)
	{
        //This receives the webhook
   
        //$email = json_decode($input['data']['customer']['email']);
       
        $this->load->view('includes/header');
        $this->load->view('pages/webhook');
        //get form's data and store in local varable
        $email=$request->input('data.customer.email');
        $bankname=$request->input('data.gateway_response');
        //$bankname = json_decode($input['data']['gateway_response']);
        $user =$this->db->where('email', $email);
        $user->bankname = $bankname;
        $user->save();

        return response()->json('processed', 200);
        
        $this->load->view('includes/footer');
    }

	public function lagos()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		$this->load->view('pages/lagos');
		$this->load->view('includes/footer');
	}

//Lagos package functions start here
	public function biggie()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load registration view form

		$this->load->helper('form');
		$this->load->helper('url');
		//load registration view form
		//load Model
		$this->load->database();
		$this->load->library('session');

		// validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('form/biggie');
			$this->load->view('includes/footer');
		} else {
			//load Model
			$this->load->model('Referral_model');
			$fname = $this->input->post('name');
			$phone_number = $this->input->post('phone_number');
			$delivery_address = $this->input->post('delivery_address');
			$delivery_date = $this->input->post('delivery_date');
			$location = $this->input->post('location');
			$email = $this->input->post('email');
			$plan = "Biggie";
			$bamount = $this->input->post('bamount');
			$amount = '48100';
			$reference = $this->input->post('reference');
			$paystack_status="Pending";
			$referral_code = $this->input->post('referral');
			$code = $this->Referral_model->referral($referral_code);

			if($referral_code== $code) {
				//get percentage for a particular user base on referral code entered
				$query = $this->db->get_where('users',array('referral' => $referral_code));
				$row = $query->row(0);

				$percentage = isset($row->percentage) ? $row->percentage : 0; // access attributes
				//agent commission
				$commission = $amount * $percentage /100;
				$agent_commission = $commission;
				// customer's discount
				$c_per =1;
				$c_discount = $amount * $c_per / 100;
				$discount_amount= $bamount - $c_discount;
				$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,delivery_date, location,phone_number,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$delivery_date', '$location', '$phone_number','$plan', '$referral_code','$agent_commission', '$c_discount','$bamount','$reference','$paystack_status', 'Not Paid')");
				//redirect('biggie_pay');
				$this->load->view('form/biggie_pay');
			}

			elseif (empty($referral_code))
			{
				$agent_commission = 0;
				$c_discount = 0;
				$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,delivery_date, location,phone_number,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$delivery_date', '$location', '$phone_number','$plan', '$referral_code','$agent_commission', '$c_discount','$bamount','$reference','$paystack_status', 'Not Paid')");
				//redirect('biggie_pay');
				$this->load->view('form/biggie_pay');
			}
			else {
				$this->session->set_flashdata('code', 'Invalid Referral Code');
				$this->load->view('form/biggie');
			}

		}
	}

	public function biggie_pay()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');

		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('date');
		//load library
		$this->load->library('pagination');
		$this->load->library('table');
		$this->load->library('calendar');
		$this->load->model('meat_model');

		$callbackurl = site_url(). 'Specta/biggie_verify';
		$ref = $this->input->post('reference');
		$merchant = $this->input->post('merchantid');
		//$plan = $this->input->post('plan');
		$email = $this->input->post('email');
		$amount = $this->input->post('bamount');


		if ($this->input->post('specta_btn')) {

			//update payment status
			$meat['specta_status'] = 'in-progress';
			$this->meat_model->update($meat, $ref);

			$data = array(
				"callBackUrl" => $callbackurl,
				"reference" => $ref,
				"merchantId" => $merchant,
				"description" => $email,
				"amount" => $amount
			);

			$post_data = json_encode($data);

			// Prepare new cURL resource
			$crl = curl_init('https://paywithspectaapi.sterling.ng/api/Purchase/CreatePaymentUrl');
			curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($crl, CURLINFO_HEADER_OUT, true);
			curl_setopt($crl, CURLOPT_POST, true);
			curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);

			// Set HTTP Header for POST request -
			// TEST_API_KEY
			// 969688b48dad4b8981be20fc035da527
			curl_setopt($crl, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'x-ApiKey: 969688b48dad4b8981be20fc035da527',
					'Content-Length: ' . strlen($post_data))
			);
			// Submit the POST request
			$result = curl_exec($crl);

			curl_close($crl);

			$result = json_decode($result);
			//print_r($result);
			// handle curl error
			if ((isset($result->success) && $result->success) && isset($result->result)) {
				redirect($result->result);
				exit;
			} else {

				$result_noti = 1; die();
			}
		}
		//load page
		$this->load->view('form/biggie_pay');
		$this->load->view('includes/footer');
	}



	public function biggiesuccess($biggiesuccess = '')
	{
		$this->load->database();
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load database
		$data['biggiesuccess'] = $biggiesuccess;
		$successfullypaid = $this->input->get('successfullypaid');
		$pref = $this->input->get('pref');
		$fname = $this->input->get('name');
		$phone_number = $this->input->get('phone_number');
		$delivery_address = $this->input->get('delivery_address');
		$delivery_date = $this->input->get('delivery_date');
		$location = $this->input->get('location');
		$email = $this->input->get('email');
		$bamount = $this->input->get('bamount');
		$reference = $this->input->get('reference');
		$referral = $this->input->get('referral');
		$status="Success";
		//  $verify = $this->input->get('verify');
		// $this->db->query("INSERT INTO biggie(paystack_reference_meat247) VALUES ('$successfullypaid')");

		$sql="UPDATE meat_sharing SET paystack_reference = '$successfullypaid', paystack_status = '$status'  WHERE  reference='$pref' ";
		$this->db->query($sql);

//agent email

		//send email
		//set up email
		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'admin@livestock247.com', // change it to yours
			'smtp_pass' => 'b861N8cPchr3', // change it to yours
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);

        $to = $email;
       $groupmail = 'meat247@livestock247.com';
	//	$groupmail = 'esther.akowe@livestock247.com';

		$message = "
						<html>
						<head>
							<title>New Lagos Meat247 Sales</title>
						</head>
						<body>
							
							<p>Hi $fname, Your payment of NGN $bamount for a Biggie Meat Sharing Package was successful
    						</p>
    						<p>
    						 <strong>Transaction Details:</strong> <br>
							Payment ID: 			$pref<br>
							Name:  					$fname<br>
							Email: 					$email<br>
							Phone: 					$phone_number<br>
							Delivery Address:		$delivery_address<br>
							Delivery Date (Y/M/D): 		$delivery_date <br>
							Location:				$location
						</p>
							
					
					
						</body>
						</html>
						";

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from($config['smtp_user']);
		$this->email->to($to);
		$this->email->cc($groupmail);

		$this->email->subject('Lagos Meat247 Biggie Request');
        $this->email->message($message);



		if ($this->email->send()) {

		}
		//load view
		$this->load->view('form/biggie_success', $data);
		$this->output->set_header('refresh:5;'.site_url());
		$this->load->view('includes/footer');
	}


	public function midi()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		$this->load->helper('form');
		$this->load->helper('url');
		//load registration view form
		$this->load->database();
		//load Model
		$this->load->model('Referral_model');
		$this->load->library('session');

		// validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('form/midi');
			$this->load->view('includes/footer');
		} else {
			$fname = $this->input->post('name');
			$phone_number = $this->input->post('phone_number');
			$delivery_address = $this->input->post('delivery_address');
			$delivery_date = $this->input->post('delivery_date');
			$location = $this->input->post('location');
			$email = $this->input->post('email');
			$plan = "Midi";
			$bamount = $this->input->post('midi_amount');
			$amount = '24800';
			$reference = $this->input->post('reference');
			$paystack_status="Pending";
			$referral_code = $this->input->post('referral');
			$code = $this->Referral_model->referral($referral_code);

			if($referral_code== $code) {
				//get percentage for a particular user base on referral code entered
				$query = $this->db->get_where('users',array('referral' => $referral_code));
				$row = $query->row(0);

				$percentage = isset($row->percentage) ? $row->percentage : 0; // access attributes
				//agent commission
				$commission = $amount * $percentage /100;
				$agent_commission = $commission;
				// customer's discount
				$c_per =1;
				$c_discount = $amount * $c_per / 100;
				$discount_amount= $bamount - $c_discount;
				$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,delivery_date, location,phone_number,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$delivery_date', '$location', '$phone_number','$plan', '$referral_code','$agent_commission', '$c_discount','$bamount','$reference','$paystack_status', 'Not Paid')");
				$this->load->view('form/midi_pay');
			}

			elseif (empty($referral_code))
			{
				$agent_commission = 0;
				$c_discount = 0;
				$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,delivery_date, location,phone_number,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$delivery_date', '$location', '$phone_number','$plan', '$referral_code','$agent_commission', '$c_discount','$bamount','$reference','$paystack_status', 'Not Paid')");

				$this->load->view('form/midi_pay');
			}
			else {
				$this->session->set_flashdata('code', 'Invalid Referral Code');
				$this->load->view('form/midi');
			}

		}

    }

	public function midi_pay()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		$this->load->database();
		//load registration view form
		$this->load->model('meat_model');

		$callbackurl = site_url(). 'Specta/midi_verify';
		$ref = $this->input->post('reference');
		$merchant = $this->input->post('merchantid');
		//$plan = $this->input->post('plan');
		$email = $this->input->post('email');
		$amount = $this->input->post('midi_amount');

		if ($this->input->post('specta_btn')) {
			//update payment status
			$meat['specta_status'] = 'in-progress';
			$this->meat_model->update($meat, $ref);

			$data = array(
				"callBackUrl" => $callbackurl,
				"reference" => $ref,
				"merchantId" => $merchant,
				"description" => $email,
				"amount" => $amount
			);

			$post_data = json_encode($data);

			// Prepare new cURL resource
			$crl = curl_init('https://paywithspectaapi.sterling.ng/api/Purchase/CreatePaymentUrl');
			curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($crl, CURLINFO_HEADER_OUT, true);
			curl_setopt($crl, CURLOPT_POST, true);
			curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);

			// Set HTTP Header for POST request -
			// TEST_API_KEY
			// 969688b48dad4b8981be20fc035da527
			curl_setopt($crl, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'x-ApiKey: 969688b48dad4b8981be20fc035da527',
					'Content-Length: ' . strlen($post_data))
			);
			// Submit the POST request
			$result = curl_exec($crl);

			curl_close($crl);

			$result = json_decode($result);
			//print_r($result);
			// handle curl error
			if ((isset($result->success) && $result->success) && isset($result->result)) {
				redirect($result->result);
				exit;
			} else {

				$result_noti = 1; die();
			}
		}

		$this->load->view('form/midi_pay');
		$this->load->view('includes/footer');
	}


	public function midisuccess($midisuccess = '')
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load database
		$data['midisuccess'] = $midisuccess;
		$successfullypaid = $this->input->get('successfullypaid');
		$pref = $this->input->get('pref');
		$fname = $this->input->get('name');
		$phone_number = $this->input->get('phone_number');
		$delivery_address = $this->input->get('delivery_address');
		$delivery_date = $this->input->get('delivery_date');
		$location = $this->input->get('location');
		$email = $this->input->get('email');
		$midi_amount = $this->input->get('midi_amount');
		$reference = $this->input->get('reference');
		$status="Success";
		//  $verify = $this->input->get('verify');
		// $this->db->query("INSERT INTO biggie(paystack_reference_meat247) VALUES ('$successfullypaid')");

		$sql="UPDATE meat_sharing SET paystack_reference = '$successfullypaid', paystack_status = '$status'  WHERE  reference='$pref' ";
		$this->db->query($sql);

//agent email
		//send email
		//set up email
		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'admin@livestock247.com', // change it to yours
			'smtp_pass' => 'b861N8cPchr3', // change it to yours
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);

		$to = $email;
		$groupmail = 'meat247@livestock247.com';
		//$groupmail = 'esther.akowe@livestock247.com';

		$message = "
						<html>
						<head>
							<title>New Lagos Meat247 Sales</title>
						</head>
						<body>
							
							<p>Hi $fname, Your payment of NGN $midi_amount for a Midi Meat Sharing Package was successful
    						</p>
    						<p>
    						 <strong>Transaction Details:</strong> <br>
							Payment ID: 			$pref<br>
							Name:  					$fname<br>
							Email: 					$email<br>
							Phone: 					$phone_number<br>
							Delivery Address:		$delivery_address<br>
							Delivery Date (Y/M/D): 		$delivery_date <br>
							Location:				$location
						</p>
							
					
					
						</body>
						</html>
						";

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from($config['smtp_user']);
		$this->email->to($to);
		$this->email->cc($groupmail);

		$this->email->subject('Lagos Meat247 Midi Request');
		$this->email->message($message);

		if ($this->email->send()) {

		}
		//load view
		$this->load->view('form/midi_success', $data);
		$this->output->set_header('refresh:5;'.site_url());
		$this->load->view('includes/footer');
    }

    public function mini()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		$this->load->helper('form');
		$this->load->helper('url');
		//load registration view form
		$this->load->database();
		//load Model
		$this->load->model('Referral_model');
		$this->load->library('session');

		// validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('form/mini');
			$this->load->view('includes/footer');
		} else {
			$fname = $this->input->post('name');
			$phone_number = $this->input->post('phone_number');
			$delivery_address = $this->input->post('delivery_address');
			$delivery_date = $this->input->post('delivery_date');
			$location = $this->input->post('location');
			$email = $this->input->post('email');
			$plan = "Mini";
			$bamount = $this->input->post('mini_amount');
			$amount = '12900';
			$reference = $this->input->post('reference');
			$paystack_status="Pending";
			$referral_code = $this->input->post('referral');
			$code = $this->Referral_model->referral($referral_code);

			if($referral_code== $code) {
				//get percentage for a particular user base on referral code entered
				$query = $this->db->get_where('users',array('referral' => $referral_code));
				$row = $query->row(0);

				$percentage = isset($row->percentage) ? $row->percentage : 0; // access attributes
				//agent commission
				$commission = $amount * $percentage /100;
				$agent_commission = $commission;
				// customer's discount
				$c_per =1;
				$c_discount = $amount * $c_per / 100;
				$discount_amount= $bamount - $c_discount;
				$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,delivery_date, location,phone_number,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$delivery_date', '$location', '$phone_number','$plan', '$referral_code','$agent_commission', '$c_discount','$bamount','$reference','$paystack_status', 'Not Paid')");
				$this->load->view('form/mini_pay');
			}

			elseif (empty($referral_code))
			{
				$agent_commission = 0;
				$c_discount = 0;
				$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,delivery_date, location,phone_number,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$delivery_date', '$location', '$phone_number','$plan', '$referral_code','$agent_commission', '$c_discount','$bamount','$reference','$paystack_status', 'Not Paid')");

				$this->load->view('form/mini_pay');
			}
			else {
				$this->session->set_flashdata('code', 'Invalid Referral Code');
				$this->load->view('form/mini');
			}

		}
	}

	public function mini_pay()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		$this->load->database();
		//load registration view form
		$this->load->model('meat_model');
		$this->load->view('form/mini_pay');
		$this->load->view('includes/footer');
	}


	public function minisuccess($minisuccess = '')
    {
		$this->load->view('includes/head');
    	$this->load->view('includes/header');
		//load database
		$data['midisuccess'] = $minisuccess;
		$successfullypaid = $this->input->get('successfullypaid');
		$pref = $this->input->get('pref');
		$fname = $this->input->get('name');
		$phone_number = $this->input->get('phone_number');
		$delivery_address = $this->input->get('delivery_address');
		$delivery_date = $this->input->get('delivery_date');
		$location = $this->input->get('location');
		$email = $this->input->get('email');
		$mini_amount = $this->input->get('mini_amount');
		$reference = $this->input->get('reference');
		$referral = $this->input->get('referral');
		$status="Success";
		//  $verify = $this->input->get('verify');
		// $this->db->query("INSERT INTO biggie(paystack_reference_meat247) VALUES ('$successfullypaid')");

		$sql="UPDATE meat_sharing SET paystack_reference = '$successfullypaid', paystack_status = '$status'  WHERE  reference='$pref' ";
		$this->db->query($sql);

		//agent email
		$agent_email= $this->db->get('users', array('email' => 'email', 'referral' => $referral));


		//send email
		//set up email
		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'admin@livestock247.com', // change it to yours
			'smtp_pass' => 'b861N8cPchr3', // change it to yours
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);

		$to = $email;
		$groupmail = 'meat247@livestock247.com';
		//$groupmail = 'esther.akowe@livestock247.com';

		$message = "
						<html>
						<head>
							<title>New Lagos Meat247 Sales</title>
						</head>
						<body>
							
							<p>Hi $fname, Your payment of NGN $mini_amount for a Mini Meat Sharing Package was successful
    						</p>
    						<p>
    						 <strong>Transaction Details:</strong> <br>
						
							Payment ID: 			$pref<br>
							Name:  					$fname<br>
							Email: 					$email<br>
							Phone: 					$phone_number<br>
							Delivery Address:		$delivery_address<br>
							Delivery Date (Y/M/D): 	$delivery_date <br>
							Location:				$location
						</p>
							
					
					
						</body>
						</html>
						";

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from($config['smtp_user']);
		$this->email->to($to);
		$this->email->cc($groupmail, $agent_email);

		$this->email->subject('Lagos Meat247 Mini Request');
		$this->email->message($message);

		if ($this->email->send()) {


		}
		//load view
		$this->load->view('form/mini_success', $data);
		$this->output->set_header('refresh:5;'.site_url());
		$this->load->view('includes/footer');
    }


	public function goat_large()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load registration view form

		$this->load->helper('form');
		$this->load->helper('url');
		//load registration view form
		//load Model
		$this->load->database();
		$this->load->library('session');

		// validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('form/goat_large');
			$this->load->view('includes/footer');
		} else {
			//load Model
			$this->load->model('Referral_model');
			$fname = $this->input->post('name');
			$phone_number = $this->input->post('phone_number');
			$delivery_address = $this->input->post('delivery_address');
			$delivery_date = $this->input->post('delivery_date');
			$process = $this->input->post('process');
			$email = $this->input->post('email');
			$plan = "Large";
			$goat_amount = $this->input->post('goat_amount');
			$amount = '29800';
			$reference = $this->input->post('reference');
			$paystack_status="Pending";
			$referral_code = $this->input->post('referral');
			$code = $this->Referral_model->referral($referral_code);

			if($referral_code== $code) {
				//get percentage for a particular user base on referral code entered
				$query = $this->db->get_where('users',array('referral' => $referral_code));
				$row = $query->row(0);

				$percentage = isset($row->percentage) ? $row->percentage : 0; // access attributes
				//agent commission
				$commission = $amount * $percentage /100;
				$agent_commission = $commission;
				// customer's discount
				$c_per =1;
				$c_discount = $amount * $c_per / 100;
				//$discount_amount= $goat_amount - $c_discount;
				$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,delivery_date, phone_number,process,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$delivery_date' ,'$phone_number', '$process', '$plan', '$referral_code','$agent_commission', '$c_discount','$goat_amount','$reference','$paystack_status', 'Not Paid')");
				$this->load->view('form/goat_large_pay');
			}

			elseif (empty($referral_code))
			{
				$agent_commission = 0;
				$c_discount = 0;
				$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,delivery_date, phone_number,process,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$delivery_date' ,'$phone_number', '$process', '$plan', '$referral_code','$agent_commission', '$c_discount','$goat_amount','$reference','$paystack_status', 'Not Paid')");
				//redirect('biggie_pay');
				$this->load->view('form/goat_large_pay');
			}
			else {
				$this->session->set_flashdata('code', 'Invalid Referral Code');
				$this->load->view('form/goat_large');
			}
		}
	}

	public function goat_large_pay()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		$this->load->database();

		$this->load->model('meat_model');

		$callbackurl = site_url(). 'Specta/large_verify';
		$ref = $this->input->post('reference');
		$merchant = $this->input->post('merchantid');
		//$plan = $this->input->post('plan');
		$email = $this->input->post('email');
		$amount = $this->input->post('goat_amount');

		if ($this->input->post('specta_btn')) {
			//update payment status
			$meat['specta_status'] = 'in-progress';
			$this->meat_model->update($meat, $ref);

			$data = array(
				"callBackUrl" => $callbackurl,
				"reference" => $ref,
				"merchantId" => $merchant,
				"description" => $email,
				"amount" => $amount
			);

			$post_data = json_encode($data);

			// Prepare new cURL resource
			$crl = curl_init('https://paywithspectaapi.sterling.ng/api/Purchase/CreatePaymentUrl');
			curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($crl, CURLINFO_HEADER_OUT, true);
			curl_setopt($crl, CURLOPT_POST, true);
			curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);

			// Set HTTP Header for POST request -
			// TEST_API_KEY
			// 969688b48dad4b8981be20fc035da527
			curl_setopt($crl, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'x-ApiKey: 969688b48dad4b8981be20fc035da527',
					'Content-Length: ' . strlen($post_data))
			);
			// Submit the POST request
			$result = curl_exec($crl);

			curl_close($crl);

			$result = json_decode($result);
			//print_r($result);
			// handle curl error
			if ((isset($result->success) && $result->success) && isset($result->result)) {
				redirect($result->result);
				exit;
			} else {

				$result_noti = 1; die();
			}
		}
		$this->load->view('form/goat_large_pay');
		$this->load->view('includes/footer');
	}


	public function goat_large_success($goat_large_success = '')
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load database
		$data['goat_large_success'] = $goat_large_success;
		$successfullypaid = $this->input->get('successfullypaid');
		$pref = $this->input->get('pref');
		$fname = $this->input->get('name');
		$phone_number = $this->input->get('phone_number');
		$delivery_address = $this->input->get('delivery_address');
		$delivery_date = $this->input->get('delivery_date');
		$process = $this->input->get('process');
		$email = $this->input->get('email');
		$goat_amount = $this->input->get('goat_amount');
		$reference = $this->input->get('reference');
		$referral = $this->input->get('referral');
		$status="Success";
		//  $verify = $this->input->get('verify');
		// $this->db->query("INSERT INTO biggie(paystack_reference_meat247) VALUES ('$successfullypaid')");

		$sql="UPDATE meat_sharing SET paystack_reference = '$successfullypaid', paystack_status = '$status'  WHERE  reference='$pref' ";
		$this->db->query($sql);

		//agent email
		$agent_email= $this->db->get('users', array('email' => 'email', 'referral' => $referral));


		//send email
		//set up email
		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'admin@livestock247.com', // change it to yours
			'smtp_pass' => 'b861N8cPchr3', // change it to yours
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);

		$to = $email;
		$groupmail = 'meat247@livestock247.com';
		//$groupmail = 'esther.akowe@livestock247.com';

		$message = "
						<html>
						<head>
							<title>New Meat247 Sales</title>
						</head>
						<body>
							
							<p>Hi $fname, Your payment of NGN $goat_amount for a Large Goat Meat Package was successful
    						</p>
    						<p>
    						 <strong>Transaction Details:</strong> <br>
							Payment ID: 			$pref<br>
							Name:  					$fname<br>
							Email: 					$email<br>
							Phone: 					$phone_number<br>
							Delivery Address:		$delivery_address<br>
							Delivery Date (Y/M/D):	$delivery_date<br>
							How to Process:			$process
						</p>
							
					
					
						</body>
						</html>
						";

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from($config['smtp_user']);
		$this->email->to($to);
		$this->email->cc($groupmail, $agent_email);
		$this->email->subject('Meat247 Large Goat Request');
		$this->email->message($message);

		if ($this->email->send()) {

		}
		//load view
		$this->load->view('form/goat_large_success', $data);
		$this->output->set_header('refresh:5;'.site_url());
		$this->load->view('includes/footer');
	}


	public function goat_medium()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load registration view form

		$this->load->helper('form');
		$this->load->helper('url');
		//load registration view form
		//load Model
		$this->load->database();
		$this->load->library('session');

		// validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('form/goat_medium');
			$this->load->view('includes/footer');
		} else {
			//load Model
			$this->load->model('Referral_model');
			$fname = $this->input->post('name');
			$phone_number = $this->input->post('phone_number');
			$delivery_address = $this->input->post('delivery_address');
			$delivery_date = $this->input->post('delivery_date');
			$process = $this->input->post('process');
			$email = $this->input->post('email');
			$plan = "Medium";
			$goat_amount = $this->input->post('goat_amount');
			$amount = '25000';
			$reference = $this->input->post('reference');
			$paystack_status="Pending";
			$referral_code = $this->input->post('referral');
			$code = $this->Referral_model->referral($referral_code);

			if($referral_code== $code) {
				//get percentage for a particular user base on referral code entered
				$query = $this->db->get_where('users',array('referral' => $referral_code));
				$row = $query->row(0);

				$percentage = isset($row->percentage) ? $row->percentage : 0; // access attributes
				//agent commission
				$commission = $amount * $percentage /100;
				$agent_commission = $commission;
				// customer's discount
				$c_per =1;
				$c_discount = $amount * $c_per / 100;
				//$discount_amount= $goat_amount - $c_discount;
			$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,delivery_date, phone_number,process,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$delivery_date' ,'$phone_number', '$process', '$plan', '$referral_code','$agent_commission', '$c_discount','$goat_amount','$reference','$paystack_status', 'Not Paid')");
				$this->load->view('form/goat_medium_pay');
			}

	elseif (empty($referral_code))
	{
	$agent_commission = 0;
		$c_discount = 0;
		$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,delivery_date, phone_number,process,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$delivery_date' ,'$phone_number', '$process', '$plan', '$referral_code','$agent_commission', '$c_discount','$goat_amount','$reference','$paystack_status', 'Not Paid')");
		//redirect('biggie_pay');
				$this->load->view('form/goat_medium_pay');
			}
			else {
				$this->session->set_flashdata('code', 'Invalid Referral Code');
				$this->load->view('form/goat_medium');
			}
		}
	}


	public function goat_medium_pay()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		$this->load->database();

		$this->load->model('meat_model');

		$callbackurl = site_url(). 'Specta/medium_verify';
		$ref = $this->input->post('reference');
		$merchant = $this->input->post('merchantid');
		//$plan = $this->input->post('plan');
		$email = $this->input->post('email');
		$amount = $this->input->post('goat_amount');

		if ($this->input->post('specta_btn')) {
			//update payment status
			$meat['specta_status'] = 'in-progress';
			$this->meat_model->update($meat, $ref);

			$data = array(
				"callBackUrl" => $callbackurl,
				"reference" => $ref,
				"merchantId" => $merchant,
				"description" => $email,
				"amount" => $amount
			);

			$post_data = json_encode($data);

			// Prepare new cURL resource
			$crl = curl_init('https://paywithspectaapi.sterling.ng/api/Purchase/CreatePaymentUrl');
			curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($crl, CURLINFO_HEADER_OUT, true);
			curl_setopt($crl, CURLOPT_POST, true);
			curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);

			// Set HTTP Header for POST request -
			// TEST_API_KEY
			// 969688b48dad4b8981be20fc035da527
			curl_setopt($crl, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'x-ApiKey: 969688b48dad4b8981be20fc035da527',
					'Content-Length: ' . strlen($post_data))
			);
			// Submit the POST request
			$result = curl_exec($crl);

			curl_close($crl);

			$result = json_decode($result);
			//print_r($result);
			// handle curl error
			if ((isset($result->success) && $result->success) && isset($result->result)) {
				redirect($result->result);
				exit;
			} else {

				$result_noti = 1; die();
			}
		}
		$this->load->view('form/goat_medium_pay');
		$this->load->view('includes/footer');
	}


	public function goat_medium_success($goat_medium_success = '')
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load database
		$data['goat_medium_success'] = $goat_medium_success;
		$successfullypaid = $this->input->get('successfullypaid');
		$pref = $this->input->get('pref');
		$fname = $this->input->get('name');
		$phone_number = $this->input->get('phone_number');
		$delivery_address = $this->input->get('delivery_address');
		$delivery_date = $this->input->get('delivery_date');
		$process = $this->input->get('process');
		$email = $this->input->get('email');
		$goat_amount = $this->input->get('goat_amount');
		$reference = $this->input->get('reference');
		$referral = $this->input->get('referral');
		$status="Success";
		//  $verify = $this->input->get('verify');
		// $this->db->query("INSERT INTO biggie(paystack_reference_meat247) VALUES ('$successfullypaid')");

		$sql="UPDATE meat_sharing SET paystack_reference = '$successfullypaid', paystack_status = '$status'  WHERE  reference='$pref' ";
		$this->db->query($sql);

		//agent email
		$agent_email= $this->db->get('users', array('email' => 'email', 'referral' => $referral));


		//send email
		//set up email
		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'admin@livestock247.com', // change it to yours
			'smtp_pass' => 'b861N8cPchr3', // change it to yours
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);

		$to = $email;
		//$groupmail = 'esther.akowe@livestock247.com';
		$groupmail = 'meat247@livestock247.com';

		$message = "
						<html>
						<head>
							<title>New Meat247 Sales</title>
						</head>
						<body>
							
							<p>Hi $fname, Your payment of NGN $goat_amount for a Medium Goat Meat Package was successful
    						</p>
    						<p>
    						 <strong>Transaction Details:</strong> <br>
							Payment ID: 		$pref<br>
							Name:  				$fname<br>
							Email: 				$email<br>
							Phone: 				$phone_number<br>
							Delivery Address:	$delivery_address<br>
							Delivery Date (Y/M/D):	$delivery_date<br>
							How to Process:			$process
						</p>
						</p>
							
					
					
						</body>
						</html>
						";

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from($config['smtp_user']);
		$this->email->to($to);
		$this->email->cc($groupmail, $agent_email);
		$this->email->subject('Meat247 Medium Goat Request');
		$this->email->message($message);

		if ($this->email->send()) {

		}
		//load view
		$this->load->view('form/goat_medium_success', $data);
		$this->output->set_header('refresh:5;'.site_url());
		$this->load->view('includes/footer');
	}


	public function goat_small()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load registration view form

		$this->load->helper('form');
		$this->load->helper('url');
		//load registration view form
		//load Model
		$this->load->database();
		$this->load->library('session');

		// validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('form/goat_small');
			$this->load->view('includes/footer');
		} else {
			//load Model
			$this->load->model('Referral_model');
			$fname = $this->input->post('name');
			$phone_number = $this->input->post('phone_number');
			$delivery_address = $this->input->post('delivery_address');
			$delivery_date = $this->input->post('delivery_date');
			$process = $this->input->post('process');
			$email = $this->input->post('email');
			$plan = "Small";
			$goat_amount = $this->input->post('goat_amount');
			$amount = '18500';
			$reference = $this->input->post('reference');
			$paystack_status="Pending";
			$referral_code = $this->input->post('referral');
			$code = $this->Referral_model->referral($referral_code);

			if($referral_code== $code) {
				//get percentage for a particular user base on referral code entered
				$query = $this->db->get_where('users',array('referral' => $referral_code));
				$row = $query->row(0);

				$percentage = isset($row->percentage) ? $row->percentage : 0; // access attributes
				//agent commission
				$commission = $amount * $percentage /100;
				$agent_commission = $commission;
				// customer's discount
				$c_per =1;
				$c_discount = $amount * $c_per / 100;
				//$discount_amount= $goat_amount - $c_discount;
				$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,delivery_date, phone_number,process,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$delivery_date' ,'$phone_number', '$process', '$plan', '$referral_code','$agent_commission', '$c_discount','$goat_amount','$reference','$paystack_status', 'Not Paid')");
				$this->load->view('form/goat_small_pay');
		}

	elseif (empty($referral_code))
	{
	$agent_commission = 0;
		$c_discount = 0;
		$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,delivery_date, phone_number,process,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$delivery_date' ,'$phone_number', '$process', '$plan', '$referral_code','$agent_commission', '$c_discount','$goat_amount','$reference','$paystack_status', 'Not Paid')");
		//redirect('biggie_pay');
				$this->load->view('form/goat_small_pay');
			}
			else {
				$this->session->set_flashdata('code', 'Invalid Referral Code');
				$this->load->view('form/goat_small');
			}
		}
	}


	public function goat_small_pay()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load registration view form
		$this->load->view('form/goat_small_pay');
//		$this->load->helper('form');
//		$this->load->helper('url');
		$this->load->database();
		$this->load->view('includes/footer');
	}


	public function goat_small_success($goat_small_success = '')
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load database
		$data['goat_small_success'] = $goat_small_success;
		$successfullypaid = $this->input->get('successfullypaid');
		$pref = $this->input->get('pref');
		$fname = $this->input->get('name');
		$phone_number = $this->input->get('phone_number');
		$delivery_address = $this->input->get('delivery_address');
		$delivery_date = $this->input->get('delivery_date');
		$process = $this->input->get('process');
		$email = $this->input->get('email');
		$goat_amount = $this->input->get('goat_amount');
		$reference = $this->input->get('reference');
		$referral = $this->input->get('referral');
		$status="Success";
		//  $verify = $this->input->get('verify');
		// $this->db->query("INSERT INTO biggie(paystack_reference_meat247) VALUES ('$successfullypaid')");

		$sql="UPDATE meat_sharing SET paystack_reference = '$successfullypaid', paystack_status = '$status'  WHERE  reference='$pref' ";
		$this->db->query($sql);

		//agent email
		$agent_email= $this->db->get('users', array('email' => 'email', 'referral' => $referral));


		//send email
		//set up email
		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'admin@livestock247.com', // change it to yours
			'smtp_pass' => 'b861N8cPchr3', // change it to yours
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);

		$to = $email;
		//$groupmail = 'esther.akowe@livestock247.com';
		$groupmail = 'meat247@livestock247.com';

		$message = "
						<html>
						<head>
							<title>New Meat247 Sales</title>
						</head>
						<body>
							
							<p>Hi $fname, Your payment of NGN $goat_amount for a Small Goat Meat Package was successful
    						</p>
    						<p>
    						 <strong>Transaction Details:</strong> <br>
							Payment ID: 		$pref<br>
							Name:  				$fname<br>
							Email: 				$email<br>
							Phone: 				$phone_number<br>
							Delivery Address:	$delivery_address<br>
							Delivery Date (Y/M/D):	$delivery_date<br>
							How to Process:			$process
						</p>
							
					
					
						</body>
						</html>
						";

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from($config['smtp_user']);
		$this->email->to($to);
		$this->email->cc($groupmail, $agent_email);
		$this->email->subject('Meat247 Small Goat Request');
		$this->email->message($message);

		if ($this->email->send()) {

		}
		//load view
		$this->load->view('form/goat_small_success', $data);
		$this->output->set_header('refresh:5;'.site_url());
		$this->load->view('includes/footer');
	}


	public function ram_maxi()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load registration view form

		$this->load->helper('form');
		$this->load->helper('url');
		//load registration view form
		//load Model
		$this->load->database();
		$this->load->library('session');

		// validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('form/ram_maxi');
			$this->load->view('includes/footer');
		} else {
			//load Model
			$this->load->model('Referral_model');
			$fname = $this->input->post('name');
			$phone_number = $this->input->post('phone_number');
			$delivery_address = $this->input->post('delivery_address');
			$delivery_date = $this->input->post('delivery_date');
			$process = $this->input->post('process');
			$email = $this->input->post('email');
			$plan = "Maxi";
			$ram_amount = $this->input->post('ram_amount');
			$amount = '75000';
			$reference = $this->input->post('reference');
			$paystack_status="Pending";
			$referral_code = $this->input->post('referral');
			$code = $this->Referral_model->referral($referral_code);

			if($referral_code== $code) {
				//get percentage for a particular user base on referral code entered
				$query = $this->db->get_where('users',array('referral' => $referral_code));
				$row = $query->row(0);

				$percentage = isset($row->percentage) ? $row->percentage : 0; // access attributes
				//agent commission
				$commission = $amount * $percentage /100;
				$agent_commission = $commission;
				// customer's discount
				$c_per =1;
				$c_discount = $amount * $c_per / 100;
				//$discount_amount= $goat_amount - $c_discount;
				$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,delivery_date, phone_number,process,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$delivery_date' ,'$phone_number', '$process', '$plan', '$referral_code','$agent_commission', '$c_discount','$ram_amount','$reference','$paystack_status', 'Not Paid')");
				$this->load->view('form/ram_maxi_pay');
		}

	elseif (empty($referral_code))
	{
	$agent_commission = 0;
		$c_discount = 0;
		$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,delivery_date, phone_number,process,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$delivery_date' ,'$phone_number', '$process', '$plan', '$referral_code','$agent_commission', '$c_discount','$ram_amount','$reference','$paystack_status', 'Not Paid')");
		//redirect('biggie_pay');
				$this->load->view('form/ram_maxi_pay');
			}
			else {
				$this->session->set_flashdata('code', 'Invalid Referral Code');
				$this->load->view('form/ram_maxi');
			}
		}
	}


	public function ram_maxi_pay()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		$this->load->database();
		//load registration view form
		$this->load->model('meat_model');

		$callbackurl = site_url(). 'Specta/maxi_verify';
		$ref = $this->input->post('reference');
		$merchant = $this->input->post('merchantid');
		//$plan = $this->input->post('plan');
		$email = $this->input->post('email');
		$amount = $this->input->post('ram_amount');

		if ($this->input->post('specta_btn')) {
			//update payment status
			$meat['specta_status'] = 'in-progress';
			$this->meat_model->update($meat, $ref);

			$data = array(
				"callBackUrl" => $callbackurl,
				"reference" => $ref,
				"merchantId" => $merchant,
				"description" => $email,
				"amount" => $amount
			);

			$post_data = json_encode($data);

			// Prepare new cURL resource
			$crl = curl_init('https://paywithspectaapi.sterling.ng/api/Purchase/CreatePaymentUrl');
			curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($crl, CURLINFO_HEADER_OUT, true);
			curl_setopt($crl, CURLOPT_POST, true);
			curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);

			// Set HTTP Header for POST request -
			// TEST_API_KEY
			// 969688b48dad4b8981be20fc035da527
			curl_setopt($crl, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'x-ApiKey: 969688b48dad4b8981be20fc035da527',
					'Content-Length: ' . strlen($post_data))
			);
			// Submit the POST request
			$result = curl_exec($crl);

			curl_close($crl);

			$result = json_decode($result);
			//print_r($result);
			// handle curl error
			if ((isset($result->success) && $result->success) && isset($result->result)) {
				redirect($result->result);
				exit;
			} else {

				$result_noti = 1; die();
			}
		}


		$this->load->view('form/ram_maxi_pay');
		$this->load->view('includes/footer');
	}



	public function ram_maxi_success($ram_maxi_success = '')
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load database
		$data['ram_maxi_success'] = $ram_maxi_success;
		$successfullypaid = $this->input->get('successfullypaid');
		$pref = $this->input->get('pref');
		$fname = $this->input->get('name');
		$phone_number = $this->input->get('phone_number');
		$delivery_address = $this->input->get('delivery_address');
		$delivery_date = $this->input->get('delivery_date');
		$process = $this->input->get('process');
		$email = $this->input->get('email');
		$ram_amount = $this->input->get('ram_amount');
		$reference = $this->input->get('reference');
		$referral = $this->input->get('referral');
		$status="Success";
		//  $verify = $this->input->get('verify');
		// $this->db->query("INSERT INTO biggie(paystack_reference_meat247) VALUES ('$successfullypaid')");

		$sql="UPDATE meat_sharing SET paystack_reference = '$successfullypaid', paystack_status = '$status'  WHERE  reference='$pref' ";
		$this->db->query($sql);

		//agent email
		$agent_email= $this->db->get('users', array('email' => 'email', 'referral' => $referral));


		//send email
		//set up email
		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'admin@livestock247.com', // change it to yours
			'smtp_pass' => 'b861N8cPchr3', // change it to yours
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);

		$to = $email;
		//$groupmail = 'esther.akowe@livestock247.com';
		$groupmail = 'meat247@livestock247.com';

		$message = "
						<html>
						<head>
							<title>New Meat247 Sales</title>
						</head>
						<body>
							
							<p>Hi $fname, Your payment of NGN $ram_amount for a Maxi Ram Meat Package was successful
    						</p>
    						<p>
    						 <strong>Transaction Details:</strong> <br>
							Payment ID: 		$pref<br>
							Name:  				$fname<br>
							Email: 				$email<br>
							Phone: 				$phone_number<br>
							Delivery Address:	$delivery_address<br>
							Delivery Date (Y/M/D):	$delivery_date<br>
							How to Process:			$process
						</p>
							
					
					
						</body>
						</html>
						";

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from($config['smtp_user']);
		$this->email->to($to);
		$this->email->cc($groupmail, $agent_email);
		$this->email->subject('Meat247 Maxi Ram Request');
		$this->email->message($message);

		if ($this->email->send()) {

		}
		//load view
		$this->load->view('form/ram_maxi_success', $data);
		$this->output->set_header('refresh:5;'.site_url());
		$this->load->view('includes/footer');
	}



	public function ram_standard()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load registration view form

		$this->load->helper('form');
		$this->load->helper('url');
		//load registration view form
		//load Model
		$this->load->database();
		$this->load->library('session');

		// validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('form/ram_standard');
			$this->load->view('includes/footer');
		} else {
			//load Model
			$this->load->model('Referral_model');
			$fname = $this->input->post('name');
			$phone_number = $this->input->post('phone_number');
			$delivery_address = $this->input->post('delivery_address');
			$delivery_date = $this->input->post('delivery_date');
			$process = $this->input->post('process');
			$email = $this->input->post('email');
			$plan = "Standard";
			$ram_amount = $this->input->post('ram_amount');
			$amount = '55000';
			$reference = $this->input->post('reference');
			$paystack_status="Pending";
			$referral_code = $this->input->post('referral');
			$code = $this->Referral_model->referral($referral_code);

			if($referral_code== $code) {
				//get percentage for a particular user base on referral code entered
				$query = $this->db->get_where('users',array('referral' => $referral_code));
				$row = $query->row(0);

				$percentage = isset($row->percentage) ? $row->percentage : 0; // access attributes
				//agent commission
				$commission = $amount * $percentage /100;
				$agent_commission = $commission;
				// customer's discount
				$c_per =1;
				$c_discount = $amount * $c_per / 100;
				//$discount_amount= $goat_amount - $c_discount;

				$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,delivery_date, phone_number,process,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$delivery_date' ,'$phone_number', '$process', '$plan', '$referral_code','$agent_commission', '$c_discount','$ram_amount','$reference','$paystack_status', 'Not Paid')");
				$this->load->view('form/ram_standard_pay');
		}

	elseif (empty($referral_code))
	{
	$agent_commission = 0;
		$c_discount = 0;
		$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,delivery_date, phone_number,process,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$delivery_date' ,'$phone_number', '$process', '$plan', '$referral_code','$agent_commission', '$c_discount','$ram_amount','$reference','$paystack_status', 'Not Paid')");
		//redirect('biggie_pay');
				$this->load->view('form/ram_standard_pay');
			}
			else {
				$this->session->set_flashdata('code', 'Invalid Referral Code');
				$this->load->view('form/ram_standard');
			}
		}
	}


	public function ram_standard_pay()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		$this->load->database();
		//load registration view form

		$this->load->model('meat_model');

		$callbackurl = site_url(). 'Specta/standard_verify';
		$ref = $this->input->post('reference');
		$merchant = $this->input->post('merchantid');
		//$plan = $this->input->post('plan');
		$email = $this->input->post('email');
		$amount = $this->input->post('ram_amount');

		if ($this->input->post('specta_btn')) {
			//update payment status
			$meat['specta_status'] = 'in-progress';
			$this->meat_model->update($meat, $ref);

			$data = array(
				"callBackUrl" => $callbackurl,
				"reference" => $ref,
				"merchantId" => $merchant,
				"description" => $email,
				"amount" => $amount
			);

			$post_data = json_encode($data);

			// Prepare new cURL resource
			$crl = curl_init('https://paywithspectaapi.sterling.ng/api/Purchase/CreatePaymentUrl');
			curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($crl, CURLINFO_HEADER_OUT, true);
			curl_setopt($crl, CURLOPT_POST, true);
			curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);

			// Set HTTP Header for POST request -
			// TEST_API_KEY
			// 969688b48dad4b8981be20fc035da527
			curl_setopt($crl, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'x-ApiKey: 969688b48dad4b8981be20fc035da527',
					'Content-Length: ' . strlen($post_data))
			);
			// Submit the POST request
			$result = curl_exec($crl);

			curl_close($crl);

			$result = json_decode($result);
			//print_r($result);
			// handle curl error
			if ((isset($result->success) && $result->success) && isset($result->result)) {
				redirect($result->result);
				exit;
			} else {

				$result_noti = 1; die();
			}
		}
		$this->load->view('form/ram_standard_pay');
		$this->load->view('includes/footer');
	}


	public function ram_standard_success($ram_standard_success = '')
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load database
		$data['ram_standard_success'] = $ram_standard_success;

		$successfullypaid = $this->input->get('successfullypaid');
		$pref = $this->input->get('pref');
		$fname = $this->input->get('name');
		$phone_number = $this->input->get('phone_number');
		$delivery_address = $this->input->get('delivery_address');
		$delivery_date = $this->input->get('delivery_date');
		$process = $this->input->get('process');
		$email = $this->input->get('email');
		$ram_amount = $this->input->get('ram_amount');
		$reference = $this->input->get('reference');
		$referral = $this->input->get('referral');
		$status="Success";
		//  $verify = $this->input->get('verify');
		// $this->db->query("INSERT INTO biggie(paystack_reference_meat247) VALUES ('$successfullypaid')");

		$sql="UPDATE meat_sharing SET paystack_reference = '$successfullypaid', paystack_status = '$status'  WHERE  reference='$pref' ";
		$this->db->query($sql);

		//agent email
		$agent_email= $this->db->get('users', array('email' => 'email', 'referral' => $referral));


		//send email
		//set up email
		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'admin@livestock247.com', // change it to yours
			'smtp_pass' => 'b861N8cPchr3', // change it to yours
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);

		$to = $email;
		//$groupmail = 'esther.akowe@livestock247.com';
		$groupmail = 'meat247@livestock247.com';

		$message = "
						<html>
						<head>
							<title>New Meat247 Sales</title>
						</head>
						<body>
							
							<p>Hi $fname, Your payment of NGN $ram_amount for a Standard Ram Meat Package was successful
    						</p>
    						<p>
    						 <strong>Transaction Details:</strong> <br>
							Payment ID: 		$pref<br>
							Name:  				$fname<br>
							Email: 				$email<br>
							Phone: 				$phone_number<br>
							Delivery Address:	$delivery_address<br>
							Delivery Date (Y/M/D):	$delivery_date<br>
							How to Process:			$process
						</p>
							
					
					
						</body>
						</html>
						";

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from($config['smtp_user']);
		$this->email->to($to);
		$this->email->cc($groupmail, $agent_email);
		$this->email->subject('Meat247 Standard Ram Request');
		$this->email->message($message);

		if ($this->email->send()) {

		}
		//load view
		$this->load->view('form/ram_standard_success', $data);
		$this->output->set_header('refresh:5;'.site_url());
		//$this->output->set_header('refresh:5;url=wherever.php');
		$this->load->view('includes/footer');
	}



	public function ram_compact()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load registration view form

		$this->load->helper('form');
		$this->load->helper('url');
		//load registration view form
		//load Model
		$this->load->database();
		$this->load->library('session');

		// validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('form/ram_compact');
			$this->load->view('includes/footer');
		} else {
			//load Model
			$this->load->model('Referral_model');
			$fname = $this->input->post('name');
			$phone_number = $this->input->post('phone_number');
			$delivery_address = $this->input->post('delivery_address');
			$delivery_date = $this->input->post('delivery_date');
			$process = $this->input->post('process');
			$email = $this->input->post('email');
			$plan = "Compact";
			$ram_amount = $this->input->post('ram_amount');
			$amount = '45000';
			$reference = $this->input->post('reference');
			$paystack_status="Pending";
			$referral_code = $this->input->post('referral');
			$code = $this->Referral_model->referral($referral_code);

			if($referral_code== $code) {
				//get percentage for a particular user base on referral code entered
				$query = $this->db->get_where('users',array('referral' => $referral_code));
				$row = $query->row(0);

				$percentage = isset($row->percentage) ? $row->percentage : 0; // access attributes
				//agent commission
				$commission = $amount * $percentage /100;
				$agent_commission = $commission;
				// customer's discount
				$c_per =1;
				$c_discount = $amount * $c_per / 100;
				//$discount_amount= $goat_amount - $c_discount;

				$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,delivery_date, phone_number,process,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$delivery_date' ,'$phone_number', '$process', '$plan', '$referral_code','$agent_commission', '$c_discount','$ram_amount','$reference','$paystack_status', 'Not Paid')");
				$this->load->view('form/ram_compact_pay');

			}

	elseif (empty($referral_code))
	{
	$agent_commission = 0;
		$c_discount = 0;
		$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,delivery_date, phone_number,process,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$delivery_date' ,'$phone_number', '$process', '$plan', '$referral_code','$agent_commission', '$c_discount','$ram_amount','$reference','$paystack_status', 'Not Paid')");
		//redirect('biggie_pay');
				$this->load->view('form/ram_compact_pay');
			}
			else {
				$this->session->set_flashdata('code', 'Invalid Referral Code');
				$this->load->view('form/ram_compact');
			}
		}
	}


	public function ram_compact_pay()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load registration view form
		$this->load->database();
		$this->load->model('meat_model');

		$callbackurl = site_url(). 'Specta/compact_verify';
		$ref = $this->input->post('reference');
		$merchant = $this->input->post('merchantid');
		//$plan = $this->input->post('plan');
		$email = $this->input->post('email');
		$amount = $this->input->post('ram_amount');

		if ($this->input->post('specta_btn')) {
			//update payment status
			$meat['specta_status'] = 'in-progress';
			$this->meat_model->update($meat, $ref);

			$data = array(
				"callBackUrl" => $callbackurl,
				"reference" => $ref,
				"merchantId" => $merchant,
				"description" => $email,
				"amount" => $amount
			);

			$post_data = json_encode($data);

			// Prepare new cURL resource
			$crl = curl_init('https://paywithspectaapi.sterling.ng/api/Purchase/CreatePaymentUrl');
			curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($crl, CURLINFO_HEADER_OUT, true);
			curl_setopt($crl, CURLOPT_POST, true);
			curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);

			// Set HTTP Header for POST request -
			// TEST_API_KEY
			// 969688b48dad4b8981be20fc035da527
			curl_setopt($crl, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'x-ApiKey: 969688b48dad4b8981be20fc035da527',
					'Content-Length: ' . strlen($post_data))
			);
			// Submit the POST request
			$result = curl_exec($crl);

			curl_close($crl);

			$result = json_decode($result);
			//print_r($result);
			// handle curl error
			if ((isset($result->success) && $result->success) && isset($result->result)) {
				redirect($result->result);
				exit;
			} else {

				$result_noti = 1; die();
			}
		}

		$this->load->view('form/ram_compact_pay');
		$this->load->view('includes/footer');
	}


	public function ram_compact_success($ram_compact_success = '')
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load database
		$data['ram_compact_success'] = $ram_compact_success;
		$successfullypaid = $this->input->get('successfullypaid');
		$pref = $this->input->get('pref');
		$fname = $this->input->get('name');
		$phone_number = $this->input->get('phone_number');
		$delivery_address = $this->input->get('delivery_address');
		$delivery_date = $this->input->get('delivery_date');
		$process = $this->input->get('process');
		$email = $this->input->get('email');
		$ram_amount = $this->input->get('ram_amount');
		$reference = $this->input->get('reference');
		$referral = $this->input->get('referral');
		$status="Success";
		//  $verify = $this->input->get('verify');
		// $this->db->query("INSERT INTO biggie(paystack_reference_meat247) VALUES ('$successfullypaid')");

		$sql="UPDATE meat_sharing SET paystack_reference = '$successfullypaid', paystack_status = '$status'  WHERE  reference='$pref' ";
		$this->db->query($sql);

		//agent email
		$agent_email= $this->db->get('users', array('email' => 'email', 'referral' => $referral));


		//send email
		//set up email
		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'admin@livestock247.com', // change it to yours
			'smtp_pass' => 'b861N8cPchr3', // change it to yours
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);

		$to = $email;
		//$groupmail = 'esther.akowe@livestock247.com';
		$groupmail = 'meat247e@livestock247.com';

		$message = "
						<html>
						<head>
							<title>New Meat247 Sales</title>
						</head>
						<body>
							
							<p>Hi $fname, Your payment of NGN $ram_amount for a Compact Ram Meat Package was successful
    						</p>
    						<p>
    						 <strong>Transaction Details:</strong> <br>
							Payment ID: 		$pref<br>
							Name:  				$fname<br>
							Email: 				$email<br>
							Phone: 				$phone_number<br>
							Delivery Address:	$delivery_address<br>
							Delivery Date (Y/M/D):	$delivery_date<br>
							How to Process:			$process
						</p>
						
						</body>
						</html>
						";

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from($config['smtp_user']);
		$this->email->to($to);
		$this->email->cc($groupmail, $agent_email);
		$this->email->subject('Meat247 Compact Ram Request');
		$this->email->message($message);

		if ($this->email->send()) {

		}
		//load view
		$this->load->view('form/ram_compact_success', $data);
		$this->output->set_header('refresh:5;'.site_url());
		$this->load->view('includes/footer');
	}


	public function flash_sales()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load registration view form

		$this->load->helper('form');
		$this->load->helper('url');
		//load registration view form
		//load Model
		$this->load->database();
		$this->load->library('session');

		// validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('form/flash_sales');
			$this->load->view('includes/footer');
		} else {
			//load Model
			$this->load->model('Referral_model');
			$fname = $this->input->post('name');
			$phone_number = $this->input->post('phone_number');
			$delivery_address = $this->input->post('delivery_address');
			$email = $this->input->post('email');
			$plan = "Flash_Sales";
			$goat_amount = $this->input->post('goat_amount');
			$amount = '22000';
			$reference = $this->input->post('reference');
			$paystack_status="Pending";
			$referral_code = $this->input->post('referral');
			$code = $this->Referral_model->referral($referral_code);

			if($referral_code== $code) {
				//get percentage for a particular user base on referral code entered
				$query = $this->db->get_where('users',array('referral' => $referral_code));
				$row = $query->row(0);

				$percentage = isset($row->percentage) ? $row->percentage : 0; // access attributes
				//agent commission
				$commission = $amount * $percentage /100;
				$agent_commission = $commission;
				// customer's discount
				$c_per =1;
				$c_discount = $amount * $c_per / 100;
				//$discount_amount= $goat_amount - $c_discount;
				$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,phone_number,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$phone_number','$plan', '$referral_code','$agent_commission', '$c_discount','$goat_amount','$reference','$paystack_status', 'Not Paid')");
				$this->load->view('form/flash_sales_pay');
			}

			elseif (empty($referral_code))
			{
				$agent_commission = 0;
				$c_discount = 0;
				$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,phone_number,plan, referral, agent_commission, customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$phone_number','$plan', '$referral_code','$agent_commission','$c_discount','$goat_amount', '$reference','$paystack_status', 'Not Paid')");
				//redirect('biggie_pay');
				$this->load->view('form/flash_sales_pay');
			}
			else {
				$this->session->set_flashdata('code', 'Invalid Referral Code');
				$this->load->view('form/flash_sales');
			}
		}
	}




	public function flash_sales_pay()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load registration view form
		$this->load->view('form/flash_sales_pay');
//		$this->load->helper('form');
//		$this->load->helper('url');
		$this->load->database();
		$this->load->view('includes/footer');
	}



	public function flash_sales_success($flash_sales_success = '')
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load database
		$data['flash_sales_success'] = $flash_sales_success;
		$successfullypaid = $this->input->get('successfullypaid');
		$pref = $this->input->get('pref');
		$fname = $this->input->get('name');
		$phone_number = $this->input->get('phone_number');
		$delivery_address = $this->input->get('delivery_address');
		$email = $this->input->get('email');
		$goat_amount = $this->input->get('goat_amount');
		$reference = $this->input->get('reference');
		$referral = $this->input->get('referral');
		$status="Success";
		//  $verify = $this->input->get('verify');
		// $this->db->query("INSERT INTO biggie(paystack_reference_meat247) VALUES ('$successfullypaid')");

		$sql="UPDATE meat_sharing SET paystack_reference = '$successfullypaid', paystack_status = '$status'  WHERE  reference='$pref' ";
		$this->db->query($sql);

		//agent email
		$agent_email= $this->db->get('users', array('email' => 'email', 'referral' => $referral));


		//send email
		//set up email
		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'admin@livestock247.com', // change it to yours
			'smtp_pass' => 'b861N8cPchr3', // change it to yours
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);

		$to = $email;
	//	$groupmail = 'esther.akowe@livestock247.com';
		$groupmail = 'meat247@livestock247.com';

		$message = "
						<html>
						<head>
							<title>New Meat247 Sales</title>
						</head>
						<body>
							
							<p>Hi $fname, Your payment of NGN $goat_amount for a Flash Sales Medium Goat Meat Package was successful
    						</p>
    						<p>
    						 <strong>Transaction Details:</strong> <br>
							Payment ID: 		$pref<br>
							Name:  				$fname<br>
							Email: 				$email<br>
							Phone: 				$phone_number<br>
							Delivery Address:	$delivery_address
						</p>
						
						</body>
						</html>
						";

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from($config['smtp_user']);
		$this->email->to($to);
		$this->email->cc($groupmail, $agent_email);
		$this->email->subject('Meat247 Flash Sales Request');
		$this->email->message($message);

		if ($this->email->send()) {

		}
		//load view
		$this->load->view('form/flash_sales_success', $data);
		$this->load->view('includes/footer');
	}

	public function easter()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		$this->load->view('specials/easter');
		$this->load->view('includes/footer');
	}

	public function sallah()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		$this->load->view('specials/sallah');
		$this->load->view('includes/footer');
	}

	public function goatysales()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		$this->load->view('pages/goatysales');
		$this->load->view('includes/footer');
	}

	public function christmasMedium()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//$this->load->view('pages/christmasSales');
		$this->load->database();
		// validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('pages/christmasMedium');
			$this->load->view('includes/footer');
		} else {
			//load Model
			$this->load->model('Referral_model');
			$fname = $this->input->post('name');
			$phone_number = $this->input->post('phone_number');
			$delivery_address = $this->input->post('delivery_address');
			$delivery_date = $this->input->post('delivery_date');
			$process = $this->input->post('process');
			$email = $this->input->post('email');
			$plan = "Medium";
			$goat_amount = $this->input->post('goat_amount');
			$amount = '23000';
			$reference = $this->input->post('reference');
			$paystack_status="Pending";
			$referral_code = $this->input->post('referral');
			$code = $this->Referral_model->referral($referral_code);

			if($referral_code== $code) {
				//get percentage for a particular user base on referral code entered
				$query = $this->db->get_where('users',array('referral' => $referral_code));
				$row = $query->row(0);

				$percentage = isset($row->percentage) ? $row->percentage : 0; // access attributes
				//agent commission
				$commission = $amount * $percentage /100;
				$agent_commission = $commission;
				// customer's discount
				$c_per =1;
				$c_discount = $amount * $c_per / 100;
				//$discount_amount= $goat_amount - $c_discount;
				$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,delivery_date, phone_number,process,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$delivery_date' ,'$phone_number', '$process', '$plan', '$referral_code','$agent_commission', '$c_discount','$goat_amount','$reference','$paystack_status', 'Not Paid')");
				$this->load->view('pages/christmasMedium_pay');
			}

			elseif (empty($referral_code))
			{
				$agent_commission = 0;
				$c_discount = 0;
				$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,delivery_date, phone_number,process,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$delivery_date' ,'$phone_number', '$process', '$plan', '$referral_code','$agent_commission', '$c_discount','$goat_amount','$reference','$paystack_status', 'Not Paid')");
				//redirect('biggie_pay');
				$this->load->view('pages/christmasMedium_pay');
			}
			else {
				$this->session->set_flashdata('code', 'Invalid Referral Code');
				$this->load->view('pages/christmasMedium');
			}
		}

		//$this->load->view('includes/footer');
	}
	public function christmasMedium_pay()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		$this->load->model('meat_model');
		//load registration view form
		$this->load->database();
		$callbackurl = site_url(). 'Specta/christmas';
		$ref = $this->input->post('reference');
		$merchant = $this->input->post('merchantid');
		//$plan = $this->input->post('plan');
		$email = $this->input->post('email');
		$amount = $this->input->post('goat_amount');

		if ($this->input->post('specta_btn')) {
			//update payment status
			$meat['specta_status'] = 'in-progress';
			$this->meat_model->update($meat, $ref);

			$data = array(
				"callBackUrl" => $callbackurl,
				"reference" => $ref,
				"merchantId" => $merchant,
				"description" => $email,
				"amount" => $amount
			);

			$post_data = json_encode($data);

			// Prepare new cURL resource
			$crl = curl_init('https://paywithspectaapi.sterling.ng/api/Purchase/CreatePaymentUrl');
			curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($crl, CURLINFO_HEADER_OUT, true);
			curl_setopt($crl, CURLOPT_POST, true);
			curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);

			// Set HTTP Header for POST request -
			// TEST_API_KEY
			// 969688b48dad4b8981be20fc035da527
			curl_setopt($crl, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'x-ApiKey: 969688b48dad4b8981be20fc035da527',
					'Content-Length: ' . strlen($post_data))
			);
			// Submit the POST request
			$result = curl_exec($crl);

			curl_close($crl);

			$result = json_decode($result);
			//print_r($result);
			// handle curl error
			if ((isset($result->success) && $result->success) && isset($result->result)) {
				redirect($result->result);
				exit;
			} else {

				$result_noti = 1; die();
			}
		}
		$this->load->view('pages/christmasMedium_pay');
		//$this->load->view('includes/footer');
	}

	public function christmasMedium_success($christmasMedium_success = '')
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load database
		$data['christmasMedium_success'] = $christmasMedium_success;
		$successfullypaid = $this->input->get('successfullypaid');
		$pref = $this->input->get('pref');
		$fname = $this->input->get('name');
		$phone_number = $this->input->get('phone_number');
		$delivery_address = $this->input->get('delivery_address');
		$email = $this->input->get('email');
		$goat_amount = $this->input->get('goat_amount');
		$reference = $this->input->get('reference');
	//	$referral = $this->input->get('referral');
		$status="Success";
		//  $verify = $this->input->get('verify');
		// $this->db->query("INSERT INTO biggie(paystack_reference_meat247) VALUES ('$successfullypaid')");

		$sql="UPDATE meat_sharing SET paystack_reference = '$successfullypaid', paystack_status = '$status'  WHERE  reference='$pref' ";
		$this->db->query($sql);


		//send email
		//set up email
		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'admin@livestock247.com', // change it to yours
			'smtp_pass' => 'b861N8cPchr3', // change it to yours
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);

		$to = $email;
			$groupmail = 'meat247@livestock247.com';
		//$groupmail = 'esther.akowe@livestock247.com';

		$message = "
						<html>
						<head>
							<title>New Meat247 Sales</title>
						</head>
						<body>
							
							<p>Hi $fname, Your payment of NGN $goat_amount for a Christmas Promo Medium Goat Meat Package was successful
    						</p>
    						<p>
    						 <strong>Transaction Details:</strong> <br>
							Payment ID: 		$pref<br>
							Name:  				$fname<br>
							Email: 				$email<br>
							Phone: 				$phone_number<br>
							Delivery Address:	$delivery_address
						</p>
						
						</body>
						</html>
						";

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from($config['smtp_user']);
		$this->email->to($to);
		$this->email->cc($groupmail);
		$this->email->subject('Meat247 Christmas Promo Request');
		$this->email->message($message);

		if ($this->email->send()) {

		}
		//load view
		$this->load->view('pages/christmasMedium_success', $data);
		$this->output->set_header('refresh:5;'.site_url());
		$this->load->view('includes/footer');
	}



	public function christmasLarge()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//$this->load->view('pages/christmasSales');
		$this->load->database();
		// validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('pages/christmasLarge');
			$this->load->view('includes/footer');
		} else {
			//load Model
			$this->load->model('Referral_model');
			$fname = $this->input->post('name');
			$phone_number = $this->input->post('phone_number');
			$delivery_address = $this->input->post('delivery_address');
			$delivery_date = $this->input->post('delivery_date');
			$process = $this->input->post('process');
			$email = $this->input->post('email');
			$plan = "Large";
			$goat_amount = $this->input->post('goat_amount');
			$amount = '27800';
			$reference = $this->input->post('reference');
			$paystack_status="Pending";
			$referral_code = $this->input->post('referral');
			$code = $this->Referral_model->referral($referral_code);

			if($referral_code== $code) {
				//get percentage for a particular user base on referral code entered
				$query = $this->db->get_where('users',array('referral' => $referral_code));
				$row = $query->row(0);

				$percentage = isset($row->percentage) ? $row->percentage : 0; // access attributes
				//agent commission
				$commission = $amount * $percentage /100;
				$agent_commission = $commission;
				// customer's discount
				$c_per =1;
				$c_discount = $amount * $c_per / 100;
				//$discount_amount= $goat_amount - $c_discount;
				$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,delivery_date, phone_number,process,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$delivery_date' ,'$phone_number', '$process', '$plan', '$referral_code','$agent_commission', '$c_discount','$goat_amount','$reference','$paystack_status', 'Not Paid')");
				$this->load->view('pages/christmasLarge_pay');
			}

			elseif (empty($referral_code))
			{
				$agent_commission = 0;
				$c_discount = 0;
				$this->db->query("INSERT INTO meat_sharing(cname,email,delivery_address,delivery_date, phone_number,process,plan,referral,agent_commission,customer_discount,amount,reference,paystack_status, agent_payment_status) VALUES ('$fname','$email','$delivery_address', '$delivery_date' ,'$phone_number', '$process', '$plan', '$referral_code','$agent_commission', '$c_discount','$goat_amount','$reference','$paystack_status', 'Not Paid')");
				//redirect('biggie_pay');
				$this->load->view('pages/christmasLarge_pay');
			}
			else {
				$this->session->set_flashdata('code', 'Invalid Referral Code');
				$this->load->view('pages/christmasLarge');
			}
		}

		//$this->load->view('includes/footer');
	}
	public function christmasLarge_pay()
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		$this->load->model('meat_model');
		//load registration view form
		$this->load->database();
		$callbackurl = site_url(). 'Specta/christmasLarge';
		$ref = $this->input->post('reference');
		$merchant = $this->input->post('merchantid');
		//$plan = $this->input->post('plan');
		$email = $this->input->post('email');
		$amount = $this->input->post('goat_amount');

		if ($this->input->post('specta_btn')) {
			//update payment status
			$meat['specta_status'] = 'in-progress';
			$this->meat_model->update($meat, $ref);

			$data = array(
				"callBackUrl" => $callbackurl,
				"reference" => $ref,
				"merchantId" => $merchant,
				"description" => $email,
				"amount" => $amount
			);

			$post_data = json_encode($data);

			// Prepare new cURL resource
			$crl = curl_init('https://paywithspectaapi.sterling.ng/api/Purchase/CreatePaymentUrl');
			curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($crl, CURLINFO_HEADER_OUT, true);
			curl_setopt($crl, CURLOPT_POST, true);
			curl_setopt($crl, CURLOPT_POSTFIELDS, $post_data);

			// Set HTTP Header for POST request -
			// TEST_API_KEY
			// 969688b48dad4b8981be20fc035da527
			curl_setopt($crl, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'x-ApiKey: 969688b48dad4b8981be20fc035da527',
					'Content-Length: ' . strlen($post_data))
			);
			// Submit the POST request
			$result = curl_exec($crl);

			curl_close($crl);

			$result = json_decode($result);
			//print_r($result);
			// handle curl error
			if ((isset($result->success) && $result->success) && isset($result->result)) {
				redirect($result->result);
				exit;
			} else {

				$result_noti = 1; die();
			}
		}
		$this->load->view('pages/christmasLarge_pay');
		//$this->load->view('includes/footer');
	}

	public function christmasLarge_success($christmasLarge_success = '')
	{
		$this->load->view('includes/head');
		$this->load->view('includes/header');
		//load database
		$data['christmasLarge_success'] = $christmasLarge_success;
		$successfullypaid = $this->input->get('successfullypaid');
		$pref = $this->input->get('pref');
		$fname = $this->input->get('name');
		$phone_number = $this->input->get('phone_number');
		$delivery_address = $this->input->get('delivery_address');
		$email = $this->input->get('email');
		$goat_amount = $this->input->get('goat_amount');
		$reference = $this->input->get('reference');
		//	$referral = $this->input->get('referral');
		$status="Success";
		//  $verify = $this->input->get('verify');
		// $this->db->query("INSERT INTO biggie(paystack_reference_meat247) VALUES ('$successfullypaid')");

		$sql="UPDATE meat_sharing SET paystack_reference = '$successfullypaid', paystack_status = '$status'  WHERE  reference='$pref' ";
		$this->db->query($sql);


		//send email
		//set up email
		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'admin@livestock247.com', // change it to yours
			'smtp_pass' => 'b861N8cPchr3', // change it to yours
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);

		$to = $email;
		//	$groupmail = 'esther.akowe@livestock247.com';
		$groupmail = 'meat247@livestock247.com';

		$message = "
						<html>
						<head>
							<title>New Meat247 Sales</title>
						</head>
						<body>
							
							<p>Hi $fname, Your payment of NGN $goat_amount for a Christmas Promo Large Goat Meat Package was successful
    						</p>
    						<p>
    						 <strong>Transaction Details:</strong> <br>
							Payment ID: 		$pref<br>
							Name:  				$fname<br>
							Email: 				$email<br>
							Phone: 				$phone_number<br>
							Delivery Address:	$delivery_address
						</p>
						
						</body>
						</html>
						";

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from($config['smtp_user']);
		$this->email->to($to);
		$this->email->cc($groupmail);
		$this->email->subject('Meat247 Christmas Promo Request');
		$this->email->message($message);

		if ($this->email->send()) {

		}
		//load view
		$this->load->view('pages/christmasLarge_success', $data);
		$this->output->set_header('refresh:5;'.site_url());
		$this->load->view('includes/footer');
	}


}// end class

